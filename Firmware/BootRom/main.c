#include <string.h>
#include <stdint.h>
#include "../libFpgaBee/include/libFpgaBee.h"
#include "../libNffs/include/libNffs.h"
#include "mbr.h"

extern uint32_t g_diskBaseBlockNumber;

/*
Boot Loader Memory Map

0x0000 - 0x3FFF - boot loader rom (latched out after thunking to fpgabee.sys)
0x4000 - 0xF000 - load space for fpgabee.sys (before being moved to 0x0000)
0xF400 - 0xFF00 - boot loader data segment
0x**** - 0xFF00	- top of stack
0xFF00 - 0xFFFF - thunk to move fpgabee.sys to 0x0000, clear boot rom latch and launch
*/

NFFSCONTEXT nffsctx;
uint8_t diskBuffer[NFFS_BLOCK_SIZE];

void* nffsReadBlock(NFFSCONTEXT* ctx, uint32_t block);
void nffsLockBlock(NFFSCONTEXT* ctx, uint32_t block, bool lock);
void thunkStart(void* endPtr);

// Main Entry Point
void main(void) 
{
	int err;
	NFFSDIRENTRY de;
	uint8_t* pDest;
	uint32_t blockNumber;
	uint8_t i;

	memset(VideoCharBuffer, ' ', 1024);

	// Setup NFFS context
	memset(&nffsctx, 0, sizeof(NFFSCONTEXT));
	nffsctx.readBlock = nffsReadBlock;
	nffsctx.lockBlock = nffsLockBlock;

	// Wait for SD to initialize
	DiskInit();

	// Try to open on root partition
	err = nffsInit(&nffsctx);
	if (err == NFFS_ERR_INVALIDFS)
	{
		// Failed, try using first partition, assuming MBR record
		g_diskBaseBlockNumber = ((MBR*)diskBuffer)->partition[0].firstSector;
		if (g_diskBaseBlockNumber)
		{
			// Try again
			err = nffsInit(&nffsctx);
		}
	}

	// Failed to initialize nffs
	if (err)
	{
		DebugLeds = 0x81;
		return;
	}

	// Find "fpgabee.sys"
	err = nffsFindFile(&nffsctx, "fpgabee.sys", &de);
	if (err)
	{
		DebugLeds = 0x82;
		return;
	}

	// Read fbgabee.sys
	pDest = (void*)0x4000;
	blockNumber = de.block;
	for (i = 0; i < de.blockCount; i++)
	{
		if (!DiskRead(blockNumber, pDest))
		{
			DebugLeds = 0x83;
			return;
		}
		pDest += 512;
		blockNumber++;
	}

	// Run baby run...
	thunkStart(pDest);
}

void* nffsReadBlock(NFFSCONTEXT* ctx, uint32_t block)
{
	ctx;	// unused

	if (!DiskRead(block, diskBuffer))
	{
		return NULL;
	}
	return diskBuffer;
}

void nffsLockBlock(NFFSCONTEXT* ctx, uint32_t block, bool lock)
{
	ctx; block; lock;		// unused

	// nop
}

// Copy memory from 0x8000-endptr to 0x0000
// clear the boot rom flag and jump to 0x0100
void thunkStart(void* endPtr)
{
	endPtr;

	__asm

	; Copy the thunk routine to high memory
	ld		HL,#90$
	ld		DE,#0xFF00
	ld		BC,#99$ - #90$
	ldir	

	; Set BC to number of bytes to be copied
	; (just clear the top bit of endPtr)
	ld		iy,#2
	add		iy,sp
	ld		H,1 (iy)
	ld		L,0 (iy)
	ld      BC,#-0x4000
	add     HL,BC
	push	HL
	pop     BC

	; Source
	ld		HL,#0x4000

	; Destination
	ld		DE,#0

	jp		0xFF00

90$:
	; Turn off boot rom
	ld		A,#0
	out		(_MemoryBankPort),A

	; Copy loaded system image
	ldir

	; Pass FBFS root sector in DEBC
	ld		BC,(_g_diskBaseBlockNumber)
	ld		DE,(_g_diskBaseBlockNumber+2)

	; Entry point
	jp		0x100
99$:
	__endasm;
}

