///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Rename a file
int nffsRenameFile(NFFSCONTEXT* ctx, const char* oldname, const char* newname)
{
	FINDFILECTX fctx;
	int err;

	// Check new name is valid
	if (!nffsIsValidFileName(newname))
		return NFFS_ERR_FILENAME;

	// Check if new filename already exists
	if (nffsExists(ctx, newname))
		return NFFS_ERR_ALREADYEXISTS;

	// Find the file
	fctx.ctx = ctx;
	fctx.filename = oldname;
	err = nffsFindFileInternal(&fctx);
	if (err)
		return err;

	// Copy the new filename in
	strcpy(fctx.de->filename, newname);

	// Re-write the directory block
	if (!ctx->writeBlock(ctx, ctx->rootSector.directoryBlock + fctx.deIndex / NFFS_DIRENTRIES_PER_BLOCK, NULL))
		return NFFS_ERR_WRITE;

	// Done!
	return NFFS_OK;
}

