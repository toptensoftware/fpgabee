///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"


// Based roughly on this: https://superuser.com/questions/475874/how-does-the-windows-rename-command-interpret-wildcards
char* nffsGlobReplace(const char* file, const char* pattern, char* pVal)
{
	char* o = pVal;
	const char* p = pattern;
	const char* f = file;

	if (!nffsContainsWildcards(pattern))
	{
		while (*p)
		{
			*o++ = *p++;
		}
		*o = '\0';
		return o;
	}

	while (true)
	{
		switch (*p)
		{
			case '\0':
				// End if input

				// Strip dots and spaces from end of string
				while (o > pVal && o[-1]=='.' || o[-1]==' ')
					o--;
				*o = '\0';
				return o;

			case '*':
				p++;
				switch (*p)
				{
					case '\0':
					case '?':
						// Copy rest of string
						while (*f)
						{
							*o++ = *f++;
						}
						if (*p == '?')
							p++;
						break;

					default:
					{
						// Find the last instance of the next character
						const char* last = NULL;
						const char* t = f;
						while (*t)
						{
							if (*t == *p)
								last = t;
							t++;
						}

						// Not found? copy to end
						if (!last)
							last = t;

						// Copy everything up to the last dot
						memcpy(o, f, last-f);
						o += last - f;
						f = last;

						// Copy the character from the pattern
						*o++ = *p++;
						break;
					}
				}
				break;

			case '?':
				p++;

				// Copy one character (unless on '.' in input)
				if (*f != '.' && *f != '\0')
				{
					*o++ = *f++;
				}
				break;

			case '.':
				p++;
				while (*f != '.' && *f !='\0')
					f++;
				*o++ = '.';
				if (*f == '.')
					f++;
				break;

			default:
				// Copy character to output
				*o++ = *p++;

				// Skip character in input but don't skip dots
				if (*f != '.' && *f != '\0')
					f++;
				
				break;
		}
	}
	
}
