///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamClose(NFFSSTREAM* stream)
{
	// Flush stream
	int err = nffsStreamFlush(stream);
	if (err)
		return err;

	if (!stream->readOnly)
	{
		if (stream->tailStream)
		{
			// Save the tail
			NFFSDIRENTRY deUnused;
			err = nffsSaveTail(stream->owner, stream->filename, stream->length, &deUnused);
		}
		else
		{
			// Update the file length
			err = nffsSetFileLength(stream->owner, stream->filename, stream->length, false);
		}
	}

	// Release the stream
	free(stream->filename);
	free(stream);
	return err;
}
