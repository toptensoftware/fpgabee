#include "../include/libNffs.h"
#include <string.h>
#include <stdlib.h>

#ifdef __SDCC
void qsort(void *base, size_t nitems, size_t size, int (*compar)(const void *, const void*));
#endif


#ifdef _MSC_VER
#pragma warning(disable: 4996)
#endif

typedef struct FINDFILECTX
{
	NFFSCONTEXT* ctx;
	const char* filename;
	uint32_t deIndex;
	NFFSDIRENTRY* de;
} FINDFILECTX;

// Internal version of find file returns information about where in directory
// the directory entry lives
int nffsFindFileInternal(FINDFILECTX* pfctx);


typedef struct tagBLOCKRANGE
{
	uint32_t block;
	uint32_t blockCount;
} BLOCKRANGE;


// Given a range of used block ranges, find space for a specified number of blocks
uint32_t nffsFindSpace(BLOCKRANGE* pUsedBlocks, uint32_t rangeCount, uint32_t requiredBlocks);

// Internal version of find files can return unused entries
int nffsEnumFilesInternal(NFFSCONTEXT* ctx, PFNENUMFILES callback, void* user, bool includedUnusedEntries);

// Internal version allows create file at a specified location
int nffsCreateFileInternal(NFFSCONTEXT* ctx, const char* filename, uint32_t length, uint32_t blockNumber, uint32_t blocks, NFFSDIRENTRY* ppde);

// Internal helper to write the root sector
int nffsWriteRootSector(NFFSCONTEXT* ctx);

// Open stream helper
int nffsOpenStreamInternal(NFFSCONTEXT* ctx, const char* filename, uint32_t block, uint32_t blockCount, uint32_t length, bool readOnly, bool tailStream, NFFSSTREAM** pVal);

