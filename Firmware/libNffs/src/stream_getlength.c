///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamGetLength(NFFSSTREAM* stream, uint32_t* pVal)
{
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;

	*pVal = stream->length;
	return NFFS_OK;
}

