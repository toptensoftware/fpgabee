///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

typedef struct tagCREATEFILECONTEXT
{
	BLOCKRANGE* pRangePtr;
	uint32_t requiredBlocks;
	uint32_t unusedDirEntryIndex;
	bool unusedDirEntryFound;
	uint32_t existingDirEntryIndex;
	bool existingDirEntryFound;
	bool fitsInExistingLocation;
	const char* filename;
} CREATEFILECONTEXT;


static bool scanDirectory(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	CREATEFILECONTEXT* ctx = (CREATEFILECONTEXT*)user;

	if (de->filename[0] == '\0')
	{
		if (!ctx->unusedDirEntryFound)
		{
			// Remember unused directory entry
			ctx->unusedDirEntryIndex = deIndex;
			ctx->unusedDirEntryFound = true;
		}
	}
	else
	{
		// Existing file found?
		if (strcmp(de->filename, ctx->filename)==0)
		{
			ctx->existingDirEntryFound = true;
			ctx->existingDirEntryIndex = deIndex;
			ctx->fitsInExistingLocation = de->blockCount >= ctx->requiredBlocks;

			// Don't need to finish scanning cause everything already fits
			if (ctx->fitsInExistingLocation)
				return false;
		}

		// Copy the used range
		ctx->pRangePtr->block = de->block;
		ctx->pRangePtr->blockCount = de->blockCount;
		ctx->pRangePtr++;
	}

	return true;
}


// Create a new file
int nffsCreateFile(NFFSCONTEXT* ctx, const char* filename, uint32_t blocks, uint32_t length, NFFSDIRENTRY* pde)
{
	// Disallow public file creation during active tail operations
	if (ctx->tailBlock != 0)
		return NFFS_ERR_TAIL;

	return nffsCreateFileInternal(ctx, filename, length, 0, blocks, pde);
}

// Create a new file
int nffsCreateFileInternal(NFFSCONTEXT* ctx, const char* filename, uint32_t length, uint32_t blockNumber, uint32_t blocks, NFFSDIRENTRY* pde)
{
	NFFSROOTSECTOR rs;
	uint32_t rangeCount;
	BLOCKRANGE* pRanges;
	CREATEFILECONTEXT cfctx;
	int err;
	uint32_t directoryIndex;
	uint32_t directoryBlock;
	uint8_t directorySubIndex;
	NFFSDIRENTRY* pDE;

	// Check filename is valid
	if (!nffsIsValidFileName(filename))
		return NFFS_ERR_FILENAME;

	// File system clean?
	if (ctx->rootSector.dirty)
		return NFFS_ERR_NEEDSREPAIR;

	// Work on a local copy of the root sector for now
	memcpy(&rs, &ctx->rootSector, sizeof(rs));

	// Allocate memory block ranges (enough for every file in the directory + the directory)
	rangeCount = rs.fileCount + 1;
	pRanges = malloc(sizeof(BLOCKRANGE) * rangeCount);
	if (!pRanges)
		return NFFS_ERR_MEMORY;

	// How many blocks
	if (blocks == 0)
	{ 
		// Work out it out to fit
		blocks = (length + NFFS_BLOCK_SIZE - 1) / NFFS_BLOCK_SIZE;
		if (blocks == 0)
			blocks = 1;
	}
	else
	{
		// Check specifed length fits
		if (length > blocks * NFFS_BLOCK_SIZE)
			return NFFS_ERR_SPACE;
	}

	// Scan directory
	cfctx.requiredBlocks = blocks;
	cfctx.pRangePtr = pRanges + 1;
	cfctx.unusedDirEntryFound = false;
	cfctx.existingDirEntryFound = false;
	cfctx.filename = filename;

	// Used the first entry in the range array for either 
	// a) the directory table if creating a new file in as yet unknown location
	// b) the already allocated file if creating file at existing location
	if (blockNumber == 0)
	{
		// Don't allocate over the directory
		pRanges[0].block = rs.directoryBlock;
		pRanges[0].blockCount = rs.directoryBlockCount;
	}
	else
	{
		// Don't reallocate the directory over the already allocated file
		pRanges[0].block = blockNumber;
		pRanges[0].blockCount = cfctx.requiredBlocks;
	}

	// Scan directory
	err = nffsEnumFilesInternal(ctx, scanDirectory, &cfctx, true);
	if (err && err != NFFS_ERR_CANCELLED)
		goto exit;

	// Mark FS as dirty
	ctx->rootSector.dirty = true;
	err = nffsWriteRootSector(ctx);
	if (err)
		goto exit;

	// Do we have room for the directory entry
	if (cfctx.existingDirEntryFound)
	{
		directoryIndex = cfctx.existingDirEntryIndex;
	}
	else
	{
		if (cfctx.unusedDirEntryFound)
		{
			// Can use an existing unused entry
			directoryIndex = cfctx.unusedDirEntryIndex;
		}
		else
		{
			// No unused entries, allocate a new one
			if (rs.directoryHighWater < rs.directoryBlockCount * NFFS_DIRENTRIES_PER_BLOCK)
			{
				// Still have room in the current directory
				directoryIndex = rs.directoryHighWater;
			}
			else
			{
				// Ugh! Need to grow (and move) the directory

				// Find space (but exclude the directory entry which is first in the list)
				uint32_t newDirectoryLocation = nffsFindSpace(pRanges + (blockNumber == 0 ? 1 : 0), rangeCount - (blockNumber == 0 ? 1 : 0), rs.directoryBlockCount + 4);
				if (newDirectoryLocation == 0)
				{
					err = NFFS_ERR_SPACE;
					goto exit;
				}

				// Check we're not exceeding the file system size
				if (rs.totalBlocks != 0 && newDirectoryLocation + rs.directoryBlockCount + 4 > rs.totalBlocks)
				{
					err = NFFS_ERR_SPACE;
					goto exit;
				}

				// Move the old directory and zero the new directories
				err = nffsMoveBlocks(ctx, rs.directoryBlock, rs.directoryBlockCount, newDirectoryLocation);
				if (err)
					goto exit;
				err = nffsZeroBlocks(ctx, newDirectoryLocation + rs.directoryBlockCount, 4);
				if (err)
					goto exit;

				// Update ranges to include new directory range
				pRanges[0].block = newDirectoryLocation;
				pRanges[1].blockCount = rs.directoryBlockCount + 4;

				// Update the root sector
				rs.directoryBlock = newDirectoryLocation;
				rs.directoryBlockCount += 4;

				// Update the total number of used blocks
				if (rs.directoryBlock + rs.directoryBlockCount > rs.totalBlocksUsed)
					rs.totalBlocksUsed = rs.directoryBlock + rs.directoryBlockCount;

				// This is where the new file will go
				directoryIndex = rs.directoryHighWater;
			}
		}
	}

	// Work out where the directory will go
	directoryBlock = rs.directoryBlock + directoryIndex / NFFS_DIRENTRIES_PER_BLOCK;
	directorySubIndex = directoryIndex % NFFS_DIRENTRIES_PER_BLOCK;

	// Read directory
	pDE = (NFFSDIRENTRY*)ctx->readBlock(ctx, directoryBlock);
	if (!pDE)
		goto exit;
	
	// Move to the correct directory entry
	pDE += directorySubIndex;

	// Find space for the new file (unless supplied as a parameter
	if (blockNumber == 0)
	{
		if (cfctx.existingDirEntryFound && cfctx.fitsInExistingLocation)
		{
			blockNumber = pDE->block;
		}
		else
		{
			// Find space for the file
			blockNumber = nffsFindSpace(pRanges, rangeCount, cfctx.requiredBlocks);
			if (blockNumber == 0)
			{
				err = NFFS_ERR_SPACE;
				goto exit;
			}
		}
	}

	// Update the number of used blocks
	if (blockNumber + cfctx.requiredBlocks > rs.totalBlocksUsed)
	{
		rs.totalBlocksUsed = blockNumber + cfctx.requiredBlocks;
	}

	// Check fits
	if (rs.totalBlocks !=0 && rs.totalBlocksUsed > rs.totalBlocks)
	{
		err = NFFS_ERR_SPACE;
		goto exit;
	}

	// Setup the directory entry
	pDE->block = blockNumber;
	pDE->blockCount = cfctx.requiredBlocks;
	pDE->length = length;
	strcpy(pDE->filename, filename);
	pDE->unused = 0;

	// Return the directory entry
	memcpy(pde, pDE, sizeof(NFFSDIRENTRY));

	// Re-write the directory block
	if (!ctx->writeBlock(ctx, directoryBlock, NULL))
	{
		err = NFFS_ERR_WRITE;
		goto exit;
	}

	// Update the file count if this is a new file
	if (!cfctx.existingDirEntryFound)
	{
		rs.fileCount++;
	}

	// Update the number of used directory entries
	if (directoryIndex == rs.directoryHighWater)
	{
		rs.directoryHighWater++;
	}

	// Re-write the root sector
	memcpy(&ctx->rootSector, &rs, sizeof(rs));
	ctx->rootSector.dirty = false;
	err = nffsWriteRootSector(ctx);

	// If there was an error, remark the root sector as dirty
	if (err)
		ctx->rootSector.dirty = true;

exit:
	free(pRanges);
	return err;
}

