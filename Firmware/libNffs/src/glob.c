///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

bool nffsGlobInternal(const char* filename, const char* p);

// Check if a string matches glob pattern (case sensitive, supports '*' and '?' and multiple-patterns (eg: "*.c;*.h")
bool nffsGlob(const char* filename, const char* p)
{
	while (true)
	{
		// Test
		if (nffsGlobInternal(filename, p))
			return true;

		// Next pattern
		while (*p!=';')
		{
			if (!*p)
				return false;
			p++;
		}

		// Skip separator
		p++;
	}
}


bool nffsGlobInternal(const char* filename, const char* p)
{
	const char* f = filename;
	// Compare characters
	while (true)
	{
		// End of both strings = match!
		if ((*p=='\0' || *p==';') && *f=='\0')
			return true;

		// End of sub-pattern?
		if (*p==';' || *p=='\0' || *f=='\0')
		{
			return false;
		}

		// Single character wildcard
		if (*p=='?')
		{
			p++;
			f++;
			continue;
		}

		// Multi-character wildcard
		if (*p=='*')
		{
			p++;
            if (*p == '\0')
                return true;
			while (*f!='\0')
			{
				if (nffsGlobInternal(f, p))
					return true;
				f++;
			}
			return false;
		}

		// Same character?
		if (*p != *f)
			return false;

		// Next
		p++;
		f++;
	}
}

// Based roughly on this: https://superuser.com/questions/475874/how-does-the-windows-rename-command-interpret-wildcards
char* nffsGlobRename(const char* file, const char* pattern, char* pVal)
{
	char* o = pVal;
	const char* p = pattern;
	const char* f = file;

	if (!nffsContainsWildcards(pattern))
	{
		while (*p)
		{
			*o++ = *p++;
		}
		*o = '\0';
		return o;
	}

	while (true)
	{
		switch (*p)
		{
			case '\0':
				// End if input

				// Strip dots and spaces from end of string
				while (o > pVal && o[-1]=='.' || o[-1]==' ')
					o--;
				*o = '\0';
				return o;

			case '*':
				p++;
				switch (*p)
				{
					case '\0':
					case '?':
						// Copy rest of string
						while (*f)
						{
							*o++ = *f++;
						}
						if (*p == '?')
							p++;
						break;

					default:
					{
						// Find the last instance of the next character
						const char* last = NULL;
						const char* t = f;
						while (*t)
						{
							if (*t == *p)
								last = t;
							t++;
						}

						// Not found? copy to end
						if (!last)
							last = t;

						// Copy everything up to the last dot
						memcpy(o, f, last-f);
						o += last - f;
						f = last;

						// Copy the character from the pattern
						*o++ = *p++;
						break;
					}
				}
				break;

			case '?':
				p++;

				// Copy one character (unless on '.' in input)
				if (*f != '.' && *f != '\0')
				{
					*o++ = *f++;
				}
				break;

			case '.':
				p++;
				while (*f != '.' && *f !='\0')
					f++;
				*o++ = '.';
				if (*f == '.')
					f++;
				break;

			default:
				// Copy character to output
				*o++ = *p++;

				// Skip character in input but don't skip dots
				if (*f != '.' && *f != '\0')
					f++;
				
				break;
		}
	}
	
}
