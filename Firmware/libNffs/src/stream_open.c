///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamOpen(NFFSCONTEXT* ctx, const char* filename, bool readOnly, NFFSSTREAM** pVal)
{
	// Find the file
	NFFSDIRENTRY DE;
	int err = nffsFindFile(ctx, filename, &DE);
	if (err)
		return err;

	// Open it
	return nffsOpenStreamInternal(ctx, filename, DE.block, DE.blockCount, DE.length, readOnly, false, pVal);
}

