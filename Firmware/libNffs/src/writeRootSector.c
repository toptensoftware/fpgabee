///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Move blocks
int nffsWriteRootSector(NFFSCONTEXT* ctx)
{
	int err;

	// Write it
	void* pMem = ctx->createBlock(ctx, 0);
	if (!pMem)
	{
		err = NFFS_ERR_MEMORY;
		return err;
	}

	// Copy root sector to it
	memcpy(pMem, &ctx->rootSector, sizeof(ctx->rootSector));

	// Write it
	if (!ctx->writeBlock(ctx, 0, NULL))
	{
		err = NFFS_ERR_WRITE;
		return err;
	}

	return NFFS_OK;
}

