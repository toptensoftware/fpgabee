///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

typedef struct tagREPAIRCONTEXT
{
	NFFSCONTEXT* ctx;
	NFFSROOTSECTOR* repairedSector;
} REPAIRCONTEXT;

void mark_blocks_used(REPAIRCONTEXT* rctx, uint32_t block, uint32_t blockCount)
{
	// Update total blocks used
	if (block + blockCount > rctx->repairedSector->totalBlocksUsed)
	{
		rctx->repairedSector->totalBlocksUsed = block + blockCount;
	}
}

bool repair_callback(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	REPAIRCONTEXT* rctx = (REPAIRCONTEXT*)user;

	// Mark blocks as used
	mark_blocks_used(rctx, de->block, de->blockCount);

	// Update the directory high water...
	if (deIndex + 1 > rctx->repairedSector->directoryHighWater)
		rctx->repairedSector->directoryHighWater = deIndex + 1;

	// Update file count
	rctx->repairedSector->fileCount++;

	return true;
}


int nffsRepair(NFFSCONTEXT* ctx)
{
	int err;
	NFFSROOTSECTOR repairedSector;
	REPAIRCONTEXT rctx;

	// Read the root block
	void* pBlock = ctx->readBlock(ctx, 0);
	if (!pBlock)
		return NFFS_ERR_READ;

	// Copy the root sector
	memcpy(&ctx->rootSector, pBlock, sizeof(ctx->rootSector));

	// Check signature
	if (ctx->rootSector.signature != NFFS_SIGNATURE)
		return NFFS_ERR_INVALIDFS;

	// Check version
	if (ctx->rootSector.version!= NFFS_VERSION)
		return NFFS_ERR_VERSION;

	// Clear working
	ctx->tailBlock = 0;

	// Make a working copy of the root sector
	memcpy(&repairedSector, &ctx->rootSector, sizeof(NFFSROOTSECTOR));

	// Make sure we enumerate all directory entries
	ctx->rootSector.directoryHighWater = ctx->rootSector.directoryBlockCount * NFFS_DIRENTRIES_PER_BLOCK;

	// Reset the repaired sector
	repairedSector.fileCount = 0;
	repairedSector.totalBlocksUsed = 0;
	repairedSector.directoryHighWater = 0;

	// Setup repair context
	rctx.ctx = ctx;
	rctx.repairedSector = &repairedSector;

	// Mark directory blocks as used
	mark_blocks_used(&rctx, repairedSector.directoryBlock, repairedSector.directoryBlockCount);

	// Walk directory
	err = nffsEnumFilesInternal(ctx, repair_callback, &rctx, false);
	if (err)
		return err;

	// Clear the dirty flag and copy back to the real context
	repairedSector.dirty = false;
	memcpy(&ctx->rootSector, &repairedSector, sizeof(NFFSROOTSECTOR));

	// Re-write the root sector
	err = nffsWriteRootSector(ctx);

	// If write failed, remark it as dirty
	if (err)
		ctx->rootSector.dirty = true;

	// Done!
	return err;
}
