///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamWrite(NFFSSTREAM* stream, void* buf, uint32_t bytes)
{
	uint32_t bytesLeft;

	// Check params
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;
	if (stream->readOnly)
		return NFFS_ERR_READONLY;

	// Write bytes
	bytesLeft = bytes;
	while (bytesLeft)
	{
		// Work out which part of block we're reading
		uint32_t requiredBlock = stream->position / NFFS_BLOCK_SIZE;
		uint32_t positionInBlock = stream->position % NFFS_BLOCK_SIZE;
		uint32_t bytesInBlock = bytesLeft;
		if (positionInBlock + bytesLeft > NFFS_BLOCK_SIZE)
		{
			bytesInBlock = NFFS_BLOCK_SIZE - positionInBlock;
		}

		// Which block do we need
		if (requiredBlock != stream->currentBlock)
		{
			// Flush the current block?
			if (stream->currentBlockDirty)
			{
				int err = nffsStreamFlush(stream);
				if (err)
					return err;
			}

			if (bytesInBlock == NFFS_BLOCK_SIZE ||
				(positionInBlock == 0 && (stream->position + bytesInBlock) > stream->length))
			{
				// Zero it
				memset(stream->buffer, 0, NFFS_BLOCK_SIZE);
			}
			else
			{
				// Read the block
				void* pMem = stream->owner->readBlock(stream->owner, stream->block + requiredBlock);
				if (!pMem)
					return NFFS_ERR_READ;

				memcpy(stream->buffer, pMem, NFFS_BLOCK_SIZE);
			}

			// Update current block number
			stream->currentBlock = requiredBlock;
		}

		// Copy data into our buffer
		memcpy((char*)stream->buffer + positionInBlock, buf, bytesInBlock);

		// Remember that the current block is dirty
		stream->currentBlockDirty = true;

		// Update current position
		stream->position += bytesInBlock;
		if (stream->position > stream->length)
			stream->length = stream->position;
		buf = (char*)buf + bytesInBlock;
		bytesLeft -= bytesInBlock;
	}

	return NFFS_OK;
}
