///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamTell(NFFSSTREAM* stream, uint32_t* pVal)
{
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;

	// Return the current position
	*pVal = stream->position;
	return NFFS_OK;
}

