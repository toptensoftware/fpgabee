///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

int nffsStreamSeek(NFFSSTREAM* stream, uint32_t position)
{
	if (!stream || !stream->owner)
		return NFFS_ERR_PARAMETER;

	// Store new position (We'll validate it at read/write time)
	stream->position = position;

	return 0;
}

