///////////////////////////////////////////////////////////////////////////////////////////////////
// libNffs.h

#include "common.h"

// Tail operations
int nffsCreateTail(NFFSCONTEXT* ctx, uint32_t* pBlock, uint32_t* pAvailableBlocks)
{
	// File system clean?
	if (ctx->rootSector.dirty)
		return NFFS_ERR_NEEDSREPAIR;

	if (ctx->tailBlock)
		return NFFS_ERR_TAIL;

	// Remember the tail block
	ctx->tailBlock = ctx->rootSector.totalBlocksUsed;
	
	// Return where the app can write...
	*pBlock = ctx->tailBlock;
	*pAvailableBlocks = ctx->rootSector.totalBlocks - ctx->rootSector.totalBlocksUsed;

	return NFFS_OK;
}

// Discard the tail
int nffsDiscardTail(NFFSCONTEXT* ctx)
{
	if (!ctx->tailBlock)
		return NFFS_OK;

	// Clear tail operation
	ctx->tailBlock = 0;
	return NFFS_OK;
}

int nffsSaveTail(NFFSCONTEXT* ctx, const char* filename, uint32_t length, NFFSDIRENTRY* pde)
{
	int err;
	
	// Can't save if don't have!
	if (!ctx->tailBlock)
		return NFFS_ERR_TAIL;

	// Create the file
	err = nffsCreateFileInternal(ctx, filename, length, ctx->tailBlock, 0, pde);

	// Clear the tail block
	ctx->tailBlock = 0;

	// Done
	return err;
}
