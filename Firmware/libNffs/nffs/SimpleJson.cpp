#include "common.h"
#include "SimpleJson.h"

CJsonValue* CJsonValue::GetMember(const wchar_t* psz)
{
	CJsonMap* pMap = AsMap();
	if (!pMap)
		return NULL;
	return pMap->Get(psz, NULL);
}


int parseHexNibble(wchar_t ch)
{
	if (ch >= '0' && ch <= '9')
		return ch - '0';

	if (ch >= 'a' && ch <= 'f')
		return ch - 'a' + 0xA;

	if (ch >= 'A' && ch <= 'F')
		return ch - 'A' + 0xA;
	
	return -1;
}

bool isDigit(wchar_t ch)
{
	return ch >= '0' && ch <= '9';
}

bool isLetter(wchar_t ch)
{
	// Good enough for our purposes
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
}

bool isLetterOrDigit(wchar_t ch)
{
	return isLetter(ch) || isDigit(ch);
}

////////////////////////////////////////////////////////////////////////////////////////
// CJsonTokenizer

CJsonTokenizer::CJsonTokenizer(const wchar_t* psz, bool strict)
{
	_psz = psz;
	_p = psz;
	IsStrict = strict;
	CurrentToken = JsonToken::Null;
	NextChar();
	NextToken();
}

wchar_t CJsonTokenizer::NextChar()
{
	_currentChar = _p[0];
	if (_currentChar)
		_p++;
	return _currentChar;
}

void CJsonTokenizer::NextToken()
{
	// In error state?
	if (CurrentToken == JsonToken::Error)
		return;

	while (true)
	{
		// Skip whitespace and handle line numbers
		while (true)
		{
			if (_currentChar == '\r')
			{
				if (NextChar() == '\n')
				{
					NextChar();
				}
			}
			else if (_currentChar == '\n')
			{
				if (NextChar() == '\r')
				{
					NextChar();
				}
			}
			else if (_currentChar == ' ')
			{
				NextChar();
			}
			else if (_currentChar == '\t')
			{
				NextChar();
			}
			else
				break;
		}

		// Handle common characters first
		switch (_currentChar)
		{
			case '/':
			{
				// Comments not support in strict mode
				if (IsStrict)
				{
					return SetError(L"Comments not supported in strict mode");
				}

				// Process comment
				NextChar();
				switch (_currentChar)
				{
					case '/':
						NextChar();
						while (_currentChar != '\0' && _currentChar != '\r' && _currentChar != '\n')
						{
							NextChar();
						}
						break;

					case '*':
					{
						bool endFound = false;
						while (!endFound && _currentChar != '\0')
						{
							if (_currentChar == '*')
							{
								NextChar();
								if (_currentChar == '/')
								{
									endFound = true;
								}
							}
							NextChar();
						}
						break;

					}

					default:
						return SetError(L"syntax error, unexpected character after slash");
				}
				continue;
			}

			case '\"':
			case '\'':
			{
				_string.Empty();
				wchar_t quoteKind = _currentChar;
				NextChar();
				while (_currentChar != '\0')
				{
					if (_currentChar == '\\')
					{
						NextChar();
						wchar_t escape = _currentChar;
						switch (escape)
						{
						case '\"': _string.Append('\"'); break;
						case '\\': _string.Append('\\'); break;
						case '/': _string.Append('/'); break;
						case 'b': _string.Append('\b'); break;
						case 'f': _string.Append('\f'); break;
						case 'n': _string.Append('\n'); break;
						case 'r': _string.Append('\r'); break;
						case 't': _string.Append('\t'); break;
						case '0': _string.Append(L'\0'); break;
						case 'u':
						{
							wchar_t ch;
							for (int i = 0; i < 4; i++)
							{
								int nib = parseHexNibble(_currentChar);
								if (nib < 0)
								{
									return SetError(L"Invalid hex digit in \u escape");
								}
								ch = ch << 4 | 
								NextChar();
							}
							_string.Append(ch);
							break;
						}

						default:
							return SetError(Format(L"Invalid escape sequence in string literal: '\\%x'", _currentChar));
						}
					}
					else if (_currentChar == quoteKind)
					{
						CurrentToken = JsonToken::String;
						NextChar();
						return;
					}
					else
					{
						_string.Append(_currentChar);
					}

					NextChar();
				}
				return SetError(L"syntax error, unterminated string literal");
			}

			case '{': CurrentToken = JsonToken::OpenBrace; NextChar(); return;
			case '}': CurrentToken = JsonToken::CloseBrace; NextChar(); return;
			case '[': CurrentToken = JsonToken::OpenSquare; NextChar(); return;
			case ']': CurrentToken = JsonToken::CloseSquare; NextChar(); return;
			case '=': CurrentToken = JsonToken::Equal; NextChar(); return;
			case ':': CurrentToken = JsonToken::Colon; NextChar(); return;
			case ';': CurrentToken = JsonToken::SemiColon; NextChar(); return;
			case ',': CurrentToken = JsonToken::Comma; NextChar(); return;
			case '\0': CurrentToken = JsonToken::eof; return;
		}

		// Number?
		if (isDigit(_currentChar) || _currentChar == '-')
		{
			TokenizeNumber();
			return;
		}

		// Identifier?  (checked for after everything else as identifiers are actually quite rare in valid json)
		if (isLetter(_currentChar) || _currentChar == '_' || _currentChar == '$')
		{
			// Find end of identifier
			_string.Empty();
			while (isLetterOrDigit(_currentChar) || _currentChar == '_' || _currentChar == '$')
			{
				_string.Append(_currentChar);
				NextChar();
			}

			// Handle special identifiers
			if (IsEqualString(_string, L"true"))
			{
				CurrentToken = JsonToken::Bool;
				_bool = true;
				return;
			}

			if (IsEqualString(_string, L"false"))
			{
				CurrentToken = JsonToken::Bool;
				_bool = false;
				return;
			}
			if (IsEqualString(_string, L"null"))
			{
				CurrentToken = JsonToken::Null;
				return;
			}
			if (IsEqualString(_string, L"undefined"))
			{
				CurrentToken = JsonToken::Undefined;
				return;
			}

			CurrentToken = JsonToken::Identifier;
			return;
		}

		// What the?
		SetError(Format(L"syntax error, unexpected character '%c'", _currentChar));
	}
}


// Parse a sequence of characters that could make up a valid number
// For performance, we don't actually parse it into a number yet.  When using PetaJsonEmit we parse
// later, directly into a value type to avoid boxing
void CJsonTokenizer::TokenizeNumber()
{
	_string.Empty();

	// Leading negative sign
	bool isSigned = false;
	if (_currentChar == '-')
	{
		isSigned = true;
		_string.Append(_currentChar);
		NextChar();
	}

	// Hex prefix?
	bool hex = false;
	if (_currentChar == '0' && !IsStrict)
	{
		_string.Append(_currentChar);
		NextChar();
		if (_currentChar == 'x' || _currentChar == 'X')
		{
			_string.Append(_currentChar);
			NextChar();
			hex = true;
		}
	}

	// Process characters, but vaguely figure out what type it is
	bool cont = true;
	bool fp = false;
	while (cont)
	{
		switch (_currentChar)
		{
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			_string.Append(_currentChar);
			NextChar();
			break;

		case 'A':
		case 'a':
		case 'B':
		case 'b':
		case 'C':
		case 'c':
		case 'D':
		case 'd':
		case 'F':
		case 'f':
			if (!hex)
				cont = false;
			else
			{
				_string.Append(_currentChar);
				NextChar();
			}
			break;

		case '.':
			if (hex)
			{
				cont = false;
			}
			else
			{
				fp = true;
				_string.Append(_currentChar);
				NextChar();
			}
			break;

		case 'E':
		case 'e':
			if (!hex)
			{
				fp = true;
				_string.Append(_currentChar);
				NextChar();
				if (_currentChar == '+' || _currentChar == '-')
				{
					_string.Append(_currentChar);
					NextChar();
				}
			}
			break;

		default:
			cont = false;
			break;
		}
	}

	if (isLetter(_currentChar))
		return SetError(Format(L"syntax error, invalid character following number '%c'", _currentChar));

	// Setup token
	CurrentToken = JsonToken::Number;

	if (!hex)
	{
		_number = _wtof(_string);
	}
	else
	{
		uint64_t val = 0;
		for (int i = 2; i < _string.GetLength(); i++)
		{
			val = val << 4 | parseHexNibble(_string[i]);
		}
		_number = (double)val;
	}
}

void CJsonTokenizer::SetError(const wchar_t* pszError)
{
	CurrentToken = JsonToken::Error;
	_string = pszError;
}

bool CJsonTokenizer::Check(JsonToken::value tokenRequired)
{
	if (tokenRequired != CurrentToken)
	{
		if (CurrentToken == JsonToken::eof)
			SetError(L"syntax error, unexpected end of data");
		else
			SetError(L"syntax error, unexpected token");
		return false;
	}

	return true;
}

// Skip token which must match
bool CJsonTokenizer::Skip(JsonToken::value tokenRequired)
{
	if (!Check(tokenRequired))
		return false;
	NextToken();
	return true;
}

// Skip token if it matches
bool CJsonTokenizer::SkipIf(JsonToken::value tokenRequired)
{
	if (tokenRequired == CurrentToken)
	{
		NextToken();
		return true;
	}
	return false;
}

CJsonValue* JsonParseArray(CJsonTokenizer& tok)
{
	// Open brace
	if (!tok.Skip(JsonToken::OpenSquare))
		return NULL;

	CAutoPtr<CJsonArray> pArray = new CJsonArray();

	while (tok.CurrentToken != JsonToken::CloseSquare)
	{
		// Get the value
		CJsonValue* pVal = JsonParse(tok);
		if (!pVal)
			return NULL;

		// Add to the array
		pArray->Add(pVal);

		// Another?
		if (tok.CurrentToken == JsonToken::Comma)
		{
			tok.NextToken();

			if (tok.CurrentToken == JsonToken::CloseSquare && !tok.IsStrict)
			{ 
				tok.NextToken();
				return pArray.Detach();
			}
		}
		else
		{
			break;
		}
	}

	if (!tok.Skip(JsonToken::CloseSquare))
		return NULL;

	return pArray.Detach();
}

CJsonValue* JsonParseMap(CJsonTokenizer& tok)
{
	// Open brace
	if (!tok.Skip(JsonToken::OpenBrace))
		return NULL;

	CAutoPtr<CJsonMap> pMap = new CJsonMap();

	while (tok.CurrentToken != JsonToken::CloseBrace)
	{
		switch (tok.CurrentToken) 
		{
			case JsonToken::Identifier:
				if (tok.IsStrict)
				{
					tok.SetError(L"Object keys must be quoted");
					return NULL;
				}
				break;

			case JsonToken::String:
				break;

			default:
				tok.SetError(L"Expected object key");
				return NULL;
		}

		// Save the key
		CUniString strKey = tok.GetStringValue();
		tok.NextToken();

		// check key not already added
		if (pMap->HasKey(strKey))
		{
			tok.SetError(L"duplicate key in object");
			return NULL;
		}

		// Skip the colon
		if (!tok.Skip(JsonToken::Colon))
			return NULL;

		// Get the value
		CJsonValue* pVal = JsonParse(tok);
		if (!pVal)
			return NULL;

		// Add to the array
		pMap->Add(strKey, pVal);

		// Another?
		if (tok.CurrentToken == JsonToken::Comma)
		{
			tok.NextToken();

			if (tok.CurrentToken == JsonToken::CloseBrace && !tok.IsStrict)
			{
				tok.NextToken();
				return pMap.Detach();
			}
		}
		else
		{
			break;
		}
	}

	if (!tok.Skip(JsonToken::CloseBrace))
		return NULL;

	return pMap.Detach();
}

CJsonValue* JsonParse(CJsonTokenizer& tok)
{
	CAutoPtr<CJsonValue> pVal;
	switch (tok.CurrentToken)
	{
		case JsonToken::Error:
			return NULL;

		case JsonToken::eof:
			return NULL;

		case JsonToken::Bool:
			pVal = new CJsonBool(tok.GetBoolValue());
			tok.NextToken();
			break;

		case JsonToken::String:
			pVal = new CJsonString(tok.GetStringValue());
			tok.NextToken();
			break;

		case JsonToken::Number:
			pVal = new CJsonNumber(tok.GetNumberValue());
			tok.NextToken();
			break;

		case JsonToken::Null:
			pVal = new CJsonNull();
			tok.NextToken();
			break;

		case JsonToken::Undefined:
			pVal = new CJsonUndefined();
			tok.NextToken();
			break;

		case JsonToken::OpenBrace:
			pVal = JsonParseMap(tok);
			break;

		case JsonToken::OpenSquare:
			pVal = JsonParseArray(tok);
			break;

		default:
			tok.SetError(L"syntax error, unexpected token");
			return NULL;
	}

	return pVal.Detach();
}

CJsonValue* JsonParse(const wchar_t* psz, bool strict)
{
	// Setup tokenizer
	CJsonTokenizer tok(psz, strict);

	// Parse a value
	CAutoPtr<CJsonValue> pVal = JsonParse(tok);

	if (!pVal)
	{
		return new CJsonError(tok.GetStringValue(), tok);
	}

	// Check EOF
	if (tok.CurrentToken != JsonToken::eof)
	{
		return new CJsonError(L"Unexpected token after JSON", tok);
	}

	// Return value
	return pVal.Detach();
}

CJsonValue* JsonParseFile(const wchar_t* psz, bool strict)
{
	// Open the file
	FILE* pFile = _wfopen(psz, L"rb");
	if (!pFile)
		return NULL;

	// Work out length
	fseek(pFile, 0, SEEK_END);
	long len = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	// Read the string
	CAnsiString str;
	if (fread(str.GetBuffer(len), len, 1, pFile) != 1)
	{
		fclose(pFile);
		return NULL;
	}

	fclose(pFile);

	// Convert it
	return JsonParse(utf8_2_w(str), strict);
}


void JsonStringEncode(CUniString& buf, CUniString str)
{
	buf.Append(L"\"");

	for (int i = 0; i < str.GetLength(); i++)
	{
		wchar_t ch = str[i];
		switch (ch)
		{
		case '\0':
			buf.Append(L"\\0");
			continue;

		case '\r':
			buf.Append(L"\\r");
			continue;

		case '\n':
			buf.Append(L"\\n");
			continue;

		case '\t':
			buf.Append(L"\\t");
			continue;

		case '\\':
			buf.Append(L"\\\\");
			continue;

		case '\"':
			buf.Append(L"\\\"");
			continue;

		case '\'':
			buf.Append(L"\\\'");
			continue;
		}

		if (ch >= 0 && ch <= 0x1f || ch >= 0x7f && ch <= 0x9f || ch == 0x2028 || ch == 0x2029)
		{
			buf.Append(Format(L"\u{%.4x}"));
			continue;
		}

		buf.Append(ch);
	}
	buf.Append(L"\"");
}

void AppendIndent(CUniString& buf, int depth)
{
	for (int i = 0; i < depth * 4; i++)
		buf.Append(' ');
}

void JsonFormat(CUniString& buf, int indent, CJsonValue* pVal)
{
	switch (pVal->GetType())
	{
		case JsonValueType::Null:
			buf.Append(L"null");
			return;

		case JsonValueType::Undefined:
			buf.Append(L"undefined");
			return;

		case JsonValueType::Bool:
			buf.Append(pVal->AsBool()->GetValue() ? L"true" : L"false");
			return;

		case JsonValueType::Number:
			buf.Append(Format(L"%.10g", pVal->AsNumber()->GetValue()));
			return;

		case JsonValueType::String:
			JsonStringEncode(buf, pVal->AsString()->GetValue());
			return;

		case JsonValueType::Array:
		{
			CJsonArray* pArray = pVal->AsArray();

			if (indent >= 0)
			{
				buf.Append(L"[\n");
				indent++;
				for (int i = 0; i < pArray->GetSize(); i++)
				{
					AppendIndent(buf, indent);

					JsonFormat(buf, indent, pArray->GetAt(i));
					if (i + 1 < pArray->GetSize())
						buf.Append(L",");

					buf.Append(L"\n");
				}
				indent--;
				AppendIndent(buf, indent);
				buf.Append(L']');
			}
			else
			{
				buf.Append(L'[');
				for (int i = 0; i < pArray->GetSize(); i++)
				{
					JsonFormat(buf, indent, pArray->GetAt(i));
					if (i + 1 < pArray->GetSize())
						buf.Append(L",");

				}
				buf.Append(L']');
			}
			return;
		}
		
		case JsonValueType::Map:	
		{
			CJsonMap* pMap = pVal->AsMap();

			if (indent >= 0)
			{
				buf.Append(L"{\n");
				indent++;
				for (int i = 0; i < pMap->GetSize(); i++)
				{
					AppendIndent(buf, indent);

					JsonStringEncode(buf, (*pMap)[i].Key);
					buf.Append(L": ");
					JsonFormat(buf, indent, (*pMap)[i].Value);
					if (i + 1 < pMap->GetSize())
						buf.Append(L",");

					buf.Append(L"\n");
				}
				indent--;
				AppendIndent(buf, indent);
				buf.Append(L"}");
				return;
			}
			else
			{
				buf.Append(L"{");
				for (int i = 0; i < pMap->GetSize(); i++)
				{
					JsonStringEncode(buf, (*pMap)[i].Key);
					buf.Append(L":");
					JsonFormat(buf, indent, (*pMap)[i].Value);
					if (i + 1 < pMap->GetSize())
						buf.Append(L",");
				}
				buf.Append(L"}");
				return;
			}
		}

		case JsonValueType::Error:
			buf.Append(L"##ERROR##");
			return;
	}
}

CUniString JsonFormat(CJsonValue* pVal, bool whitespace)
{
	CUniString buf;
	JsonFormat(buf, whitespace ? 0 : -1, pVal);
	return buf;
}



void SimpleJsonTest()
{
	//	CJsonValue* pVal = JsonParse(L"{apples:98765.123, pears:\"yel\\r\\n\\tlow\", }", false);
	CJsonValue* pVal = JsonParse(L"{ x: [ 1, 2, 3 ], y: { blah: 99, deblah:102 } }", false);

	CUniString str = JsonFormat(pVal, true);

	int x = 3;
}
