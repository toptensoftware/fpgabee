#ifndef _COMMON_H
#define _COMMON_H

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#ifdef _WIN32
#include "dirent.h"
#include <Windows.h>
#include <WinIoCtl.h>
#else
#include <dirent.h>
#include <errno.h>
#endif

#include "SimpleLib.h"
using namespace Simple;

#include "../include/libNffs.h"

#pragma warning(disable:4996)		// This function or variable may be unsafe. Consider using...

#ifdef _WIN32
#define PATHSEP '\\'
#else
#define PATHSEP '/'
#endif

#endif // _COMMON_H