#include "common.h"
#include "nffsFile.h"
#include "SimpleJson.h"
#ifdef _WIN32
#include <io.h>
#endif

uint32_t GetVolumeSize(FILE* pVolume, bool* pbRemoveable);

void* streamReadBlock(struct tagNFFSCONTEXT* ctx, uint32_t block)
{
	// Get self
	NFFSFS* pfs = (NFFSFS*)ctx->user;

	// Different block?
	if (pfs->currentBlock != block)
	{
		if (pfs->currentBlockLocked)
			return NULL;

		// Read it
		fseek(pfs->pUnderlying, block * NFFS_BLOCK_SIZE, SEEK_SET);
		pfs->currentBlock = block;
		fread(pfs->buffer, NFFS_BLOCK_SIZE, 1, pfs->pUnderlying);
	}

	// Return buffer...
	return pfs->buffer;
}

void* streamCreateBlock(struct tagNFFSCONTEXT* ctx, uint32_t block)
{
	// Get self
	NFFSFS* pfs = (NFFSFS*)ctx->user;

	if (pfs->currentBlockLocked)
		return NULL;

	// Setup the current buffer
	pfs->currentBlock = block;
	memset(pfs->buffer, 0, NFFS_BLOCK_SIZE);

	// Return it
	return pfs->buffer;
}

bool streamWriteBlock(struct tagNFFSCONTEXT* ctx, uint32_t block, void* pBuf)
{
	// Get self
	NFFSFS* pfs = (NFFSFS*)ctx->user;

	// Make sure writing the correct thing
	assert(pBuf!=NULL || block == pfs->currentBlock);

	// Work out buffer
	if (pBuf == NULL)
		pBuf = pfs->buffer;

	// Write it
	fseek(pfs->pUnderlying, block * NFFS_BLOCK_SIZE, SEEK_SET);
	pfs->currentBlock = block;
	return fwrite(pfs->buffer, NFFS_BLOCK_SIZE, 1, pfs->pUnderlying) == 1;
}

void streamLockBlock(struct tagNFFSCONTEXT* ctx, uint32_t block, bool lock)
{
	// Get self
	NFFSFS* pfs = (NFFSFS*)ctx->user;

	assert(block == pfs->currentBlock);

	if (lock)
	{
		assert(!pfs->currentBlockLocked);
		pfs->currentBlockLocked = true;
	}
	else
	{
		assert(pfs->currentBlockLocked);
		pfs->currentBlockLocked = false;
	}
}

static int nffsStreamInitInternal(FILE* pUnderlying, bool readOnly, NFFSFS** pVal)
{
	// Allocate memory
	NFFSFS* pfs = (NFFSFS*)malloc(sizeof(NFFSFS));
	if (!pfs)
		return NFFS_ERR_MEMORY;

	// Setup context
	pfs->ctx.user = pfs;
	pfs->ctx.createBlock = streamCreateBlock;
	pfs->ctx.readBlock = streamReadBlock;
	pfs->ctx.writeBlock = streamWriteBlock;
	pfs->ctx.lockBlock = streamLockBlock;

	// Store underlying stream
	pfs->pUnderlying = pUnderlying;
	pfs->currentBlock = 0xFFFFFFFF;
	pfs->readOnly = readOnly;
	pfs->currentBlockLocked = false;

	// Setup return
	*pVal = pfs;
	return NFFS_OK;
}

int nffsStreamInit(FILE* pUnderlying, bool readOnly, NFFSFS** pVal)
{
	// Do common init
	NFFSFS* pfs;
	int err = nffsStreamInitInternal(pUnderlying, readOnly, &pfs);

	// Initialize
	err = nffsInit(&pfs->ctx);
	if (err)
	{
		free(pfs);
		return err;
	}

	// Done
	*pVal = pfs;
	return NFFS_OK;
}

int nffsStreamRepairAndInit(FILE* pUnderlying, NFFSFS** pVal)
{
	// Do common init
	NFFSFS* pfs;
	int err = nffsStreamInitInternal(pUnderlying, false, &pfs);

	// Initialize
	err = nffsRepair(&pfs->ctx);
	if (err)
	{
		free(pfs);
		return err;
	}

	// Done
	*pVal = pfs;
	return NFFS_OK;
}

int nffsStreamInitNew(FILE* pUnderlying, uint32_t directoryBlocks, NFFSFS** pVal)
{
	// Work out total blocks from volume size
	bool removeable;
	uint32_t totalBlocks = GetVolumeSize(pUnderlying, &removeable);

	if (totalBlocks != 0xFFFFFFFF)
	{
		printf("Formatting volume with %u blocks (%.1fGb)...\n", totalBlocks, (double)totalBlocks * 512 / 1024 / 1024 / 1024);
		if (!removeable)
		{
			printf("\nVolume is NOT removeable, refusing to format.\n\n");
			return NFFS_ERR_INVALIDFS;
		}
	}

	// Do common init
	NFFSFS* pfs;
	int err = nffsStreamInitInternal(pUnderlying, false, &pfs);

	// Initialize
	err = nffsInitNew(&pfs->ctx, directoryBlocks, totalBlocks);
	if (err)
	{
		free(pfs);
		return err;
	}

	// Done
	*pVal = pfs;
	return NFFS_OK;
}

int nffsStreamUninit(NFFSFS* pFS, bool closeUnderlying)
{
	// Really?
	if (!pFS)
		return NFFS_OK;

	// Close underlying stream
	if (closeUnderlying)
		fclose(pFS->pUnderlying);

	// Free memory
	free(pFS);
	return NFFS_OK;
}

#ifdef _WIN32

uint32_t GetVolumeSize(FILE* pVolume, bool* pbRemovable)
{
	// Get the file descriptor from the stream
	int fd =_fileno(pVolume);
	if (fd == -1)
		return 0xFFFFFFFF;
	
	// Get the file handle
	HANDLE hVolume = (HANDLE)_get_osfhandle(fd);
	if (hVolume == NULL || hVolume == INVALID_HANDLE_VALUE)
		return 0xFFFFFFFF;

	// Get disk geometry
	DISK_GEOMETRY geo;
	DWORD cbBytes;
	if (!DeviceIoControl(hVolume, IOCTL_DISK_GET_DRIVE_GEOMETRY, NULL, 0, &geo, sizeof(geo), &cbBytes, NULL))
		return 0xFFFFFFFF;

	// Removeable?
	*pbRemovable = geo.MediaType != FixedMedia && geo.MediaType != Unknown;

	return geo.Cylinders.LowPart * geo.TracksPerCylinder * geo.SectorsPerTrack;
}

CAnsiString MapVolumeAlias(const char* filename)
{
	CUniString strConfigFile = Format(L"%s\\.nffs\\config.json", _wgetenv(L"HOME"));

	CAutoPtr<CJsonValue> pConfig = JsonParseFile(strConfigFile, false);
	if (pConfig)
	{
		if (pConfig->AsError())
		{
			printf("WARNING: JSON parse error %S", pConfig->AsError()->GetMessage().sz());
		}
		else
		{
			CJsonString* pAlias = pConfig->GetMember(L"Volumes")->GetMember(a2w(filename))->AsString();
			if (pAlias)
				return pAlias->GetValue();
		}
	}

	if (filename[1] == ':' && filename[2] == '\0')
	{
		// Convert to volumne name
		if (isdigit(filename[0]))
			return Format("\\\\.\\PhysicalDrive%c", filename[0]);
		else
			return Format("\\\\.\\%c:", filename[0]);
	}

	return NULL;
}


int errNoFromGetLastError()
{
	switch (GetLastError())
	{
		case ERROR_ACCESS_DENIED: return EACCES;
		case ERROR_ALREADY_EXISTS: return EEXIST;
		case ERROR_BAD_DEVICE: return ENODEV;
		case ERROR_BAD_PATHNAME: return ENOENT;
		case ERROR_BAD_USERNAME: return EINVAL;
		case ERROR_CANCELLED: return EINTR;
		case ERROR_DEVICE_DOOR_OPEN: return EIO;
		case ERROR_DEVICE_IN_USE: return EAGAIN;
		case ERROR_DEVICE_REQUIRES_CLEANING: return EIO;
		case ERROR_DEV_NOT_EXIST: return ENOENT;
		case ERROR_DIRECTORY: return ENOTDIR;
		case ERROR_DIR_NOT_EMPTY: return ENOTEMPTY;
		case ERROR_DISK_CORRUPT: return EIO;
		case ERROR_DISK_FULL: return ENOSPC;
		case ERROR_DS_GENERIC_ERROR: return EIO;
		case ERROR_DUP_NAME: return EEXIST;
		case ERROR_FILEMARK_DETECTED: return EIO;
		case ERROR_FILENAME_EXCED_RANGE: return ENAMETOOLONG;
		case ERROR_FILE_CORRUPT: return EEXIST;
		case ERROR_FILE_EXISTS: return EEXIST;
		case ERROR_FILE_INVALID: return ENXIO;
		case ERROR_FILE_NOT_FOUND: return ENOENT;
		case ERROR_HANDLE_DISK_FULL: return ENOSPC;
		case ERROR_HANDLE_EOF: return ENODATA;
		case ERROR_INVALID_ADDRESS: return EINVAL;
		case ERROR_INVALID_HANDLE: return EBADF;
		case ERROR_INVALID_NAME: return ENOENT;
		case ERROR_INVALID_PARAMETER: return EINVAL;
		case ERROR_INVALID_SIGNAL_NUMBER: return EINVAL;
		case ERROR_IO_DEVICE: return EIO;
		case ERROR_IO_INCOMPLETE: return EAGAIN;
		case ERROR_IO_PENDING: return EAGAIN;
		case ERROR_LOCK_VIOLATION: return EBUSY;
		case ERROR_NEGATIVE_SEEK: return EINVAL;
		case ERROR_NOACCESS: return EFAULT;
		case ERROR_NOT_ENOUGH_MEMORY: return ENOMEM;
		case ERROR_NOT_OWNER: return EPERM;
		case ERROR_NOT_READY: return ENOENT;
		case ERROR_NOT_SAME_DEVICE: return EXDEV;
		case ERROR_NOT_SUPPORTED: return ENOSYS;
		case ERROR_NO_MEDIA_IN_DRIVE: return ENOENT;
		case ERROR_NO_SYSTEM_RESOURCES: return EFBIG;
		case ERROR_OPEN_FAILED: return EIO;
		case ERROR_OPEN_FILES: return EAGAIN;
		case ERROR_OUTOFMEMORY: return ENOMEM;
		case ERROR_PATH_NOT_FOUND: return ENOENT;
		case ERROR_SECTOR_NOT_FOUND: return EINVAL;
		case ERROR_SEEK: return EINVAL;
		case ERROR_SHARING_BUFFER_EXCEEDED: return ENOLCK;
		case ERROR_SHARING_VIOLATION: return EBUSY;
		case ERROR_TOO_MANY_OPEN_FILES: return EMFILE;
		case ERROR_WRITE_PROTECT: return EROFS;
	}
	return -1;
}

int nffsOpenFileOrVolume(const char* filename, bool create, bool readOnly, FILE** pVal)
{
	CAnsiString strVolName = MapVolumeAlias(filename);
	if (!strVolName.IsEmpty())
	{
		HANDLE vol_handle = CreateFileA(strVolName, GENERIC_READ | (readOnly ? 0 : GENERIC_WRITE),
			FILE_SHARE_READ | (readOnly ? 0 : FILE_SHARE_WRITE), NULL,
			OPEN_EXISTING,
			FILE_FLAG_NO_BUFFERING | FILE_FLAG_RANDOM_ACCESS,
			NULL);

		if (vol_handle == INVALID_HANDLE_VALUE)
		{
			int err = errNoFromGetLastError();
			if (err)
			{
				errno = err;
				return NFFS_ERR_ERRNO;
			}
			return NFFS_ERR_UNKNOWN;
		}

		int crt_handle = _open_osfhandle((intptr_t)vol_handle, 0);
		*pVal = _fdopen(crt_handle, readOnly ? "rb" : "rb+");
		if (*pVal == NULL)
		{
			CloseHandle(vol_handle);
		}
		return 0;
	}


	*pVal = fopen(filename, create ? "wb+" : (readOnly ? "rb" : "rb+"));
	if (!*pVal)
		return NFFS_ERR_ERRNO;

	return 0;
}


#endif


int nffsStreamInit(const char* filename, bool readOnly, NFFSFS** pVal)
{
	// Open file
	FILE* file;
	int err = nffsOpenFileOrVolume(filename, false, readOnly, &file);
	if (err)
		return err;

	// Connect streams
	err = nffsStreamInit(file, readOnly, pVal);

	if (err)
		fclose(file);
	return err;
}

int nffsStreamRepairAndInit(const char* filename, NFFSFS** pVal)
{
	FILE* file = NULL;
	int err = nffsOpenFileOrVolume(filename, false, false, &file);
	if (err)
		return err;

	if (!file)
		return NFFS_ERR_ERRNO;

	// Connect streams
	err = nffsStreamRepairAndInit(file, pVal);

	if (err)
		fclose(file);
	return err;
}

/*
int nffsStreamInitNew(const char* filename, uint32_t directoryBlocks, NFFSFS** pVal)
{
	FILE* file = NULL;
#ifdef _WIN32
	file = OpenVolume(filename, false);
#endif
	if (!file)
		file = fopen(filename, "wb+");

	if (!file)
		return NFFS_ERR_ERRNO;

	// Connect streams
	int err = nffsStreamInitNew(file, directoryBlocks, pVal);

	if (err)
		fclose(file);
	return err;
}
*/
