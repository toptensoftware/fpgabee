; FpgaBee
;
; Copyright (C) 2012-2013 Topten Software.
; All Rights Reserved
; 
; Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
; product except in compliance with the License. You may obtain a copy of the License at
; 
; http://www.apache.org/licenses/LICENSE-2.0
; 
; Unless required by applicable law or agreed to in writing, software distributed under 
; the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
; ANY KIND, either express or implied. See the License for the specific language governing 
; permissions and limitations under the License.

;------------------------------------------------------------------------------------------
; BootRom
; -------
; 
; This is the boot rom that sits on the FPGA fabric.  It's job is to:
; 
; 1. Wait for SD drive to initialise
; 2. Read the boot sector and check if FBFS sector
; 3. If not, see if it's a MBR boot sector and see if first partition is FBFS
; 4. Once found FBFS partition, read the system image into memory at 0x8000
; 5. Setup a thunk to copy the system image to 0x0000
; 6. Jump to 0x0100 to start the system image
;
;------------------------------------------------------------------------------------------


;------------------------------------------------------------------------------------------
; Constants

; Ports
PORT_DISK_DATA:						EQU 0xC0	; read/write
PORT_DISK_STATUS:					EQU	0xC7	; read
PORT_DISK_COMMAND:					EQU 0xC7	; write
PORT_DISK_ADDRESS:					EQU 0xC1

; Status bits
DISK_STATUS_BIT_BUSY:				EQU 0x80 	; Disk controller is busy
DISK_STATUS_BIT_INITIALIZED:		EQU 0x40    ; Disk controller has initialized

; Basic Commands
DISK_COMMAND_READ:					EQU 1		; Read command

; FBFS Config Sector
FBFS_SIG:				EQU		0			; "fbfs"
FBFS_VER:				EQU		4			; 1
FBFS_DIRBLK:			EQU		6			; First block of directory table
FBFS_DIRCOUNT:			EQU		10			; Number of blocks in directory table
FBFS_DI_SYSTEM:			EQU		12			; Directory Index of system image
FBFS_DI_ROM_0:			EQU		14			; Selected Rom Packs
FBFS_DI_ROM_1:			EQU		16			; 
FBFS_DI_ROM_2:			EQU		18			; 
FBFS_DI_DISK_0:			EQU		20			; Selected disks
FBFS_DI_DISK_1:			EQU		22
FBFS_DI_DISK_2:			EQU		24
FBFS_DI_DISK_3:			EQU		26
FBFS_DI_DISK_4:			EQU		28
FBFS_DI_DISK_5:			EQU		30
FBFS_DI_DISK_6:			EQU		32
FBFS_CONFIG_SIZE:		EQU		34			; Size of config data
	
; FBFS Directory structure
FBFS_DIR_BLOCK:			EQU		0			; Block number where file starts
FBFS_DIR_BLOCK_COUNT:	EQU		4			; Number of blocks in file
FBFS_DIR_RESERVED:		EQU		6			; Unused
FBFS_DIR_FILENAME:		EQU		10			; Filename
FBFS_DIR_SIZE:			EQU		32

FBFS_MAX_FILENAME:		EQU		22			; Maximum length of filename (including the NULL)

; Port D0 memory bank/rom latch
;
; Bit 7 - boot rom latch (1 on boot)
; Bit 6 - unused
; Bit 5 - unused
; Bit 4 - MicroBee RAM/ROM latch
; Bits 3..0  = high 4 bits of the 18-bit external ram address
;				(Gives PCU access to all external RAM in 16k Pages)

PORT_MEMORY_BANK:					EQU 0xD0	; write
MEMORY_BANK_BOOT_LATCH:				EQU 0x80
MEMORY_BANK_RAM_LATCH:				EQU 0x10
MEMORY_BANK_ADDR:					EQU 0x8000

; Debug ports
PORT_LED_REG:						EQU 0xA0	; write
PORT_HEX_LO_REG:					EQU 0xA1	; write
PORT_HEX_HI_REG:					EQU 0xA2	; write

; MBR related

; Offset in the boot sector to the partition table
MBR_OFFSET_PARTITION_TABLE:	EQU  0x1BE

; Offsets into a partition table entry
MBR_OFFSET_PARTITION_ENTRY_STATUS:		EQU 0		; 0x80 = bootable
MBR_OFFSET_PARTITION_ENTRY_START:		EQU	1 		; 3 byte CHS address
MBR_OFFSET_PARTITION_ENTRY_PARTTYPE: 	EQU 4		; 
MBR_OFFSET_PARTITION_ENTRY_END: 		EQU 5 		; 3 byte CHS address
MBR_OFFSET_PARTITION_FIRST_SECTOR: 		EQU 8 		; 4 bytes
MBR_OFFSET_PARTITION_SECTOR_COUNT: 		EQU 12 		; 4 bytes


;------------------------------------------------------------------------------------------
; Data Segment

; This has to be first cause it get's overwritten
ORG						0x6000
SECTOR_BUFFER:			DEFS	512
FBFS_ROOT_SECTOR:		DEFS	4
FBFS_CONFIG:			DEFS	FBFS_CONFIG_SIZE
DIRECTORY:				DEFS	0

; Instruct assembler to rewind output position to overwrite the above global viables
SEEK					0


;------------------------------------------------------------------------------------------
; Code

; Execution begins here
	ORG		0
	jp		start

; Interrupt table - ignore NMI during boot and poll for disk completion
	defs	0x66-$
	RETN
	defs	0x100-$

start:

	; Setup Stack
	LD 	    SP,0x8000

	; Wait for SD startup
wait_sd_card:
	IN		A,(PORT_DISK_STATUS)
	AND		DISK_STATUS_BIT_INITIALIZED
	JR		Z,wait_sd_card

	; Read the boot sector
	ld		HL,SECTOR_BUFFER
	ld		BC,0
	ld      DE,0
	call    DISK_READ


	; Is it the FBFS root sector?
	ld		HL,SECTOR_BUFFER
	call	IS_FBFS
	jr		z,FBFS_FOUND


	; Nope, probably partition table.
	; Look for primary partition
	ld		IY,SECTOR_BUFFER + MBR_OFFSET_PARTITION_TABLE
	ld		C,(IY + MBR_OFFSET_PARTITION_FIRST_SECTOR)
	ld		B,(IY + MBR_OFFSET_PARTITION_FIRST_SECTOR + 1)
	ld		E,(IY + MBR_OFFSET_PARTITION_FIRST_SECTOR + 2)
	ld		D,(IY + MBR_OFFSET_PARTITION_FIRST_SECTOR + 3)
	ld		HL,SECTOR_BUFFER
	call	DISK_READ

	ld		HL,SECTOR_BUFFER
	call	IS_FBFS
	jr		z,FBFS_FOUND

	; Show error
	ld		A,0x81
	out		(PORT_LED_REG),A
	jr		$


FBFS_FOUND:
	; FBFS file system found, starting in sector DEBC
	; and config sector loaded into SECTOR_BUFFER

	; Store the root sector
	ld		(FBFS_ROOT_SECTOR),BC
	ld		(FBFS_ROOT_SECTOR+2),DE

	; Store config sector
	ld		HL,SECTOR_BUFFER
	ld		DE,FBFS_CONFIG
	ld		BC,FBFS_CONFIG_SIZE
	ldir	

	; Read the directory
	call 	READ_DIRECTORY

	; Get the system directory index
	ld		DE,(FBFS_CONFIG + FBFS_DI_SYSTEM)

	; Check it's set (high bit not set)
	ld		A,D
	and		0x80
	jr		z,have_image

	; Show error
	ld		A,0x82
	out		(PORT_LED_REG),A
	jr		$

have_image:

	; Multiply the directory index by 32
	ex		DE,HL
	add		HL,HL		; * 2
	add		HL,HL		; * 4
	add		HL,HL		; * 8
	add		HL,HL		; * 16
	add		HL,HL		; * 32
	ld		DE,DIRECTORY
	add		HL,DE

	; Read the PCU rom image
	push	HL
	pop		IY
	ld		A,(IY + FBFS_DIR_BLOCK_COUNT)
	ld		HL,0x8000
	call 	READ_BLOCKS

	; Copy thunk
	push	HL
	ld		HL,CHAIN_THUNK_START
	ld		DE,0xFF00
	ld		BC,CHAIN_THUNK_END - CHAIN_THUNK_START
	ldir	
	pop		HL

	; BC = number of bytes loaded
	ld		A,H
	and		0x7F
	ld		B,A
	ld		C,L

	; DE = destination
	ld		DE,0

	; HL = source
	ld		HL,0x8000


	jp		0xFF00

CHAIN_THUNK_START:
	; Turn off boot rom
	ld		A,0
	out		(PORT_MEMORY_BANK),A

	; Copy loaded system image
	ldir

	; Pass FBFS root sector in DEBC
	ld		BC,(FBFS_ROOT_SECTOR)
	ld		DE,(FBFS_ROOT_SECTOR+2)

	; Entry point
	jp		0x100
CHAIN_THUNK_END:


	; Read all the directory entries
READ_DIRECTORY:
	ld 		IY,FBFS_CONFIG + FBFS_DIRBLK
	ld		HL,DIRECTORY
	ld		A,(FBFS_CONFIG + FBFS_DIRCOUNT)
	jr		READ_BLOCKS


; Read multiple blocks
; (IY) = 32-bit block number
; HL = destination
; A = number of blocks
READ_BLOCKS:

	; Work out: DEBC = FBFS_ROOT_SECTOR + (IY)
	push	HL
	ld		L,(IY+0)
	ld		H,(IY+1)
	ld		DE,(FBFS_ROOT_SECTOR)
	add		HL,DE
	push	HL
	ld		L,(IY+2)
	ld		H,(IY+3)
	ld		DE,(FBFS_ROOT_SECTOR + 2)
	adc		HL,DE
	push	HL
	pop		DE
	pop		BC
	pop		HL

rd_loop:
	; Save counter
	push	af

	; Read block
	call	DISK_READ

	; Increment block number
	push	HL
	ld		HL,1
	add		HL,BC
	push	HL
	pop		BC
	ld		HL,0
	adc		HL,DE
	ex		DE,HL
	pop		HL

	; Loop
	pop		AF
	sub		1
	jr		nz,rd_loop

	ret


; HL = pointer
; Returns Z if HL points to 'fbfs' file system signature
IS_FBFS:
	ld 		A,(HL)
	cp		'f'
	jr		nz,is_fbfs_ret

	inc		HL
	ld 		A,(HL)
	cp		'b'
	jr		nz,is_fbfs_ret

	inc		HL
	ld 		A,(HL)
	cp		'f'
	jr		nz,is_fbfs_ret

	inc		HL
	ld 		A,(HL)
	cp		's'

is_fbfs_ret:
	ret


; Read block DEBC in to buffer at HL
DISK_READ:

	push	BC

	; Setup block number
	ld		A,C
	ld		C,PORT_DISK_ADDRESS
	out		(C),A
	out		(C),B
	out		(C),E
	out		(C),D

	; Initiate the read command
	ld			A,DISK_COMMAND_READ
	out			(PORT_DISK_COMMAND),A

	; Wait for read to finish
dr_wait:	
	in		A,(PORT_DISK_STATUS)
	and		DISK_STATUS_BIT_BUSY
	JR		NZ,dr_wait

	; Read it
	ld		BC,0x00C0
	inir
	inir

	pop		BC

	; Done!
	ret


