; DE = directory index
; A = ROM Pack number
;	-- 00[10 00]00 --> 0x20000 - 0x23FFF - 16K Rom Pack 0 (maps to Microbee Z80 addr 0x8000)
;	-- 00[10 01]00 --> 0x24000 - 0x27FFF - 16K Rom Pack 1 (maps to Microbee Z80 addr 0xC000)
;	-- 00[10 10]00 --> 0x28000 - 0x2BFFF - 16K Rom Pack 2 (maps to Microbee Z80 addr 0xC000)
;   --    ^^^^^^ this is what goes into Port D0, the hi 4 bits of 18-bit exteranl ram address
;   --           ie: or rom pack number with 1000b
LoadRom:

	; Map external ram to 0x8000->0xBFFF
	or      0x08            ; ROM Position in External RAM (see above)
	or		0x10 			; Turn on ram mapping
	out		(PORT_MEMORY_BANK),A

	; Clear old ROM data
	push	DE
	ld		HL,MEMORY_BANK_ADDR
	ld		DE,MEMORY_BANK_ADDR+1
	ld		BC,0x3ffe
	ld		(HL),0
	ldir
	pop		DE

	; Check for "no rom"
	ld		A,D
	and		80h
	jr		NZ,lr_exit

	; Find the directory entry
	ex		DE,HL
	call 	FbfsGetDirEntry

	; Read ROM pack
	push	HL
	pop		IY
	ld		A,(IY+FBFS_DIR_BLOCK_COUNT)
	ld		HL,MEMORY_BANK_ADDR

	call	DiskReadBlocks

lr_exit:
	; Unmap ROM pack RAM
	xor		A
	out		(0xD0),A

	ret

	
LoadAllRoms:

	ld		HL,FBFS_CONFIG + FBFS_DI_ROM_0
	ld		B,3

lrs_loop:
	; Load directory index
	ld		E,(HL)
	inc		HL
	ld		D,(HL)
	inc		HL

	; Work out rom pack number
	ld		A,3
	sub		B

	; Load it
	push	BC
	push	HL
	call	LoadRom
	pop		HL
	pop		BC

	; Repeat
	djnz	lrs_loop

	ret

