STR_HD1:	db "HD1:",0
STR_FD0:	db "FD0:",0
STR_FD1:	db "FD1:",0
STR_FD2:	db "FD2:",0
STR_SEP:	db "-",0
STR_RESET:	db "Reset",0

MENU_STR_HD1:			defs 32
MENU_STR_FD0:			defs 32
MENU_STR_FD1:			defs 32
MENU_STR_FD2:			defs 32

MAIN_MENU:
	dw	MENU_STR_HD1
	dw	MENU_STR_FD0
	dw	MENU_STR_FD1
	dw	STR_SEP
	dw	STR_RESET
	dw	0

TITLE:
	db		"FPGABee v3.0.1"
TITLE_LEN:	EQU $-TITLE

MainMenu:
	ld		HL,STR_HD1
	ld		DE,MENU_STR_HD1
	ld		BC,(FBFS_CONFIG + FBFS_DI_DISK_1)
	call	SETUP_MENU_SELECTION

	ld		HL,STR_FD0
	ld		DE,MENU_STR_FD0
	ld		BC,(FBFS_CONFIG + FBFS_DI_DISK_3)
	call	SETUP_MENU_SELECTION

	ld		HL,STR_FD1
	ld		DE,MENU_STR_FD1
	ld		BC,(FBFS_CONFIG + FBFS_DI_DISK_4)
	call	SETUP_MENU_SELECTION

	; Clear color
	ld		HL,COLOR_RAM
	ld		BC,0x0720
	ld		A,0xCF
	call	ClearScreenArea

	ld		HL,VCHAR_RAM
	ld		BC,0x0720
	call	DrawBorder

	; Display Title
	ld		HL,TITLE
	ld		DE,VCHAR_RAM + (SCREEN_WIDTH-TITLE_LEN)/2
	ld		BC,TITLE_LEN
	ldir

	; Display the main menu
	ld		HL,MAIN_MENU			; Strings
	push	HL
	ld		HL,0x0021				; Position
	push	HL
	ld		HL,0x051e				; Size
	push	HL
	ld		HL,0					; Selection
	push	HL
	ld		HL,0xB0CF				; Colours
	push	HL
	ld		HL,MainMenuCallback		; Callback
	push	HL
	call	LISTBOX
	ret


; HL - dest - where to write string
; DE - title string
; BC - directory index of selected file
SETUP_MENU_SELECTION:

	; Copy the menu item name
	call 	STRCPY

	; Overwrite the NULL terminator
	ex		DE,HL
	dec		HL
	ld		(HL),' '
	inc		HL

	; Is a file selected?
	ld		A,B
	and		0x80
	jr		z,sms_setup_filename

	; No, n/a
	ld		(HL),'-'
	inc		HL
	ld		(HL),0
	ret

sms_setup_filename:
	ex		DE,HL
	push	BC
	pop		HL
	call	FbfsGetDirEntry
	ld		BC,FBFS_DIR_FILENAME
	add		HL,BC		; Pointer to file name
	call	STRCPY

	ret


MainMenuCallback:
	ld		A,D
	and		80h
	jr		nz,hide_menu
	ld		A,E
	cp		0
	jr		Z,choose_hd1
	cp		1
	jr		Z,choose_fd0
	cp		2
	jr		Z,choose_fd1
	cp		4
	jr		Z,invoke_reset
	or		1
	ret

invoke_reset:
	ld		A,0
	out		(0x81),A
	out		(0xFF),A
	or		1					; shouldn't get to here...
	ret

hide_menu:
	ld		A,0
	out		(0x81),A
	or		1
	ret

choose_hd1:
	ld		HL,(FBFS_CONFIG + FBFS_DI_DISK_1)
	ld		DE,FILTER_HDD_IMAGES
	ld		A,1
	call	CHOOSE_FILE
	ld		HL,MENU_STR_HD1 + 5
	ld		C,1
	jr		insert_disk

choose_fd0:
	ld		HL,(FBFS_CONFIG + FBFS_DI_DISK_3)
	ld		DE,FILTER_FDD_IMAGES
	ld		A,1
	call	CHOOSE_FILE
	ld		HL,MENU_STR_FD0 + 5
	ld		C,3
	jr		insert_disk

choose_fd1:
	ld		HL,(FBFS_CONFIG + FBFS_DI_DISK_4)
	ld		DE,FILTER_FDD_IMAGES
	ld		A,1
	call	CHOOSE_FILE
	ld		HL,MENU_STR_FD1 + 5
	ld		C,4
	jr		insert_disk

insert_disk:
	; DE = -1 if cancelled, -2 if eject, else directory index
	; HL = menu string to be updated
	; C = drive number
	ld		A,D
	and		0x80
	jr		Z,have_new_disk
	ld		A,E
	cp		0xFE
	jr		Z,eject_disk
	or		1
	ret

eject_disk:
	; Reset the menu string
	ld		(HL),'-'
	inc		HL
	ld		(HL),0
	ld		DE,0xFFFF
	jr		apply_new_disk

have_new_disk:
	; Update the menu string
	push	DE
	push	HL
	ex		DE,HL
	add		HL,HL		; * 2
	add		HL,HL		; * 4
	add		HL,HL		; * 8
	add		HL,HL		; * 16
	add		HL,HL		; * 32
	ld		DE,(PTR_DIRECTORY)
	add		HL,DE
	ld		DE,FBFS_DIR_FILENAME
	add		HL,DE
	pop		DE
	call	STRCPY
	pop		DE

apply_new_disk:
	; DE = directory number
	; C = drive number

	; Update the config record
	push	BC
	ld		HL,FBFS_CONFIG + FBFS_DI_DISK_0
	ld		B,0
	add		HL,BC
	add		HL,BC
	ld		(HL),E
	inc		HL
	ld		(HL),D
	pop		BC

	; Update the disk controller
	ld		A,C			
	call	InsertDisk

	; Save the config
	call	FbfsWriteConfig

	or		1
	ret
MAIN_MENU_SELECTED_END:

FILTER_HDD_IMAGES:	db "hd0",0,"hd1",0,0
FILTER_FDD_IMAGES:  db "ds40",0,"ss80",0,"ds80",0,"ds82",0,"ds84",0,"ds8b",0,0


