
FBFS_CONFIG:		defs FBFS_CONFIG_SIZE
PTR_SECTOR_BUFFER:  dw	0
PTR_DIRECTORY:		dw	0
TOTAL_DIR_ENTRIES:	dw	0

FbfsInit:
	; Allocate sector buffer
	ld		BC,512
	call	HeapAlloc
	ld		(PTR_SECTOR_BUFFER),HL

	; Read configuration
	call 	FbfsReadConfig

	; Read directory
	call 	FbfsReadDirectory
	ret

; Read the config record
FbfsReadConfig:
	; Read the config sector
	ld		DE,0
	ld		BC,0
	ld		HL,(PTR_SECTOR_BUFFER)
	call	DiskReadBlock

	; Copy it
	ld		HL,(PTR_SECTOR_BUFFER)
	ld		DE,FBFS_CONFIG
	ld		BC,FBFS_CONFIG_SIZE
	ldir
	ret

; Write the config record
FbfsWriteConfig:
	ld		HL,FBFS_CONFIG
	ld		DE,(PTR_SECTOR_BUFFER)
	ld		BC,FBFS_CONFIG_SIZE
	ldir
	ld		DE,0
	ld		BC,0
	ld		HL,(PTR_SECTOR_BUFFER)
	call	DiskWriteBlock
	ret

; Allocate a block of memory, store it at (PTR_DIRECTORY) and read all
; the directory clusters in.
FbfsReadDirectory:
	; Work out total directory size (DIRCOUNT * 512)
	ld		A,(FBFS_CONFIG + FBFS_DIRCOUNT)
	sla		A
	ld		B,A
	ld		C,0

	; Allocate memory
	call 	HeapAlloc
	ld		(PTR_DIRECTORY),HL

	; Read the directory
	; HL from above
	ld		IY,FBFS_CONFIG + FBFS_DIRBLK
	ld		A,(FBFS_CONFIG + FBFS_DIRCOUNT)
	call	DiskReadBlocks

	; Now work out how many directory entries were actually used
	ld		IX,(PTR_DIRECTORY)
	ld		BC,0
	ld		DE,FBFS_DIR_SIZE
dir_load_count_loop:
	ld		A,(IX+0)
	or		(IX+1)
	or		(IX+2)
	or		(IX+3)
	jr		z,dir_end_found
	add		IX,DE
	inc		BC
	jr		dir_load_count_loop

dir_end_found:
	ld		(TOTAL_DIR_ENTRIES),BC
	ret

; Give directory index (DI) in HL, return pointer to the
; directory entry for the file
FbfsGetDirEntry:
	push	BC
	add		HL,HL		; * 2
	add		HL,HL		; * 4
	add		HL,HL		; * 8
	add		HL,HL		; * 16
	add		HL,HL		; * 32
	ld		BC,(PTR_DIRECTORY)
	add		HL,BC
	pop		BC
	ret
