ALLOW_YIELD:			DB  0
FIRST_YIELD:			DB  1
SAVE_MBEE_SP:			DW	0
SAVE_PCU_SP:			DW	0
SAVE_MBEE_REGS:			DW	0,0,0,0,0,0
SAVE_MBEE_REGS_TOS:


; NMI Handler
PCU_NMI:
	; Save Microbee state
	ld		(SAVE_MBEE_SP),SP
	ld		SP,SAVE_MBEE_REGS_TOS
	push	AF
	push	BC
	push	DE
	push	HL
	push	IX
	push	IY

	; Restore PCU state
	ld		SP,(SAVE_PCU_SP)
	pop		IY
	pop		IX
	pop		HL
	pop		DE
	pop		BC
	pop		AF

	; Carry on (PCU has the Z80 now)
	ret

; Yield to return Z80 to the Microbee
YIELD:
	; Save PCU state
	push	AF
	LD		A,(ALLOW_YIELD)
	or		A
	jr		z,skip_yield
	push	BC
	push	DE
	push	HL
	push	IX
	push	IY
	ld		(SAVE_PCU_SP),SP

	; Restore Microbee state
	ld		SP,SAVE_MBEE_REGS
	pop		IY
	pop		IX
	pop		HL
	pop		DE
	pop		BC

	; Is this the first yield - need to return with ret (not retn)
	ld      a,(FIRST_YIELD)
	or      a
	jr      nz,first_yield

	pop		AF
	ld		SP,(SAVE_MBEE_SP)

	; Request PCU exit
	out		(PORT_PCU_EXIT),A		
	retn

first_yield:
	; Clear flag
	xor		a
	ld      (FIRST_YIELD),A

	; Yield...
	pop		AF
	ld		SP,(SAVE_MBEE_SP)
	out		(PORT_PCU_EXIT),A		
	ret     ; NB: Normal ret 

skip_yield:
	pop		AF
	ret


