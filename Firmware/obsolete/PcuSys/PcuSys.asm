; Constants
include "..\Shared\pcu.asm"

RAM_LO:		EQU 0x4000
RAM_HI:		EQU 0x8000
STACK_SIZE:	EQU 0x400

	ORG		0

; Interrupt table
	defs	0x66-$
	JP		PCU_NMI
	defs	0x100-$

; Setup stack
	ld		SP,RAM_HI

; Initialize Disk
; NB: DEBC passed from boot ROM is the FBFS root sector
	call	DiskInit

; Setup Heap
	ld		HL,RAM_LO
	ld		DE,RAM_HI - RAM_LO - STACK_SIZE
	call	HeapInit

; Initialize FBFS
	call	FbfsInit

; Insert Disks
	call	InsertAllDisks

; Load ROMS
	call	LoadAllRoms

	ld		A,1
	ld		(ALLOW_YIELD),A

; Run the main menu
	call	MainMenu
	jr		$			; should never get here



include "Yield.asm"
include "fbfs.asm"
include "ChooseFile.asm"
include "DiskManager.asm"
include "RomManager.asm"
include "MainMenu.asm"
include "..\Shared\Disk.asm"
include "..\Shared\Drawing.asm"
include "..\Shared\Heap.asm"
include "..\Shared\Keyboard.asm"
include "..\Shared\ListBox.asm"
include "..\Shared\Strings.asm"


