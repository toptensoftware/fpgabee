; Copy null terminated string from HL to DE
STRCPY:
	ld		A,(HL)
	ld		(DE),A
	inc		HL
	inc		DE
	or		A
	jr		nz,STRCPY
	ret


; Given a NULL terminated string in HL, find the last '.'
; Returns found pointer in HL, or NULL if not found
FIND_EXTENSION:
	push	DE
	ld		DE,0				; Pointer to last found dot

fe_l1:
	ld		A,(HL)
	or		A
	jr		z,fe_eos			; End of string?
	cp		'.'
	jr		nz,fe_not_a_dot
	push	HL					; Save position of the dot
	pop		DE

fe_not_a_dot:
	inc		HL					; Next character
	jr		fe_l1

fe_eos:
	ex		DE,HL				; Return the found position
	pop		DE
	ret


; Compare to strings case insensitively
; HL = string 1
; DE = string 2
; Returns Z if strings match
STRICMP:
	push	BC
sic_loop:
	ld		A,(HL)
	or		A
	jr		Z,sic_eos
	call	TOUPPER
	ld		C,A
	ld		A,(DE)
	call	TOUPPER
	cp		C
	jr		NZ,sic_exit
	inc		HL
	inc		DE
	jr		sic_loop
sic_exit:
	pop		BC
	ret
sic_eos:
	ld		C,A
	ld		A,(DE)
	cp		C
	jr		sic_exit



; Make character A uppercase
TOUPPER:
	cp      'a'             ; Nothing to do if not lower case
	ret     c
	cp      'z' + 1         ; > 'z'?
	ret     nc              ; Nothing to do, either
	and     0x5f            ; Convert to upper case
	ret
