; Draw a box border
;   HL = base address
;   BC = height/with
DrawBorder:
	ld		IX,-10
	add		IX,SP

	; Save dimensions
	ld		(IX+0),C
	ld		(IX+1),B

	push	HL

	; Top left corner		
	ld		(HL),BOX_TL	
	inc		HL

	; Top edge
	ld		C,(IX+0)
	dec		C
	dec		C
	ld		B,0
	push	HL
	pop		DE
	inc		DE
	ld		(HL),BOX_H
	ldir

	; Top right corner
	ld		(HL),BOX_TR
	inc		HL

	; Right edge
	pop		HL
	push	HL
	ld		E,(IX+0)
	ld		D,0
	dec		E
	add		HL,DE				; Move to RHS
	ld		DE,SCREEN_WIDTH		; Move to second row
	add		HL,DE

	ld		B,(IX+1)			; Number of rows
	dec		B
	dec		B					; Exclude top/bottom
dba_l1:
	ld		(HL),BOX_V
	add		HL,DE
	djnz	dba_l1

	; Left edge
	pop		HL
	ld		DE,SCREEN_WIDTH
	add		HL,DE

	ld		B,(IX+1)			; Number of rows
	dec		B
	dec		B					; Exclude top/bottom
dba_l2:
	ld		(HL),BOX_V
	add		HL,DE
	djnz	dba_l2

	; Bottom left
	ld		(HL),BOX_BL
	inc		HL

	; Bottom edge
	ld		C,(IX+0)
	dec		C
	dec		C
	ld		B,0
	push	HL
	pop		DE
	inc		DE
	ld		(HL),BOX_H
	ldir

	; Bottom right corner
	ld		(HL),BOX_BR
	inc		HL

	ret



; Clear character ram
ClearScreen:
	ld		HL,VCHAR_RAM
	ld		DE,VCHAR_RAM+1
	ld		BC,VBUFFER_SIZE-1
	ld		(HL),' '
	ldir
	ret


; Clear color ram to color A
ClearColor:
	ld		HL,COLOR_RAM
	ld		DE,COLOR_RAM+1
	ld		BC,VBUFFER_SIZE-1
	ld		(HL),A
	ldir
	ret


		; print hex word in HL to DE
PrintHexWord:
		LD		A,H
		CALL	PrintHexByte
		LD		A,L
		CALL	PrintHexByte
		ret

		; print hex byte in A to DE
PrintHexByte:
		PUSH	AF
		SRL		A
		SRL		A
		SRL		A
		SRL		A
		CALL	PrintHexNibble
		POP		AF
		;; fall through


		; print low nibble of A to DE
PrintHexNibble:
		and     0xF
		cp      0xA
		jr      c,lt10
		add		'A' - 0xA;
		ld		(de),a
		inc		de
		ret
lt10:
		add		'0'
		ld		(de),a
		inc		de
		ret;



; Multiply H by E, result in HL
MUL_H_E:

   ld	l, 0
   ld	d, l

   sla	h	
   jr	nc, $+3
   ld	l, e
   
   ld b, 7
mulhe_loop:
   add	hl, hl          
   jr	nc, $+3
   add	hl, de
   
   djnz	mulhe_loop
   
   ret

; Divide HL by C
; Returns quotient in HL, remainder in A
DIV_HL_C:
   xor	a
   ld	b, 16

div_hl_c_loop:
   add	hl, hl
   rla
   cp	c
   jr	c, $+4
   sub	c
   inc	l
   
   djnz	div_hl_c_loop
   
   ret
 

; HL = pointer to screen buffer
; B = number of rows
; C = number of columns
; A = fill with
ClearScreenArea:

csa_fill:
	push	BC				; Save number of rows/cols
	push	HL				; Save current row pointer

	ld		(HL),A			; Set fill character
	push	HL				
	pop		DE
	inc		DE				; DE = HL + 1
	ld		B,0				; Number of columns in BC
	dec		C
	ldir					; memset

	pop		HL				; Restore row pointer
	pop		BC				; Restore row/col count
	ld		DE,SCREEN_WIDTH
	add		HL,DE			; Next line
	djnz	csa_fill
	ret

; Save a copy of a screen area
; 	HL = offset from start of color/char buffer
; 	B = number of rows
; 	C = number of columns
; Returns ptr to malloced saved screen
SaveScreenArea:

	; Save params
	push	HL
	push	BC

	; Work out how much room required to store character
	; and color buffers
	ld		H,B
	ld		E,C
	call	MUL_H_E			; HL = b*c
	add		HL,HL			; HL = 2*b*c (chars+color)
	ld		DE,4
	add		HL,DE			; HL = 4 + 2*b*c

	; Allocate a block from the heap
	push	HL
	pop		BC
	call	HeapAlloc
	push	HL
	pop		IX				; IX = Allocated block

	; Restore params
	pop		BC
	pop		DE

	; Save params to allocated block
	ld		(IX+0),E
	ld		(IX+1),D
	ld		(IX+2),C
	ld		(IX+3),B

	; Setup DE as destination for the copy
	ld		DE,4
	add		HL,DE
	ex		DE,HL			; DE - allocation + 4 bytes

	; Copy colors
	ld		C,(IX+0)
	ld		B,(IX+1)
	ld		HL,COLOR_RAM	
	add		HL,BC			; HL = source
	call	psa_copy		; Copy it

	; Copy characters
	ld		C,(IX+0)
	ld		B,(IX+1)
	ld		HL,VCHAR_RAM
	add		HL,BC
	call 	psa_copy

	push	IX
	pop		HL
	ret

psa_copy:
	ld		B,(IX+3)
ssa_l1:
	push	BC
	push	HL				; Save current row pointer

	ld		B,0				; Number of columns in BC
	ld		C,(IX+2)
	ldir					; copy it

	pop		HL				; Restore row pointer

	ld		BC,SCREEN_WIDTH
	add		HL,BC			; Next line

	pop		BC

	djnz	ssa_l1		; Repeat B times
	ret


; Restore a previously pushed copy of a screen area from the stack
;  HL - Saved block
RestoreScreenArea:

	push	HL
	pop		IX

	; Calculate destination ptr
	ld		HL,COLOR_RAM
	ld		E,(IX+0)
	ld		D,(IX+1)
	add		HL,DE
	ex		DE,HL

	; Calculate source ptr
	ld		BC,4
	push	IX
	pop		HL
	add		HL,BC

	; Copy region
	call 	rsa_copy

	; Calculate destination ptr
	push	HL
	ld		HL,VCHAR_RAM
	ld		E,(IX+0)
	ld		D,(IX+1)
	add		HL,DE
	ex		DE,HL
	pop		HL

	; Copy region
	call 	rsa_copy

	push	IX
	pop		HL
	call	HeapFree

	; Done!
	ret

rsa_copy:
	ld		B,(IX+3)
rsa_l1:
	push	BC
	push	DE
	ld		B,0				; Number of columns in BC
	ld		C,(IX+2)
	ldir					; copy it
	pop		DE

	push	HL
	ld		HL,SCREEN_WIDTH
	add		HL,DE
	ex		DE,HL
	pop		HL

	pop		BC

	djnz	rsa_l1			; Repeat B times
	ret


; HL = zero terminated string
; DE = screen buffer
; C = buffer width (will be space padded)
PrintLine:

	; Copy the string
pl_loop1:
	ld		A,(HL)
	or		A
	jr		Z,pl_end_of_string
	ld		(DE),A
	inc		HL
	inc		DE
	dec		C
	ld		A,C
	or		A
	jr		nz,pl_loop1

	; Destination buffer full
	ret

pl_end_of_string:
	ex		DE,HL
	ld		A,B
	ld		B,C
pl_loop2:
	ld		(HL),' '
	inc		HL
	djnz	pl_loop2
	ld		B,A

	ret


