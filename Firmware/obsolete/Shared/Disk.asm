FBFS_ROOT_SECTOR:		DW	0, 0

; Initialize Disk library by storing the root sector
; which will be automatically added to all read/write operations
; DEBC = FBFS root sector
DiskInit:
	ld		(FBFS_ROOT_SECTOR),BC
	ld		(FBFS_ROOT_SECTOR+2),DE
	ret


; Read multiple blocks
; (IY) = 32-bit block number
; HL = destination
; A = number of blocks
DiskReadBlocks:

	ld		c,(IY+0)
	ld		b,(IY+1)
	ld		e,(IY+2)
	ld		D,(IY+3)

rd_loop:
	; Save counter
	push	af

	; Read block
	call	DiskReadBlock

	; Increment block number
	call	IncDEBC

	; Loop
	pop		AF
	sub		1
	jr		nz,rd_loop

	ret


; Read block DEBC in to buffer at HL
DiskReadBlock:

	push 	DE
	push	BC

	call	AdjustSector

	; Setup block number
	ld		A,C
	ld		C,PORT_DISK_ADDRESS
	out		(C),A
	out		(C),B
	out		(C),E
	out		(C),D

	; Initiate the read command
	ld			A,DISK_COMMAND_READ
	out			(PORT_DISK_COMMAND),A

	; Wait for read to finish
dr_wait:	
	call	YIELD
	in		A,(PORT_DISK_STATUS)
	and		DISK_STATUS_BIT_BUSY
	JR		NZ,dr_wait

	; Read it
	ld		BC,0x00C0
	inir
	inir

	pop		BC
	pop		DE

	; Done!
	ret


; Write block DEBC from SECTOR_BUFFER
DiskWriteBlock:

	push 	DE
	push	BC

	call	AdjustSector

	; Setup block number
	ld		A,C
	ld		C,PORT_DISK_ADDRESS
	out		(C),A
	out		(C),B
	out		(C),E
	out		(C),D

	; Rewind buffer
	ld			A,DISK_COMMAND_REWIND_BUFFER
	out			(PORT_DISK_COMMAND),A

	; Read it
	ld		BC,0x00C0
	otir
	otir

	; Initiate the write command
	ld			A,DISK_COMMAND_WRITE
	out			(PORT_DISK_COMMAND),A

	; Wait for write to finish
dw_wait:	
	call	YIELD
	in		A,(PORT_DISK_STATUS)
	and		DISK_STATUS_BIT_BUSY
	JR		NZ,dw_wait

	pop		BC
	pop		DE

	; Done!
	ret


; DEBC = DEBC + (FBFS_ROOT_SECTOR)
AdjustSector:
	
	push	HL

	; Loword
	ld		HL,(FBFS_ROOT_SECTOR)
	add		HL,BC
	push	HL

	; Hiword
	ld		HL,(FBFS_ROOT_SECTOR + 2)
	adc		HL,DE
	ex		DE,HL

	; Retrieve loword
	pop		BC

	pop		HL
	ret

; Increment DEBC
IncDEBC:
	push	HL
	ld		HL,1
	add		HL,BC
	push	HL
	pop		BC
	ld		HL,0
	adc		HL,DE
	ex		DE,HL
	pop		HL
	ret
