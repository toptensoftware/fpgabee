#include "common.h"
#include "DiskContext.h"
#include "ChooseFile.h"

static void add_file(CHOOSEFILECONTEXT* pctx, const char* filename)
{
	if (pctx->selectedFile && strcmp(filename, pctx->selectedFile)==0)
	{
		pctx->selectedFileIndex = pctx->nFiles;
	}


	// Copy to work buffer
	strcpy(pctx->pBuffer, filename);

	// Update file count and buffer position
	pctx->ppFiles[pctx->nFiles++] = pctx->pBuffer;
	pctx->pBuffer += strlen(filename) + 1;
}

static bool enumfiles_callback(uint32_t deIndex, const NFFSDIRENTRY* de, void* user)
{
	CHOOSEFILECONTEXT* pcctx = (CHOOSEFILECONTEXT*)user;
	deIndex;
	user;

	// Matches?
	if (nffsGlob(de->filename, pcctx->pattern))
	{
		add_file(pcctx, de->filename);
	}

	return pcctx->nFiles != 0xFF;
}

const char* ChooseFileEx(
	const char* pattern, 
	const char* selectedFile, 
	const char* pszNullOption, 
	const char* pszTitle,
	size_t (*wndProc)(WINDOW* pWindow, MSG* pMsg)
	)
{
	int err, sel;	
	const char* retv = NULL;
	LISTBOX lb;

	// Find matching files
	CHOOSEFILECONTEXT ctx;
	ctx.pattern = pattern;
	ctx.pBuffer = BankedMemory + 0x100 * sizeof(void*);
	ctx.nFiles = 0;
	ctx.ppFiles = BankedMemory;
	ctx.selectedFile = selectedFile;
	ctx.selectedFileIndex = 0;

	// Null option?
	if (pszNullOption)
	{
		add_file(&ctx, pszNullOption);
	}

	// Find files
	err = nffsEnumFiles(&nffsctx, enumfiles_callback, &ctx);
	if (err)
		return NULL;

	// Display picker...
	memset(&lb, 0, sizeof(lb));
	lb.window.rcFrame.left = 4;
	lb.window.rcFrame.top = 2;
	lb.window.rcFrame.width = 17;
	lb.window.rcFrame.height = 12;
	lb.window.attrNormal = MAKECOLOR(COLOR_WHITE, COLOR_BLUE);
	lb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	lb.window.title = pszTitle;
	lb.window.wndProc = wndProc;
	lb.window.user = &ctx;
	lb.selectedItem = ctx.selectedFileIndex;
	ListBoxSetData(&lb, ctx.nFiles, ctx.ppFiles);
	lb.scrollPos = ListBoxEnsureVisible(&lb, lb.selectedItem);

	sel = RunModalWindow(&lb.window);

	// Cancelled
	if (sel<0)
		return NULL;

	// Null option selected?
	if (sel == 0 && pszNullOption)
		return "";

	// If same file selected, return cancelled.
	if (selectedFile != NULL)
	{
		if (strcmp(ctx.ppFiles[sel], selectedFile)==0)
			return NULL;
	}

	// Return pointer to filename!
	return ctx.ppFiles[sel];
}	


// Choose a file matching pattern.
// Returns NULL if cancelled, empty string if ejected, otherwise file name
// which must be copied immediately
const char* ChooseFile(const char* pattern, const char* selectedFile, const char* pszNullOption)
{
	return ChooseFileEx(pattern, selectedFile, pszNullOption, "Choose File", ListBoxWindowProc);
}

