#include "common.h"

// Low level helper to load the block number registers
void mbee_diskAddressLL(uint32_t blockNumber)
{
	blockNumber;
__asm
		ld	hl, #2
		add hl,sp
		ld	bc,#0x0400 + _VdcAddressPort
		otir
__endasm;
}

// Send the disk address (add the partition base block number first)
void mbee_diskAddress(uint32_t blockNumber)
{
	mbee_diskAddressLL(blockNumber + GetDiskBaseAddress());
}

// Disk type extensions
const char* g_extensions[] = 
{
	"ds40",
	"ss80",
	"ds80",
	"ds82",
	"ds84",
	"ds8B",
	"hd0",
	"hd1",
};

uint8_t DiskTypeFromExtension(const char* filename)
{
	uint8_t i;

	// Find the extension
	const char* dotPos = strrchr(filename, '.');
	if (!dotPos)
		return DISK_TYPE_NONE;

	dotPos++;

	for (i=0; i<8; i++)
	{
		if (stricmp(g_extensions[i], dotPos)==0)
			return DISK_TYPE_DS40 + i;
	}

	return DISK_TYPE_NONE;
}

uint32_t DiskBlockCount(uint8_t diskType)
{
	switch (diskType)
	{
		case DISK_TYPE_DS40: return 40 * 2 * 10;
		case DISK_TYPE_SS80: return 80 * 1 * 10;
		case DISK_TYPE_DS80: return 80 * 2 * 10;
		case DISK_TYPE_DS82: return 80 * 2 * 10;
		case DISK_TYPE_DS84: return 80 * 2 * 10;
		case DISK_TYPE_DS8B: return 80 * 2 * 10;
		case DISK_TYPE_HD0: return 306 * 4 * 17;
		case DISK_TYPE_HD1: return 80 * 4 * 62;
	}
	return 0;
}


void EjectDisk(uint8_t drive)
{
	// Pass block number to MBee disk controller
	mbee_diskAddress(0);

	// Set the disk type
	VdcCommandPort =  drive | DISK_TYPE_NONE;
}


int InsertDisk(uint8_t drive, const char* filename, bool testOnly, bool showError)
{
	NFFSDIRENTRY de;
	int err;
	uint8_t diskType;

	// Eject?
	if (filename == NULL || filename[0]==NULL)
	{
		if (testOnly)
			return 0;
		EjectDisk(drive);
		return 0;
	}

	// Find disk file
	err = nffsFindFile(&nffsctx, filename, &de);
	if (err)
		return err;

	// Work out disk type
	diskType = DiskTypeFromExtension(filename);
	if (diskType == DISK_TYPE_NONE)
	{
		if (testOnly)
			return 0;

		EjectDisk(drive);
		return 0;
	}

	// Check the block count matches
	if (de.blockCount != DiskBlockCount(diskType))
	{
		if (!testOnly)
			EjectDisk(drive);
		if (showError)
			MessageBox("Insert Disk", "Disk image is not\nthe correct size", MB_OK|MB_ERROR);
		return -1;
	}

	if (testOnly)
		return 0;

	// Pass block number to MBee disk controller
	mbee_diskAddress(de.block);

	// Set the disk type
	VdcCommandPort =  drive | (diskType << 3);

	return 0;
}