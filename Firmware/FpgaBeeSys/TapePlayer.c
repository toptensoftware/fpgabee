#include "common.h"
#include "ChooseFile.h"

extern int showFileError(int retv);


#define SBA_SIGNATURE	0x53425054

typedef struct tagSBAHEADER
{
	uint32_t signature;
	uint32_t sampleRate;
	uint32_t totalSamples;
	uint32_t unused;
} SBAHEADER;

typedef struct tagTAPHEADER
{
	char filename[6];
	char filetype;
	uint16_t datalen;
	uint16_t loadaddr;
	uint16_t startaddr;
	uint8_t speed;
	uint8_t autostart;
	uint8_t unused;
} TAPHEADER;

bool is_playing = false;

void tapePlayFinished(uint8_t status)
{
	status;
	if (is_playing)
	{
		EndModal(0);
	}
}

void PlayTapeUI()
{
	const char* filename;
	const char* ext;
	bool isTap;
	bool isSba;
	int i;
	NFFSDIRENTRY de;
	SBAHEADER* psba = (SBAHEADER*)diskBuffer;

	// Stop old playing
	StopTape();

	// Pick a file
	filename = ChooseFile("*.sba;*.tap", NULL, false);
	if (!filename)
		return;

	ext = nffsExtension(filename);
	isTap = strcmp(ext, ".tap") == 0;
	isSba = strcmp(ext, ".sba") == 0;
	if (!isTap && !isSba)
		return;

	if (showFileError(nffsFindFile(&nffsctx, filename, &de)))
		return;

	// Read the header 
	DiskRead(de.block, psba);


	if (isSba)
	{
		// Check the signature
		if (psba->signature != SBA_SIGNATURE)
		{
			MessageBox("Play Tape", "Invalid .sba file", MB_OK|MB_ERROR);
			return;
		}

		// Check the sample rat
		if (psba->sampleRate != 22050)
		{
			MessageBox("Play Tape", "Unsupported sample rate", MB_OK|MB_ERROR);
			return;
		}

		// Start play
		PlayTape(de.block, de.blockCount, tapePlayFinished);				
	}
	else
	{
		uint16_t speedByteIndex;
		uint16_t stopByteIndex;

		// Check it's heeader
		if (memcmp(diskBuffer, "TAP_DGOS_MBEE", 13)!=0)
		{
			MessageBox("Play Tape", "Invalid .tap file", MB_OK|MB_ERROR);
			return;
		}

		// Find the first zero byte
		for (i=13; diskBuffer[i]!=0 && i<512; i++) {}

		// Find the first non-zero byte
		for (; diskBuffer[i]==0 && i<512; i++) {}

		i++;

		// Check the speed byte in the header
		if (((TAPHEADER*)(diskBuffer + i))->speed)
		{
			// Work out which index to start 1200 baud
			// First byte after the header and it's checksum
			speedByteIndex = i + sizeof(TAPHEADER) + 1;
		}
		else
		{
			speedByteIndex = 0;
		}

		// Need to add one to the stop byte since the hdl pre-advances the cpuram_addr
		// ready for the next byte.
		stopByteIndex = ((de.length + 1) % 0x1FF);

		RenderTape(de.block, de.blockCount + (stopByteIndex == 0 ? 1 : 0), speedByteIndex, stopByteIndex, tapePlayFinished);
	}


	// Message box
	is_playing = true;
	DisplayModePort = 0;
	MessageBox("Tape", "Playing...", MB_STOP|MB_INPROGRESS);
	is_playing = false;

	// Stop 
	StopTape();
}

bool is_recording = false;

void tapeRecordFinished(uint8_t status)
{
	status;
	if (is_recording)
	{
		DisplayModePort = DISPLAY_MODE_ENABLE_VIDEO | DISPLAY_MODE_ENABLE_KEYBOARD;
		EndModal(1);
	}
}

void RecordTapeUI()
{
	uint32_t lastBlock = 0;
	NFFSDIRENTRY de;
	uint8_t status;
	uint32_t rec_block, rec_blockCount;
	int button;
	char filename[NFFS_MAX_FILENAME+1];
	SBAHEADER* psba = (SBAHEADER*)diskBuffer;

	// Open block tail
	if (showFileError(nffsCreateTail(&nffsctx, &rec_block, &rec_blockCount)))
		return;

	rec_blockCount = 8192;

	// Start recording
	RecordTape(rec_block, rec_blockCount, tapeRecordFinished);

	// Message box
	is_recording = true;
	DisplayModePort = 0;
	button = MessageBox("Tape", "Recording...", MB_STOPCANCEL|MB_ERROR);
	is_recording = false;

	// Get the status
	status = TapeStatusPort;

	// Make sure it's stopped (in case user canceled)
	StopTape();

	// Cancelled?
	if (button == 0)
	{
		nffsDiscardTail(&nffsctx);
		return;
	}

	// Check we had room
	if (status & TAPE_STATUS_BIT_FINISHED)
	{
		nffsDiscardTail(&nffsctx);
		MessageBox("Record Tape", "Insufficient Room", MB_OK | MB_ERROR);
		return;
	}

	// Work out how many blocks were actually used
	TapeCommandPort = TAPE_COMMAND_LOADCURBLOCK;
	lastBlock = TapeBlockNumberPort;
	lastBlock |= ((uint32_t)TapeBlockNumberPort) << 8;
	lastBlock |= ((uint32_t)TapeBlockNumberPort)<< 16;
	lastBlock |= ((uint32_t)TapeBlockNumberPort) << 24;

	if (lastBlock <= rec_blockCount || lastBlock >= rec_block + rec_blockCount)
	{
		nffsDiscardTail(&nffsctx);
		MessageBox("Record Tape", "Bad Block Number", MB_OK|MB_ERROR);
		return;
	}

	// Fill in the header...
	DiskRead(rec_block, psba);
	psba->signature = SBA_SIGNATURE;
	psba->sampleRate = 22050;
	psba->totalSamples = ((rec_blockCount * 512) - sizeof(SBAHEADER)) / 8;
	psba->unused = 0;
	DiskWrite(rec_block, psba);

	filename[0]='\0';
	while (true)
	{
		// Prompt for filename
		if (!PromptInput("Save Recording As", filename, NFFS_MAX_FILENAME))
		{
			nffsDiscardTail(&nffsctx);
			return;
		}

		// Append default extension
		if (showFileError(nffsEnsureExtension(filename, "sba")))
		{
			continue;
		}

		// Already exists?
		if (nffsExists(&nffsctx, filename))
		{
			if (MessageBox("Record Tape", "Overwrite existing file?", MB_YESNO)==0)
			{
				continue;
			}
		}

		// Save the tail
		if (!showFileError(nffsSaveTail(&nffsctx, filename, (lastBlock - rec_block) * 512, &de)))
		{
			return;
		}
	}
}
