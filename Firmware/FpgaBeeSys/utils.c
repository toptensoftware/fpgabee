#include "common.h"


char tolower(char ch)
{
	if (ch >= 'A' && ch <= 'Z')
		return ch - 'A' + 'a';
	else
		return ch;
}

int stricmp(const char* a, const char* b)
{
	while (*a || *b)
	{
		int cmp = tolower(*a) - tolower(*b);
		if (cmp!=0)
			return cmp;
		a++;
		b++;
	}
	return 0;
}

