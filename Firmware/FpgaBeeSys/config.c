#include "common.h"
#include "config.h"
#include "DiskManager.h"
#include "RomManager.h"

CONFIG g_Config;

int ReadConfig()
{
	int err;
	NFFSDIRENTRY de;

	// Find config file
	err = nffsFindFile(&nffsctx, "fpgabee.cfg", &de);
	if (err == NFFS_ERR_NOTFOUND)
	{
		memset(&g_Config, 0, sizeof(g_Config));
		g_Config.enableSound = true;
		g_Config.enableTapeMonitor = true;
		return WriteConfig();
	}
	if (err)
		return err;

	// Read to disk buffer
	if (!DiskRead(de.block, diskBuffer))
		return NFFS_ERR_READ;

	// Keep it
	memcpy(&g_Config, diskBuffer, sizeof(g_Config));
	return NFFS_OK;
}

int WriteConfig()
{
	int err;
	NFFSDIRENTRY de;

	// Find config file
	err = nffsCreateFile(&nffsctx, "fpgabee.cfg", 1, sizeof(g_Config), &de);
	if (err)
		return err;

	// Copy to disk buffer
	memset(diskBuffer, 0, sizeof(diskBuffer));
	memcpy(diskBuffer, &g_Config, sizeof(g_Config));

	// Read to disk buffer
	if (!DiskWrite(de.block, diskBuffer))
	{
		return NFFS_ERR_WRITE;
	}

	// Keep it
	return NFFS_OK;
}

void InsertDiskSafe(uint8_t drive,  char* psz)
{
	if (InsertDisk(drive, psz, false, false) != 0)
	{
		// Remove from config menu
		psz[0]='\0';
	}
}

int ApplyConfig()
{
	InsertDiskSafe(1, g_Config.hdd);
	InsertDiskSafe(4, g_Config.fdd1);
	InsertDiskSafe(5, g_Config.fdd2);
	LoadRomPack(0, g_Config.rompack0);
	LoadRomPack(1, g_Config.rompack0);
	LoadRomPack(2, g_Config.rompack0);
	return 0;
}

void EjectFile(const char* pszFileName)
{
	bool dirty = false;
	if (strcmp(g_Config.hdd, pszFileName)==0)
	{
		EjectDisk(1);
		g_Config.hdd[0] = '\0';
		dirty = true;
	}
	if (strcmp(g_Config.fdd1, pszFileName)==0)
	{
		EjectDisk(4);
		g_Config.fdd1[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.fdd2, pszFileName)==0)
	{
		EjectDisk(5);
		g_Config.fdd2[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.rompack0, pszFileName)==0)
	{
		g_Config.rompack0[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.rompack1, pszFileName)==0)
	{
		g_Config.rompack1[0]='\0';
		dirty = true;
	}
	if (strcmp(g_Config.rompack2, pszFileName)==0)
	{
		g_Config.rompack2[0]='\0';
		dirty = true;
	}

	if (dirty)
	{
		WriteConfig();
	}
}