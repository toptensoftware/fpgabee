#include "common.h"

typedef struct tagSTREAM
{
	uint32_t block;
	uint32_t length;
	uint32_t pos;
	bool firstWrite;
	uint8_t buf[512];
} STREAM;

STREAM* openStream(uint32_t block, uint32_t length)
{
	STREAM* pStream = malloc(sizeof(STREAM));
	pStream->block = block;
	pStream->length = length;
	pStream->pos = 0;
	pStream->firstWrite = true;
	return pStream;
}

uint32_t closeStream(STREAM* pStream)
{
	uint32_t len;
	if (!pStream->firstWrite && pStream->pos & 0x1FF)
	{
		DiskWrite(pStream->block++, pStream->buf);
	}
	len = pStream->pos;
	free(pStream);
	return len;
}

int readStream(STREAM* pStream, uint8_t* pb)
{
	uint16_t bufPos;

	// Eof?
	if (pStream->pos == pStream->length)
		return -1;

	// Next block?
	bufPos = pStream->pos & 0x1FF;
	if (bufPos == 0)
	{
		// Read the next block
		if (!DiskRead(pStream->block++, pStream->buf))
			return NFFS_ERR_READ;
	}

	*pb = pStream->buf[bufPos];

	pStream->pos++;
	return 0;
}

int writeStream(STREAM* pStream, uint8_t b)
{
	// Write to block
	uint16_t bufPos = pStream->pos & 0x1FF;
	pStream->buf[bufPos] = b;

	if (bufPos == 0x1FF && !pStream->firstWrite)
	{
		if (!DiskWrite(pStream->block++, pStream->buf))
			return NFFS_ERR_WRITE;
	}
	pStream->firstWrite = false;
	pStream->pos++;
	return 0;
}

STREAM* g_pStreamIn;
STREAM* g_pStreamOut;
uint8_t g_currentByte;
uint16_t g_currentBitMask;
bool g_currentBit;
uint8_t g_tapeSpeed = 4;
int g_error = 0;

uint8_t readHalfCycle()
{
	uint8_t count = 1;
	uint8_t bit;

	if (g_error)
		return 0;

	while (true)
	{
		if (g_currentBitMask == 0x80)
		{
			if (g_error = readStream(g_pStreamIn, &g_currentByte))
				return 0;

			g_currentBitMask = 0x01;

		}
		else
		{
			g_currentBitMask = g_currentBitMask << 1;
		}

		bit = (g_currentByte & g_currentBitMask) ? 1 : 0;


		if (bit != g_currentBit)
		{
			g_currentBit = bit;
			return count;
		}
		count++;
	}
}


int readDataBit(uint8_t* pVal)
{
	uint8_t sum = 0;
	uint8_t remainingCycles;
	uint8_t i;
	uint8_t cycle;

	// Sum N cycles
	cycle = readHalfCycle();

	for (i = 0; i < g_tapeSpeed; i++)
	{
		sum += readHalfCycle();
	}

	if (g_tapeSpeed == 4)
		sum = sum >> 2;

	remainingCycles = g_tapeSpeed * 2;

	// Divide by N
	if (sum < 7)
	{
		*pVal = 0x80;
		remainingCycles *= 2;
	}
	else
	{
		*pVal = 0;
	}
	
	// How many more cycles need to be read to align to the next byte?
	remainingCycles -= g_tapeSpeed;
	remainingCycles--;

	// Read reamining cycles
	for (i = 0; i < remainingCycles; i++)
	{
		readHalfCycle();
	}

	// Done
	return 0;
}

uint8_t readByte()
{
	uint8_t i;
	uint8_t bit;
	uint8_t byte = 0;
	int err;
	uint8_t cycle;

	// Align to cycle
	readHalfCycle();

	// Wait till get a long half cycle which is the lead zero before the bit data
	while (true)
	{
		cycle= readHalfCycle();
		if (g_error)
			return 0;
		if (cycle > 7)
			break;
	}

	// Read the rest of the lead zero bit
	for (i = 0; i < g_tapeSpeed * 2 - 1; i++)
	{
		readHalfCycle();
	}

	// Read the data bits
	for (i = 0; i < 8; i++)
	{
		if (err = readDataBit(&bit))
			return err;

		byte = bit | (byte >> 1);
	}

	return byte;
}

// Read byte from the audio and write it to the tap file
uint8_t readWriteByte()
{
	uint8_t val = readByte();
	writeStream(g_pStreamOut, val);
	return val;
}

// Header
//#pragma pack(1)
typedef struct tagHEADER
{
	char filename[6];
	char filetype;
	uint16_t datalen;
	uint16_t loadaddr;
	uint16_t startaddr;
	uint8_t speed;
	uint8_t autostart;
	uint8_t unused;
} HEADER;

int parseAudioInternal()
{
	uint8_t bytes[256];
	uint8_t checksum;
	HEADER header;
	uint16_t bytesLeft = 0;
	uint8_t* p = bytes;
	uint8_t byte;
	uint16_t i;
	int err;
	const char* pSig = "TAP_DGOS_MBEE";

	// Setup state
	g_currentBitMask = 0x80;
	g_currentBit = 0;



	// Start at 300 baud
	g_tapeSpeed = 4;
	g_error = 0;

	// Skip the SBA header
	for (i = 0; i < 16; i++)
	{
		readStream(g_pStreamIn, &g_currentByte);
	}

	// Write the .tap header
	while (*pSig)
	{
		writeStream(g_pStreamOut, *pSig++);
	}


	// Wait till we get a zero byte
	while (true)
	{
		byte = readByte();
		if (g_error)
			return g_error;
		if (byte == 0)
			break;
	}

	// And write it
	if (err = writeStream(g_pStreamOut, 0))
		return err;

	// Wait till we get the header lead byte
	while (true)
	{
		byte = readWriteByte();
		if (byte != 0)
			break;
		if (g_error)
			return g_error;
	}

	// Read the header
	checksum = 16;
	p = (uint8_t*)&header;
	for (i = 0; i < 16; i++)
	{
		byte = readWriteByte();
		*p++ = byte;
		checksum += byte;
	}

	byte =readWriteByte();
	checksum += byte;
	if (checksum)
		return -1;

	if (g_error)
		return g_error;

	// Switch to 1200 baud?
	if (header.speed)
	{
		g_tapeSpeed = 1;
	}

	bytesLeft = header.datalen;
	while (bytesLeft)
	{
		// How bytes in this block?
		uint16_t bytesInBlock = bytesLeft;
		if (bytesInBlock & 0xFF00)
			bytesInBlock = 0x100;

		// Read this block
		checksum = bytesInBlock & 0xFF;
		for (i = 0; i != bytesInBlock; i++)
		{
			byte = readWriteByte();
			bytes[i] = byte;
			checksum += byte;
		}

		// Read and check the checksum
		checksum += readWriteByte();
		if (checksum)
			return -1;
		if (g_error)
			return g_error;

		bytesLeft -= bytesInBlock;
	}

	return 0;
}

int parseAudio(const char* pszFileIn, const char* pszFileOut)
{
	NFFSDIRENTRY de;
	int retv;
	uint32_t length;
	int err;
	uint32_t outBlock;
	uint32_t outBlockCount;

	// Find input file
	err = nffsFindFile(&nffsctx, pszFileIn, &de);
	if (err)
		return err;

	// Create output file
	err = nffsCreateTail(&nffsctx, &outBlock, &outBlockCount);
	if (err)
		return err;

	// Create streams
	g_pStreamIn = openStream(de.block, de.length);
	g_pStreamOut = openStream(outBlock, 0);

	// Convert
	retv = parseAudioInternal();

	// Clean up
	closeStream(g_pStreamIn);
	length = closeStream(g_pStreamOut);

	// Discard tail if failed
	if (retv)
	{
		nffsDiscardTail(&nffsctx);
		return retv;
	}

	// Save tail
	return nffsSaveTail(&nffsctx, pszFileOut, length, &de);
}

