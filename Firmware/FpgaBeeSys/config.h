typedef struct tagCONFIG
{
	char 	hdd[NFFS_MAX_FILENAME + 1];
	char 	fdd1[NFFS_MAX_FILENAME + 1];
	char 	fdd2[NFFS_MAX_FILENAME + 1];
	char 	rompack0[NFFS_MAX_FILENAME + 1];
	char    rompack1[NFFS_MAX_FILENAME + 1];
	char    rompack2[NFFS_MAX_FILENAME + 1];
	bool	enableSound;
	bool	enableTapeMonitor;
} CONFIG;

extern CONFIG g_Config;

int ReadConfig();
int WriteConfig();
int ApplyConfig();
void EjectFile(const char* pszFileName);

