#include "common.h"
#include "ChooseFile.h"
#include "DiskManager.h"
#include "Config.h"

#include "ParseAudio.h"

#define FMC_RENAME 0
#define FMC_COPY 1
#define FMC_DELETE 2
#define FMC_NEWFILE 3
#define FMC_CONVERTTTOTAP 4

static const char* fileMenuCommands[] = {
	"Rename",
	"Copy",
	"Delete",
	"\1",
	"Convert to .tap",
	NULL
};

static bool g_bContinue;

int onFileManagerCommand(const char* pszFileName)
{
	const char* pszExt = nffsExtension(pszFileName);

	LISTBOX lb;
	memset(&lb, 0, sizeof(LISTBOX));
	lb.window.rcFrame.left = 1;
	lb.window.rcFrame.top = 3;
	lb.window.rcFrame.width = 24;
	lb.window.title = "Choose Action";
	lb.window.wndProc = ListBoxWindowProc;
	lb.window.attrNormal = MAKECOLOR(COLOR_WHITE, COLOR_BLUE);
	lb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	lb.selectedItem = 0;

	if (pszExt && strcmp(pszExt, ".sba")==0)
		fileMenuCommands[FMC_CONVERTTTOTAP-1] = "\1";
	else
		fileMenuCommands[FMC_CONVERTTTOTAP-1] = NULL;

	ListBoxSetData(&lb, -1, fileMenuCommands);

	lb.window.rcFrame.height = 2 + lb.getItemCount(&lb);

	// Draw list box content
	return RunModalWindow(&lb.window);
}

int showFileError(int retv)
{
	if (retv == 0)
		return 0;

	MessageBox("Failed", nffsErrorMessage(retv), MB_OK|MB_ERROR);

	return retv;
}

bool deleteFile(const char* pszFileName)
{
	if (!MessageBox("Really Delete?", pszFileName, MB_YESNO))
		return false;

	EjectFile(pszFileName);

	return showFileError(nffsDeleteFile(&nffsctx, pszFileName))==0;
}

bool renameFile(const char* pszFileName)
{
	char szTemp[NFFS_MAX_FILENAME+1];
	strcpy(szTemp, pszFileName);
	if (!PromptInput("New Filename:", szTemp, NFFS_MAX_FILENAME))
		return false;

	if (strcmp(szTemp, pszFileName)==0)
		return true;

	EjectFile(pszFileName);

	return showFileError(nffsRenameFile(&nffsctx, pszFileName, szTemp))==0;
}

bool copyFile(const char* pszFileName)
{
	char szTemp[NFFS_MAX_FILENAME+1];
	strcpy(szTemp, pszFileName);
	if (!PromptInput("Copy To:", szTemp, NFFS_MAX_FILENAME))
		return false;

	return showFileError(nffsDuplicateFile(&nffsctx, pszFileName, szTemp))==0;
}

bool convertToTap(const char* pszFileName)
{
	const char* pszExt;
	int err;
	char sz[NFFS_MAX_FILENAME+1];
	strcpy(sz, pszFileName);

	pszExt = strrchr(sz, '.');
	if (!pszExt || strcmp(pszExt, ".sba")!=0)
		return false;

	strcpy(sz + (pszExt-sz), ".tap");

	if (nffsExists(&nffsctx, sz))
	{
		if (!MessageBox("Overwrite file", sz, MB_YESNO))
			return false;

		nffsDeleteFile(&nffsctx, sz);
	}


	err = parseAudio(pszFileName, sz);
	if (err > 0)
	{
		showFileError(err);
		return false;
	}

	MessageBox("Convert to .tap", err == 0 ? "Converted" : "Checksum Error", MB_OK);
	return true;
}

bool newFile()
{
	char szTemp[NFFS_MAX_FILENAME+1];
	uint32_t sectors = 0;
	NFFSDIRENTRY de;
	strcpy(szTemp, "");
	if (!PromptInput("New Filename:", szTemp, NFFS_MAX_FILENAME))
		return false;

	// How many sectors?
	sectors = DiskBlockCount(DiskTypeFromExtension(szTemp));

	// Create it
	return  showFileError(nffsCreateFile(&nffsctx, szTemp, sectors, sectors * 512, &de))==0;
}

size_t FileManagerWindowProc(WINDOW* pWindow, MSG* pMsg)
{
	LISTBOX* pListBox = (LISTBOX*)pWindow;
	if (pMsg->message == MESSAGE_KEYDOWN)
	{
		switch (pMsg->param1)
		{
			case VK_ESCAPE:
				g_bContinue = false;
				EndModal(0);
				return true;

			case VK_ENTER:
			{
				if (pListBox->selectedItem == 0)
				{
					if (newFile())
						EndModal(0);
				}
				else
				{
					int cmd = onFileManagerCommand(pListBox->ppItems[pListBox->selectedItem]);
					switch (cmd)
					{
						case FMC_DELETE:
							if (deleteFile(pListBox->ppItems[pListBox->selectedItem]))
								EndModal(0);
							break;

						case FMC_RENAME:
							if (renameFile(pListBox->ppItems[pListBox->selectedItem]))
								EndModal(0);
							break;

						case FMC_COPY:
							if (copyFile(pListBox->ppItems[pListBox->selectedItem]))
								EndModal(0);
							break;

						case FMC_CONVERTTTOTAP:
							if (convertToTap(pListBox->ppItems[pListBox->selectedItem]))
								EndModal(0);
							break;
					}
				}
				return 0;
			}
		}
	}

	return ListBoxWindowProc(pWindow, pMsg);
}


void FileManager()
{
	g_bContinue = true;
	while (g_bContinue)
	{
		ChooseFileEx("*", NULL, "[new]", "File Manager", FileManagerWindowProc);
	}
}
