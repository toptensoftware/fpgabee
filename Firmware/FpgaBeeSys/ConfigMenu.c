#include "common.h"
#include "RomManager.h"
#include "DiskManager.h"
#include "RomManager.h"
#include "DiskContext.h"
#include "config.h"
#include "ChooseFile.h"
#include <stdio.h>

#define COMMAND_HDD			0
#define COMMAND_ROM0		2
#define COMMAND_ROM1		3
#define COMMAND_ROM2		4
#define COMMAND_SOUND		6
#define COMMAND_TAPEMONITOR	7

static char hdd[] =  "HDD:   ***************";
static char rom0[] = "ROM 1: ***************";
static char rom1[] = "ROM 2: ***************";
static char rom2[] = "ROM 3: ***************";
static char sound[] =       "Sound:        Off";
static char tapeMonitor[] = "Tape Monitor: Off";

static char* items[] = {
	hdd,
	"\1",	
	rom0,
	rom1,
	rom2,
	"\1",
	sound,
	tapeMonitor,
	NULL
};

static void ShowNames()
{
	strcpy(hdd + 7, g_Config.hdd);
	strcpy(rom0 + 7, g_Config.rompack0);
	strcpy(rom1 + 7, g_Config.rompack1);
	strcpy(rom2 + 7, g_Config.rompack2);
	strcpy(sound + 14, g_Config.enableSound ? "On" : "Off");
	strcpy(tapeMonitor + 14, g_Config.enableTapeMonitor ? "On" : "Off");
}

static void ChooseHddImage()
{
	const char* pszFileName = ChooseFile("*.hd0;*.hd1", g_Config.hdd, "[eject]");

	// Cancelled?
	if (pszFileName == NULL)
		return;

	// Load image
	if (InsertDisk(1, pszFileName, true, true))
		return;

	// Store new drive name
	strcpy(g_Config.hdd, pszFileName);

	// Save config
	WriteConfig();

	// Update menu
	ShowNames();
}

static void ChooseRomImage(int rom)
{
	char* dest;
	const char* pszFileName;

	// Store new drive name
	switch (rom)
	{
		case 0:
			dest = g_Config.rompack0;
			break;
		case 1:
			dest = g_Config.rompack1;
			break;
		case 2:
		default:
			dest = g_Config.rompack2;
			break;
	}

	// Pick a file
	pszFileName = ChooseFile("*.rom", dest, "[eject]");

	// Cancelled?
	if (pszFileName == NULL)
		return;

	strcpy(dest, pszFileName);

	// Save config
	WriteConfig();

	// Load image (unless system rom in which case wait to reboot)
	if (rom != 0)
	{
		LoadRomPack(rom, pszFileName);
	}

	// Update menu
	ShowNames();
}

static void invokeCommand(LISTBOX* pListBox)
{
	switch (pListBox->selectedItem)
	{
		case COMMAND_HDD:
			ChooseHddImage();
			ListBoxDrawItem(pListBox, pListBox->selectedItem);
			break;

		case COMMAND_ROM0:
		case COMMAND_ROM1:
		case COMMAND_ROM2:
			ChooseRomImage(pListBox->selectedItem - COMMAND_ROM0);	
			ListBoxDrawItem(pListBox, pListBox->selectedItem);
			break;

		case COMMAND_SOUND:
			g_Config.enableSound = !g_Config.enableSound;
			ShowNames();
			ListBoxDrawItem(pListBox, pListBox->selectedItem);
			SystemFlagsPort ^= SYSTEM_FLAG_NO_SOUND;
			WriteConfig();
			break;

		case COMMAND_TAPEMONITOR:
			g_Config.enableTapeMonitor = !g_Config.enableTapeMonitor;
			ShowNames();
			ListBoxDrawItem(pListBox, pListBox->selectedItem);
			SystemFlagsPort ^= SYSTEM_FLAG_NO_TAPE_MONITOR;
			WriteConfig();
			break;
	}
}

size_t ConfigMenuProc(WINDOW* pWindow, MSG* pMsg)
{
	if (pMsg->message == MESSAGE_KEYDOWN)
	{
		switch (pMsg->param1)
		{
			case VK_ENTER:
			{
				invokeCommand((LISTBOX*)pWindow);
				return 0;
			}
		}
	}

	return ListBoxWindowProc(pWindow, pMsg);
}


void ConfigMenu()
{
	LISTBOX lb;
	memset(&lb, 0, sizeof(LISTBOX));

	ShowNames();

	lb.window.rcFrame.left = 1;
	lb.window.rcFrame.top = 3;
	lb.window.rcFrame.width = 24;
	lb.window.rcFrame.height = 10;
	lb.window.attrNormal = MAKECOLOR(COLOR_WHITE, COLOR_BLUE);
	lb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	lb.window.title = "Configure";
	lb.window.wndProc = ConfigMenuProc;
	lb.selectedItem = 0;
	ListBoxSetData(&lb, -1, items);

	// Draw list box content
	RunModalWindow(&lb.window);
}

