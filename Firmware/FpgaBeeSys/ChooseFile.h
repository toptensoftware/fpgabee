typedef struct tagCHOOSEFILECONTEXT
{
	const char* pattern;
	const char* selectedFile;
	uint8_t selectedFileIndex;
	char* pBuffer;
	const char** ppFiles;
	uint8_t nFiles;
} CHOOSEFILECONTEXT;




const char* ChooseFileEx(
	const char* pattern, 
	const char* selectedFile, 
	const char* pszNullOption, 
	const char* pszTitle,
	size_t (*onMessage)(WINDOW* pWindow, MSG* pMsg)
	);

const char* ChooseFile(const char* pattern, const char* selectedFile, const char* pszNullOption);

