#include "common.h"
#include "config.h"

int LoadRomPack(uint8_t romPackNumber, const char* filename)
{
	NFFSDIRENTRY de;
	uint32_t blockNumber;
	uint8_t i;
	int err;
	uint8_t* pDest;

	// Configure system according to ROM pack
	if (romPackNumber == 0)
	{
		// Work out system flags
		uint8_t sysFlags = 0;

		if (!g_Config.enableSound)
			sysFlags |= SYSTEM_FLAG_NO_SOUND;

		if (!g_Config.enableTapeMonitor)
			sysFlags |= SYSTEM_FLAG_NO_TAPE_MONITOR;

//		sysFlags |= SYSTEM_FLAG_DIRECT_TAPE_MONITORING;

		// ROM Pack 0 is only loaded on system boot, never from menu
		// so use this opportunity to configure the system

		if (strcmp(filename, "PC85B_BASIC.rom")==0)
		{
			// Disable video ram options on ROM basic
			sysFlags |= SYSTEM_FLAG_NO_VIDEO_COLOR | SYSTEM_FLAG_NO_VIDEO_ATTR;

			// Zap first 32k of ram
			MemoryBankPort = 0x10;
			memset(BankedMemory, 0, sizeof(BankedMemory));
			MemoryBankPort = 0x11;
			memset(BankedMemory, 0, sizeof(BankedMemory));
		}

		// Set system flags
		SystemFlagsPort = sysFlags;
	}

	// Zap old rom pack
	MemoryBankPort = 0x18 | romPackNumber;
	memset(BankedMemory, 0, sizeof(BankedMemory));
	MemoryBankPort = 0;

	// Eject?
	if (filename == NULL || filename[0] == NULL)
	{
		return 0;
	}

	// Find rom file
	err = nffsFindFile(&nffsctx, filename, &de);
	if (err)
		return err;

	// Map to rom pack
	MemoryBankPort = 0x18 | romPackNumber;

	// Read file to high memory (0x8000)
	pDest = BankedMemory;
	blockNumber = de.block;
	for (i = 0; i < de.blockCount; i++)
	{
		if (!DiskRead(blockNumber, pDest))
			return NFFS_ERR_READ;
		pDest += 512;
		blockNumber++;
	}

	MemoryBankPort = 0;
	return 0;

}