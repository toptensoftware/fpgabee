//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"
#include <string.h>

void DrawBoxIndirect(RECT* prc, uint8_t attr)
{
	DrawBox(prc->left, prc->top, prc->width, prc->height, attr);
}

void DrawBox(uint8_t left, uint8_t top, uint8_t width, uint8_t height, uint8_t attr)
{
	uint8_t i;

	// Calculate starting position
	uint8_t* p = VideoCharBuffer + left + top * SCREEN_WIDTH;

	// Top Edge
	*p = BOX_TL;
	memset(p + 1, BOX_H, width - 2);
	*(p + width - 1) = BOX_TR;
	memset(p + 0x200, attr, width);
	p += SCREEN_WIDTH;

	// Middle
	for (i=0; i<height - 2; i++)
	{
		*p = BOX_V;
		memset(p + 1, ' ', width - 2);
		*(p + width - 1) = BOX_V;
		memset(p + 0x200, attr, width);
		p += SCREEN_WIDTH;
	}

	// Top Edge
	*p = BOX_BL;
	memset(p + 1, BOX_H, width - 2);
	*(p + width - 1) = BOX_BR;
	memset(p + 0x200, attr, width);

}
