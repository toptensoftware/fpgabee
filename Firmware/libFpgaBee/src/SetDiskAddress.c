//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

#pragma disable_warning 85 // unreferenced function argument :

uint32_t g_diskBaseBlockNumber = 0;

static void setDiskAddressLL(uint32_t blockNumber)
{
__asm
		ld	hl, #2
		add hl,sp
		ld	bc,#0x0400 + _PcuDiskAddressPort
		otir
__endasm;
}

void SetDiskBaseAddress(uint32_t blockNumber)
{
	g_diskBaseBlockNumber = blockNumber;
}

uint32_t GetDiskBaseAddress()
{
	return g_diskBaseBlockNumber;
}

void SetDiskAddress(uint32_t blockNumber)
{
	setDiskAddressLL(blockNumber + g_diskBaseBlockNumber);
}
