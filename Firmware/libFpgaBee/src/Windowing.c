//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

static WINDOW* g_pActiveWindow = NULL;

size_t SendMessage(WINDOW* pWindow, uint8_t message, uint8_t param1, uint16_t param2)
{
	MSG msg;
	msg.message = message;
	msg.param1 = param1;
	msg.param2 = param2;
	return pWindow->wndProc(pWindow, &msg);
}

void GetClientRect(WINDOW* pWindow, RECT* prc)
{
	prc->left = pWindow->rcFrame.left + 1;
	prc->top = pWindow->rcFrame.top + 1;
	prc->width = pWindow->rcFrame.width - 2;
	prc->height = pWindow->rcFrame.height - 2;
}

size_t DefWindowProc(WINDOW* pWindow, MSG* pMsg)
{
	switch (pMsg->message)
	{
		case MESSAGE_DRAWFRAME:
			// Draw background
			DrawBoxIndirect(&pWindow->rcFrame, GetNormalAttr(pWindow));

			// Draw title
			if (pWindow->title[0])
				DrawText(pWindow->rcFrame.left + 1, pWindow->rcFrame.top, pWindow->rcFrame.width - 2, pWindow->title, DT_CENTER | DT_NOFILL);
			break;
	}
	return 0;
}

WINDOW* GetActiveWindow()
{
	return g_pActiveWindow;
}

// Create a window and add it to the stack
void CreateWindow(WINDOW* pWindow)
{
	// Setup the new window, capture underlying screen
	pWindow->parent = g_pActiveWindow;
	pWindow->screenSave = NULL;
	pWindow->modal = false;
	pWindow->modalRetv = 0;

	// Make it active
	g_pActiveWindow = pWindow;

	BeginOffscreen();

	// Tell the parent window to redraw itself inactive
	if (pWindow->parent)
	{
		SendMessage(pWindow->parent, MESSAGE_DRAWFRAME, 0, 0);
		SendMessage(pWindow->parent, MESSAGE_DRAWCONTENT, 0, 0);
	}

	// Save the underlying characters
	pWindow->screenSave = SaveScreen(pWindow->rcFrame.left, pWindow->rcFrame.top, pWindow->rcFrame.width, pWindow->rcFrame.height);

	// Draw this window
	SendMessage(pWindow, MESSAGE_DRAWFRAME, 0, 0);
	SendMessage(pWindow, MESSAGE_DRAWCONTENT, 0, 0);

	pWindow->needsDraw = false;

	EndOffscreen();
}

// Destroy the top window
void DestroyWindow()
{
	// Restore the underlying characters
	WINDOW* pWindow = g_pActiveWindow;

	BeginOffscreen();

	// Remove from screen
	if (pWindow->screenSave)
	{
		RestoreScreen(pWindow->screenSave);
		pWindow->screenSave = NULL;
	}

	// Deactivate
	g_pActiveWindow = pWindow->parent;

	// Redraw parent window
	if (g_pActiveWindow)
	{
		SendMessage(g_pActiveWindow, MESSAGE_DRAWFRAME, 0, 0);
		SendMessage(g_pActiveWindow, MESSAGE_DRAWCONTENT, 0, 0);
	}

	EndOffscreen();

	// Done!
}

// Run window modally (don't call create window first)
int RunModalWindow(WINDOW* pWindow)
{
	// Create the window
	CreateWindow(pWindow);

	// Mark as modal
	pWindow->modal = true;

	// Process messages till done
	while (pWindow->modal)
	{
		MSG msg;

		// Paint?
		if (pWindow->needsDraw && !PeekMessage(&msg, false))
		{
			pWindow->needsDraw = false;
			SendMessage(pWindow, MESSAGE_DRAWCONTENT, 0, 0);
		}

		// Get message
		GetMessage(&msg);

		// Invoke it
		pWindow->wndProc(pWindow, &msg);
	}

	// Kill it
	DestroyWindow();

	// Done!
	return pWindow->modalRetv;
}

// End a modal window
void EndModal(int retv)
{
	g_pActiveWindow->modal = false;
	g_pActiveWindow->modalRetv = retv;

	// make sure message loop spins to we return to caller
	EnqueueNopMessage();
}


uint8_t GetNormalAttr(WINDOW* pWindow)
{
	if (g_pActiveWindow == pWindow)
		return pWindow->attrNormal;
	else
		return MAKECOLOR(COLOR_WHITE, COLOR_DARKGRAY);
}

uint8_t GetSelectedAttr(WINDOW* pWindow)
{
	if (g_pActiveWindow == pWindow)
		return pWindow->attrSelected;
	else
		return MAKECOLOR(COLOR_DARKGRAY, COLOR_WHITE);
}

void Invalidate(WINDOW* pWindow)
{
	pWindow->needsDraw = true;
}