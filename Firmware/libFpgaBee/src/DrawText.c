//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"
#include <string.h>

void TextOut(uint8_t x, uint8_t y, const char* psz)
{
	uint8_t* p = VideoCharBuffer + x + y * SCREEN_WIDTH;
	uint8_t length = strlen(psz);
	memcpy(p, psz, length);
}

void DrawText(uint8_t x, uint8_t y, uint8_t width, const char* psz, uint8_t flags)
{
	// Measure
	uint8_t leadSpace = 0;
	uint8_t tailSpace = 0;
	uint8_t* p = VideoCharBuffer + x + y * SCREEN_WIDTH;
	uint8_t length = strlen(psz);

	// Work out alignment
	if (length > width)
	{
		length = width;
	}
	else
	{
		switch (flags & DT_ALIGNMASK)
		{
			case DT_RIGHT:
				leadSpace = width - length;
				break;

			case DT_CENTER:
				leadSpace = (width - length) / 2;
				break;
		}
	}

	// Leading space
	if ((flags & DT_NOFILL)==0 && leadSpace)
	{
		memset(p, ' ', leadSpace);
	}
	p += leadSpace;

	// Text
	memcpy(p, psz, length);
	p += length;

	// Tail space
	tailSpace = width - (length + leadSpace);
	if ((flags & DT_NOFILL)==0 && tailSpace)
	{
		memset(p, ' ', tailSpace);
	}
}
