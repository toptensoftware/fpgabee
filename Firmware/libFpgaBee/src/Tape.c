//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

extern uint32_t g_diskBaseBlockNumber;

static void setTapeAddressLL(uint32_t blockNumber)
{
	blockNumber;
__asm
		ld	hl, #2
		add hl,sp
		ld	bc,#0x0400 + _TapeBlockNumberPort
		otir
__endasm;
}

static void SendTapeAddress(uint32_t blockNumber)
{
	setTapeAddressLL(blockNumber + g_diskBaseBlockNumber);
}

void SetTapeBlocks(uint32_t firstBlock, uint32_t blockCount)
{
	SendTapeAddress(firstBlock);
	SendTapeAddress(firstBlock + blockCount);
}


static void (*g_pendingCallback)(uint8_t);

// Called by NMI handler when a tape interrupt is invoked
void onTapeNmi()
{
	uint8_t tapeStatus = TapeStatusPort;
	if (g_pendingCallback != NULL)
	{
		g_pendingCallback(tapeStatus);
	}
}

void StopTape()
{
	TapeCommandPort	= TAPE_COMMAND_STOP;
	g_pendingCallback = NULL;
}

void PlayTape(uint32_t firstBlock, uint32_t blockCount, void (*callback)(uint8_t))
{
	SetTapeBlocks(firstBlock, blockCount);
	g_pendingCallback = callback;
	TapeCommandPort = TAPE_COMMAND_PLAY;
}

void RenderTape(uint32_t firstBlock, uint32_t blockCount, uint16_t speedByteIndex, uint16_t stopByteIndex, void (*callback)(uint8_t))
{
	// Speed byte is the index of the byte in the first data block
	// at which to switch to 1200 baud
	TapeBlockNumberPort = speedByteIndex & 0xFF;
	TapeBlockNumberPort = (speedByteIndex >> 8) & 0xFF;

	// Stop byte is the index of the byte in the last block
	// at which playback should be stopped
	TapeBlockNumberPort = stopByteIndex & 0xFF;
	TapeBlockNumberPort = (stopByteIndex >> 8) & 0xFF;

	// Set the block range
	SetTapeBlocks(firstBlock, blockCount);

	// Start the render
	g_pendingCallback = callback;
	TapeCommandPort = TAPE_COMMAND_RENDER;
}

void RecordTape(uint32_t firstBlock, uint32_t blockCount, void (*callback)(uint8_t))
{
	SetTapeBlocks(firstBlock, blockCount);
	g_pendingCallback = callback;
	TapeCommandPort = TAPE_COMMAND_RECORD;
}
