//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"
#include <string.h>

typedef struct tagMESSAGEBOX
{
	WINDOW window;
	const char* pszMessage;
	int buttons;
	int selectedButton;
} MESSAGEBOX;

const char* okButtons[] = { "OK", NULL };
const char* okCancelButtons[] = { "OK", "Cancel", NULL };
const char* yesNoButtons[] = { "Yes", "No", NULL };
const char* stopButtons[] = { "Stop", NULL };
const char* cancelButtons[] = { "Cancel", NULL };
const char* stopCancelButtons[] = { "Cancel", NULL };
const char** buttonTexts[] = { okButtons, okCancelButtons, yesNoButtons, stopButtons, cancelButtons, stopCancelButtons };


int ButtonsWidth(int buttons)
{
	const char** ppsz = buttonTexts[buttons];
	int length = 0;
	while (*ppsz)
	{
		length += strlen(*ppsz) + 2;
		ppsz++;
	}
	return length;
}

int ButtonCount(int buttons)
{
	const char** ppsz = buttonTexts[buttons];
	int count = 0;
	while (*ppsz)
	{
		count++;
		ppsz++;
	}
	return count;
}

size_t MessageBoxWindowProc(WINDOW* pWindow, MSG* pMsg)
{
	MESSAGEBOX* pmb = (MESSAGEBOX*)pWindow;
	switch (pMsg->message)
	{
		case MESSAGE_DRAWCONTENT:
		{
			RECT rcClient;
			POINT pos;
			int buttonXPos;
			int button = 0;
			const char** ppszButtons = buttonTexts[pmb->buttons];

			// Draw the message
			GetClientRect(pWindow, &rcClient);
			pos.x = rcClient.left + 1;
			pos.y = rcClient.top + 1;
			DrawMultiLineText(pmb->pszMessage, &pos);

			// Draw the buttons
			buttonXPos = rcClient.left + (rcClient.width - ButtonsWidth(pmb->buttons))/2;
			while (*ppszButtons)
			{
				DrawAttr(buttonXPos, rcClient.top + rcClient.height - 1, strlen(*ppszButtons) + 2, 1, 
						button == pmb->selectedButton ? pWindow->attrSelected : pWindow->attrNormal);
				TextOut(buttonXPos + 1, rcClient.top + rcClient.height - 1, *ppszButtons);
				buttonXPos += 2 + strlen(*ppszButtons);
				ppszButtons++;	
				button++;
			}
			break;
		}

		case MESSAGE_KEYDOWN:
		{
			switch (pMsg->param1)
			{
				case VK_LEFT:
					if (pmb->selectedButton > 0)
					{
						pmb->selectedButton--;
						Invalidate(pWindow);
					}
					break;

				case VK_RIGHT:
				{
					if (pmb->selectedButton + 1 < ButtonCount(pmb->buttons))
					{
						pmb->selectedButton++;
						Invalidate(pWindow);
					}
					break;
				}

				case VK_ESCAPE:
					EndModal(0);
					break;

				case VK_ENTER:
					if (pmb->selectedButton == ButtonCount(pmb->buttons) - 1)
						EndModal(0);	// Cancel
					else
						EndModal(pmb->selectedButton+1);	// 1, 2, 3...
					break;
			}
		}

	}
	return DefWindowProc(pWindow, pMsg);
}

int MessageBox(const char* pszTitle, const char* pszMessage, int buttons)
{
	MESSAGEBOX mb;
	POINT textSize;
	int temp;
	bool error = (buttons & MB_ERROR)!=0;
	bool progress = (buttons & MB_INPROGRESS)!=0;

	buttons &= 0x0F;

	// Measure text
	MeasureMultiLineText(pszMessage, &textSize);

	// Work out width of buttons
	temp = ButtonsWidth(buttons);
	if (temp > textSize.x)
		textSize.x = temp;

	// Work out width of title
	temp = strlen(pszTitle);
	if (temp > textSize.x)
		textSize.x = temp;

	// Setup window
	memset(&mb, 0, sizeof(mb));
	mb.window.rcFrame.left = 0;
	mb.window.rcFrame.top = SCREEN_HEIGHT - 3;
	mb.window.rcFrame.width = textSize.x + 4;
	mb.window.rcFrame.height = textSize.y + 5;
	mb.window.rcFrame.left = (SCREEN_WIDTH - mb.window.rcFrame.width) / 2;
	mb.window.rcFrame.top = (SCREEN_HEIGHT - mb.window.rcFrame.height) / 2;
	mb.window.attrNormal = MAKECOLOR(COLOR_WHITE, error ? COLOR_DARKRED : (progress ? COLOR_DARKGREEN : COLOR_BLUE));
	mb.window.attrSelected = MAKECOLOR(COLOR_BLACK, COLOR_YELLOW);
	mb.window.title = pszTitle;
	mb.window.wndProc = MessageBoxWindowProc;
	mb.pszMessage = pszMessage;
	mb.buttons = buttons;
	mb.selectedButton = 0;

	// Run it...
	return RunModalWindow(&mb.window);
}