//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

char* VideoCharBuffer = (char*)0xF000;
char* VideoAttrBuffer = (char*)0xF200;

uint8_t OffscreenDepth = 0;

void BeginOffscreen()
{
	if (OffscreenDepth == 0)
	{
		memcpy((char*)0xF400, (char*)0xF000, 0x400);
		VideoCharBuffer = (char*)0xF400;
		VideoAttrBuffer = (char*)0xF600;
	}
	OffscreenDepth++;
}

void EndOffscreen()
{
	if (OffscreenDepth == 1)
	{
		memcpy((char*)0xF000, (char*)0xF400, 0x400);
		VideoCharBuffer = (char*)0xF000;
		VideoAttrBuffer = (char*)0xF200;
	}
	OffscreenDepth--;
}
