//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"


void* g_MbeeSP;
void* g_MbeeRegs[6];
void* g_PcuSP;
void* g_PcuRegs[6];
bool g_bFirstYield = true;


extern void onKeyboardNmi();
extern void onTapeNmi();

// Called by NMI Yield handler on NMI.
// Must always read any pending keys else NMI will continously be raised
// and nothing will get done!
void onNmi()
{
	uint8_t interruptMask = InterruptFlagsPort;

	// Handle keyboard interrupts
	if (interruptMask & INTERRUPT_FLAG_KEYBOARD)
	{
		onKeyboardNmi();
	}

	// Tape operation complete?
	if (interruptMask & INTERRUPT_FLAG_TAPE)
	{
		onTapeNmi();
	}
}



// NMI handler
void nmi_handler()
{
__asm

	; Save Microbee state
	ld		(_g_MbeeSP),SP
	ld		SP,#_g_MbeeRegs + 12
	push	AF
	push	BC
	push	DE
	push	HL
	push	IX
	push	IY

	; Restore PCU state
	ld		SP,(_g_PcuSP)
	pop		IY
	pop		IX
	pop		HL
	pop		DE
	pop		BC
	pop		AF

	; Control now returns to PCU

__endasm;

	onNmi();
}



void YieldNmiProc()
{
__asm
	; Save PCU state
	push	AF
	push	BC
	push	DE
	push	HL
	push	IX
	push	IY
	ld		(_g_PcuSP),SP

	; Restore Microbee state
	ld		SP,#_g_MbeeRegs
	pop		IY
	pop		IX
	pop		HL
	pop		DE
	pop		BC

	; If this is the first yield we need to return with ret (not retn)
	ld      a,(_g_bFirstYield)
	or      a
	jr      nz,$1

	pop		AF
	ld		SP,(_g_MbeeSP)
	out		(_RequestPcuModeExitPort),A

	retn	; NB: RETN

$1:
	; Clear flag
	xor		a
	ld      (_g_bFirstYield),A

	; Yield...
	pop		AF
	ld		SP,(_g_MbeeSP)
	out		(_RequestPcuModeExitPort),A
	ret     ; NB: RET

__endasm;
}

