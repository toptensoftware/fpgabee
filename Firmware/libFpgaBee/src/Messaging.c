//////////////////////////////////////////////////////////////////////////////
// libFpgaBee

#include "common.h"

#define EVENT_BUFFER_SIZE	8		// Must be power of two

static MSG g_eventBuffer[EVENT_BUFFER_SIZE];
static uint8_t g_eventBufferWritePos = 0;
static uint8_t g_eventBufferReadPos = 0;


void copyMessage(MSG* dest, MSG* src)
{
	dest->message = src->message;
	dest->param1 = src->param1;
	dest->param2 = src->param2;
}

void EnqueueMessage(MSG* pMessage)
{
	uint8_t nextWritePos;

	// Write it to the queue
	nextWritePos = (g_eventBufferWritePos + 1) & (EVENT_BUFFER_SIZE-1);
	if (nextWritePos != g_eventBufferReadPos)
	{
		copyMessage(&g_eventBuffer[g_eventBufferWritePos], pMessage);
		g_eventBufferWritePos = nextWritePos;
	}
}

void EnqueueNopMessage()
{
	MSG msg;
	msg.message = MESSAGE_NOP;
	msg.param1 = 0;
	msg.param2 = 0;
	EnqueueMessage(&msg);
}

void onKeyboardNmi()
{
	while (true)
	{
		// Get the event
		uint8_t lo = KeyboardPortLo;
		uint8_t hi = KeyboardPortHi;
		uint16_t key = (hi << 8) | lo;
		uint8_t vk;

		// Quit if no more
		if ((hi & 0x80)==0)
			break;

		// Clear NMI flag
		KeyboardPortHi = 0;

		DebugHexLo = lo;
		DebugHexHi = hi;

		vk = ScanCodeToVK(key);
		if (vk != 0)
		{
			char ch = VKToChar(vk, key);

			MSG msg;
			msg.message = MESSAGE_KEYDOWN;
			msg.param1 = vk;
			msg.param2 = 0;

			EnqueueMessage(&msg);
			if (ch)
			{
				msg.message = MESSAGE_CHAR;
				msg.param1 = ch;
				EnqueueMessage(&msg);
			}
		}
	}
}

bool PeekMessage(MSG* pMsg, bool remove)
{
	while (true)
	{
		// Anything in the queue?
		if (g_eventBufferReadPos == g_eventBufferWritePos)
			return false;

		// Yep
		copyMessage(pMsg, &g_eventBuffer[g_eventBufferReadPos]);

		if (pMsg->message == MESSAGE_KEYDOWN && pMsg->param1 == VK_F12)
		{
			if (DisplayModePort == 0)
				DisplayModePort = DISPLAY_MODE_ENABLE_VIDEO | DISPLAY_MODE_ENABLE_KEYBOARD;
			else
				DisplayModePort = 0;
			g_eventBufferReadPos = (g_eventBufferReadPos + 1) & (EVENT_BUFFER_SIZE-1);
			continue;
		}

		if (remove)
		{
			g_eventBufferReadPos = (g_eventBufferReadPos + 1) & (EVENT_BUFFER_SIZE-1);
		}

		return true;
	}
}

void GetMessage(MSG* pMsg)
{
	while (true)
	{
		if (PeekMessage(pMsg, true))
			return;

		Yield();
	}
}
