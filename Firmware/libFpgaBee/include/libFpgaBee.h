////////////////////////////////////////////////////////////////////////////////////////////////
// FPGABee.h - hardware interface

#ifndef __FPGABEE_H
#define __FPGABEE_H

#include <stdint.h>
#include <stdbool.h>

////////////////////////////////////////////////////////////////////////////////////////////////
// Video Controller

#define SCREEN_WIDTH		32
#define SCREEN_HEIGHT		16

//__at(0xF000) char VideoCharBuffer[SCREEN_WIDTH * SCREEN_HEIGHT];
//__at(0xF200) char VideoAttrBuffer[SCREEN_WIDTH * SCREEN_HEIGHT];

extern char* VideoCharBuffer;
extern char* VideoAttrBuffer;


__sfr __at(0x81) DisplayModePort;

#define DISPLAY_MODE_ENABLE_VIDEO			0x01
#define DISPLAY_MODE_ENABLE_KEYBOARD		0x02

__sfr __at(0x84) SystemFlagsPort;

#define SYSTEM_FLAG_NO_VIDEO_COLOR			0x01
#define SYSTEM_FLAG_NO_VIDEO_ATTR			0x02	
#define SYSTEM_FLAG_NO_TAPE_MONITOR			0x04
#define SYSTEM_FLAG_NO_SOUND				0x08
#define SYSTEM_FLAG_DIRECT_TAPE_MONITORING	0x10	// Bypass DAC and send tape directly to speakers (for diagnostics on scope)

// Reading this port tells the kind of interrupt(s) that
// caused the NMI to occur.  NMI handler must read the
// ports associated with the kinds of interrupts to clear
// the interrupt mask otherwise another NMI will occur immediately
__sfr __at(0x85) InterruptFlagsPort;

#define INTERRUPT_FLAG_KEYBOARD				0x01
#define INTERRUPT_FLAG_TAPE					0x02
#define INTERRUPT_FLAG_PCUDISK				0x04

// Colors
#define COLOR_BLACK			0
#define COLOR_DARKRED		1
#define COLOR_DARKGREEN		2
#define COLOR_BROWN			3
#define COLOR_DARKBLUE		4
#define COLOR_DARKMAGENTA	5
#define COLOR_DARKCYAN		6
#define COLOR_GRAY			7
#define COLOR_DARKGRAY		8
#define COLOR_RED			9
#define COLOR_GREEN			10
#define COLOR_YELLOW		11
#define COLOR_BLUE			12
#define COLOR_MAGENTA		13
#define COLOR_CYAN			14
#define COLOR_WHITE			15

#define MAKECOLOR(fg, bg) ((uint8_t)(((bg) << 4) | (fg)))


// PCU Character rom is reduced character set.
// These are the box draw character codes
#define BOX_TL	((char)6)
#define BOX_TR	((char)3)
#define BOX_BL	((char)4)
#define BOX_BR	((char)5)
#define BOX_H	((char)1)
#define BOX_V	((char)2)
#define CH_CURSOR	((char)20)


////////////////////////////////////////////////////////////////////////////////////////////////
// PCU Mode

// Writing anything to this will request the FPGA to switch out of 
// PCU mode back to regular Microbee mode
//    Must be immediately followed by RET or RETN

__sfr __at(0x80) RequestPcuModeExitPort;


////////////////////////////////////////////////////////////////////////////////////////////////
// Memory banking

// Bit 7 - boot rom latch (1 on boot)
// Bit 6 - unused
// Bit 5 - unused
// Bit 4 - MicroBee RAM/ROM latch
// Bits 3..0  = high 4 bits of the 18-bit external ram address
//				(Gives PCU access to all external RAM in 16k Pages)

__sfr __at(0xD0) MemoryBankPort;

#define MEMORY_BANK_BOOT_LATCH_BIT		0x80
#define MEMORY_BANK_RAM_LATCH_BIT		0x10

__at(0xC000) char BankedMemory[16384];


////////////////////////////////////////////////////////////////////////////////////////////////
// Soft reset request

// Writing anything to this port will reset the microbee

__sfr __at(0xFF) SoftResetRequestPort;


////////////////////////////////////////////////////////////////////////////////////////////////
// Debug LEDS

__sfr __at(0xA0) DebugLeds;
__sfr __at(0xA1) DebugHexLo;
__sfr __at(0xA2) DebugHexHi;


////////////////////////////////////////////////////////////////////////////////////////////////
// PCU Disk Controller


__sfr __at(0xC0) PcuDiskDataPort;
__sfr __at(0xC1) PcuDiskAddressPort;
__sfr __at(0xC7) PcuDiskStatusPort;
__sfr __at(0xC7) PcuDiskCommandPort;

// PCU Disk status bits
#define DISK_STATUS_BIT_BUSY			0x80 	// Disk controller is busy
#define DISK_STATUS_BIT_INITIALIZED		0x40    // Disk controller has initialized
#define DISK_STATUS_BIT_ERROR			0x01    // Disk controller has an DISK_STATUS_BIT_ERROR

// PCU Disk commands
#define DISK_COMMAND_REWIND_BUFFER		0
#define DISK_COMMAND_READ				1
#define DISK_COMMAND_WRITE				2


////////////////////////////////////////////////////////////////////////////////////////////////
// Tape Controller

__sfr __at(0xC9) TapeBlockNumberPort;
__sfr __at(0xCF) TapeCommandPort;
__sfr __at(0xCF) TapeStatusPort;

#define TAPE_STATUS_BIT_ERROR			0x80
#define TAPE_STATUS_BIT_FINISHED		0x04
#define TAPE_STATUS_BIT_RECORDING		0x02
#define TAPE_STATUS_BIT_PLAYING			0x01

#define TAPE_COMMAND_STOP			0
#define TAPE_COMMAND_PLAY			1		// Play .sba file
#define TAPE_COMMAND_RECORD			2		// Record .sba file
#define TAPE_COMMAND_RENDER			3		// Play .tap file
#define TAPE_COMMAND_LOADCURBLOCK	7

////////////////////////////////////////////////////////////////////////////////////////////////
// Virtual Disk Controller (only in PCU Mode)

__sfr __at(0x40) VdcAddressPort;
__sfr __at(0x41) VdcCommandPort;

// To set disk configuration
// Bits 6 downto 3 = disk type
// Bits 2 downto 0 = disk number
#define DISK_COMMAND_DISKNUMBERMASK		0x07
#define DISK_COMMAND_DISKTYPE       	0x78

// Disk type constants
#define DISK_TYPE_DS40 0
#define DISK_TYPE_SS80 1
#define DISK_TYPE_DS80 2
#define DISK_TYPE_DS82 3
#define DISK_TYPE_DS84 4
#define DISK_TYPE_DS8B 5
#define DISK_TYPE_HD0 6
#define DISK_TYPE_HD1 7
#define DISK_TYPE_NONE 8


////////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard

// Read keyboard event port
// Reading the hi byte clears the pending event
__sfr __at(0x82) KeyboardPortLo;
__sfr __at(0x83) KeyboardPortHi;

// Key codes
#define VK_BACKTICK 0x01
#define VK_LSHIFT 0x02
#define VK_RSHIFT 0x03
#define VK_LCTRL 0x04
#define VK_RCTRL 0x05
#define VK_LMENU 0x06
#define VK_RMENU 0x07
#define VK_BACKSPACE 0x08
#define VK_TAB 0x09
#define VK_COMMA 0x0A
#define VK_PERIOD 0x0B
#define VK_HYPHEN 0x0C
#define VK_ENTER 0x0D
#define VK_SEMICOLON 0x0E
#define VK_EQUALS 0x0F
#define VK_ESCAPE 0x10
#define VK_F1 0x11
#define VK_F2 0x12
#define VK_F3 0x13
#define VK_F4 0x14
#define VK_F5 0x15
#define VK_F6 0x16
#define VK_F7 0x17
#define VK_F8 0x18
#define VK_F9 0x19
#define VK_F10 0x1a
#define VK_F11 0x1b
#define VK_F12 0x1c
#define VK_LSQUARE 0x1d
#define VK_RSQUARE 0x1e
#define VK_QUOTE 0x1f
#define VK_SPACE 0x20
#define VK_LEFT 0x21
#define VK_RIGHT 0x22
#define VK_UP 0x23
#define VK_DOWN 0x24
#define VK_HOME 0x25
#define VK_END 0x26
#define VK_NEXT 0x27
#define VK_PRIOR 0x28
#define VK_INSERT 0x29
#define VK_DELETE 0x2a
#define VK_SLASH 0x2b
#define VK_BACKSLASH 0x3a
#define VK_CAPITAL 0x3b
#define VK_NUMENTER 0x3c
#define VK_SUBTRACT 0x5b
#define VK_MULTIPLY 0x5c
#define VK_DIVIDE 0x5d
#define VK_ADD 0x5e
#define VK_DECIMAL 0x5F
#define VK_NUM0 0x60
#define VK_NUM1 0x61
#define VK_NUM2 0x62
#define VK_NUM3 0x63
#define VK_NUM4 0x64
#define VK_NUM5 0x65
#define VK_NUM6 0x66
#define VK_NUM7 0x67
#define VK_NUM8 0x68
#define VK_NUM9 0x69

#define VK_POWER 0xFF				// extended 0x37
#define VK_VOLUME_MUTE 0xAD			// extended 0x23
#define VK_LAUNCH_MAIL 0xB4			// extended 0x48
#define VK_BROWSER_HOME 0xAC		// extended 0x3a
#define VK_MEDIA_PREV_TRACK 0xB1	// extended 0x15
#define VK_MEDIA_PLAY_PAUSE 0xB3	// extended 0x34
#define VK_MEDIA_NEXT_TRACK 0xB0	// extended 0x4d
#define VK_MEDIA_STOP 0xB2			// extended 0x3b
#define VK_VOLUME_DOWN 0xAE			// extended 0x21
#define VK_VOLUME_UP 0xAF			// extended 0x32


////////////////////////////////////////////////////////////////////////////////////////////////
// Scheduling

// Call from anywhere that needs to yield
extern void (*Yield)();

// Yield implementations
void YieldNopProc();
void YieldNmiProc();

////////////////////////////////////////////////////////////////////////////////////////////////
// Messaging

#define MESSAGE_NOP			0
#define MESSAGE_KEYDOWN  	1
#define MESSAGE_CHAR	 	2
#define MESSAGE_DRAWFRAME	3
#define MESSAGE_DRAWCONTENT	4


typedef struct tagMSG
{
	uint8_t message;
	uint8_t param1;
	uint16_t param2;
} MSG;


// Enqueue a message to the message queue
void EnqueueMessage(MSG* pMessage);

// Enqueue a nop message
void EnqueueNopMessage();

// Peek the next available message (if any)
bool PeekMessage(MSG* pMessage, bool remove);

// Get next queued message
void GetMessage(MSG* pMessage);


////////////////////////////////////////////////////////////////////////////////////////////////
// Disk helpers

// Disk operations
void DiskInit();
uint32_t GetDiskBaseAddress();
void SetDiskBaseAddress(uint32_t blockNumber);
void SetDiskAddress(uint32_t blockNumber);
bool DiskRead(uint32_t blockNumber, void* buffer);
bool DiskWrite(uint32_t blockNumber, void* buffer);


////////////////////////////////////////////////////////////////////////////////////////////////
// Tape helpers

void SetTapeBlocks(uint32_t firstBlock, uint32_t blockCount);

void StopTape();
void PlayTape(uint32_t firstBlock, uint32_t blockCount, void (*callback)(uint8_t));
void RenderTape(uint32_t firstBlock, uint32_t blockCount, uint16_t speedByteIndex, uint16_t stopByteIndex, void (*callback)(uint8_t));
void RecordTape(uint32_t firstBlock, uint32_t blockCount, void (*callback)(uint8_t));


////////////////////////////////////////////////////////////////////////////////////////////////
// Drawing

#define DT_LEFT			0
#define DT_RIGHT 		1
#define DT_CENTER 		2
#define DT_ALIGNMASK 	0x03
#define DT_NOFILL 		0x80		// Otherwise fill unused area with spaces

typedef struct tagRECT
{
	uint8_t left;
	uint8_t top;
	uint8_t width;
	uint8_t height;
} RECT;

typedef struct tagPOINT
{
	uint8_t x;
	uint8_t y;
} POINT;


// Drawing
void BeginOffscreen();
void EndOffscreen();
void DrawAttr(uint8_t left, uint8_t top, uint8_t width, uint8_t height, uint8_t attr);
void DrawBox(uint8_t left, uint8_t top, uint8_t width, uint8_t height, uint8_t attr);
void DrawBoxIndirect(RECT* prc, uint8_t attr);
void TextOut(uint8_t x, uint8_t y, const char* psz);
void DrawText(uint8_t x, uint8_t y, uint8_t width, const char* psz, uint8_t flags);
void MeasureMultiLineText(const char* psz, POINT* pVal);
void DrawMultiLineText(const char* psz, POINT* pPos);
void* SaveScreen(uint8_t left, uint8_t top, uint8_t width, uint8_t height);
void RestoreScreen(void* saveData);

void ScrollScreen(uint8_t left, uint8_t top, uint8_t width, uint8_t height, 
					int dy, bool attr, 
					uint8_t* pRedrawFrom, uint8_t* pRedrawCount);

////////////////////////////////////////////////////////////////////////////////////////////////
// Windowing

typedef struct tagWINDOW
{
// public:
	RECT rcFrame;
	uint8_t attrNormal;
	uint8_t attrSelected;
	void* user;
	const char* title;
	size_t (*wndProc)(struct tagWINDOW* pWindow, MSG* pMessage);

// private/readonly:
	struct tagWINDOW* parent;
	void* screenSave;
	bool modal;
	int modalRetv;
	bool needsDraw;
} WINDOW;

// Send a message to window
size_t SendMessage(WINDOW* pWindow, uint8_t message, uint8_t param1, uint16_t param2);

// The default window procedure
size_t DefWindowProc(WINDOW* pWindow, MSG* pMsg);

void GetClientRect(WINDOW* pWindow, RECT* prc);

// Get the window on the top of the stack
WINDOW* GetActiveWindow();

// Create a window and add it to the stack
void CreateWindow(WINDOW* pWindow);

// Destroy the top window
void DestroyWindow();

// Run window modally (don't call create window first)
int RunModalWindow(WINDOW* pWindow);

// End a modal window
void EndModal(int retv);

// Invalidate the window so it'll be redrawn when no messages left in queue
void Invalidate(WINDOW* pWindow);

uint8_t GetNormalAttr(WINDOW* pWindow);

uint8_t GetSelectedAttr(WINDOW* pWindow);

////////////////////////////////////////////////////////////////////////////////////////////////
// Listbox

typedef struct tagLISTBOX
{
// Window
	WINDOW window;

// Public
	int selectedItem;

// Pull Data source
	int (*getItemCount)(struct tagLISTBOX* pListBox);
	const char* (*getItemText)(struct tagLISTBOX* pListBox, int item);

// Push data source
	int itemCount;
	const char** ppItems;

// Internal
	uint8_t	state;
	int scrollPos;
} LISTBOX;

size_t ListBoxWindowProc(WINDOW* pWindow, MSG* pMsg);

void ListBoxDraw(LISTBOX* pListBox);
void ListBoxDrawItem(LISTBOX* pListBox, int item);
bool ListBoxMessage(LISTBOX* pListBox, MSG* pMsg);
int ListBoxEnsureVisible(LISTBOX* pListBox, int item);
bool ListBoxSelect(LISTBOX* pListBox, int item);
void ListBoxSetData(LISTBOX* pListBox, int itemCount, const char** ppItems);

int ListBoxModal(LISTBOX* pListBox);

////////////////////////////////////////////////////////////////////////////////////////////////
// Text Prompt

bool PromptInput(const char* pszTitle, char* buf, int cbBuf);


////////////////////////////////////////////////////////////////////////////////////////////////
// MessageBox

#define MB_OK			0	
#define MB_OKCANCEL		1
#define MB_YESNO		2
#define MB_STOP			3
#define MB_CANCEL		4
#define MB_STOPCANCEL	5
#define MB_INPROGRESS	0x40	// Green display
#define MB_ERROR		0x80	// Red display

// Returns 0 if last button pressed
// Otherwise 1 based button index
int MessageBox(const char* pszTitle, const char* pszMessage, int buttons);

////////////////////////////////////////////////////////////////////////////////////////////////
// Keyboard mapping

uint8_t ScanCodeToVK(uint16_t keyEvent);
char VKToChar(uint8_t vk, uint16_t keyEvent);

#endif

