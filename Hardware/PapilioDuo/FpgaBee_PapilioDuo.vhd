-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.



library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library unisim;
use unisim.vcomponents.all;

use work.SDStatusBits.ALL;

entity FpgaBee_PapilioDuo is
port 
( 
	clock : in std_logic;
	reset : in std_logic;

	sram_addr : out std_logic_vector(20 downto 0);
	sram_data : inout std_logic_vector(7 downto 0);
	sram_ce : out std_logic;
	sram_we : out std_logic;
	sram_oe : out std_logic;

	vga_red: out std_logic_vector(3 downto 0);
	vga_green: out std_logic_vector(3 downto 0);
	vga_blue: out std_logic_vector(3 downto 0);
	vga_hsync: out std_logic;
	vga_vsync: out std_logic;

	ps2_keyboard_data : inout std_logic;
	ps2_keyboard_clock : inout std_logic;

	audio1_left : out std_logic;
	audio1_right : out std_logic;

 	leds : out std_logic_vector(3 downto 0);

	sd_sclk : out std_logic;
	sd_mosi : out std_logic;
	sd_miso : in std_logic;
	sd_ss_n : out std_logic
);
end FpgaBee_PapilioDuo;

architecture Behavioral of FpgaBee_PapilioDuo is

	signal clock_108_000 : std_logic;
	signal clock_40_000 : std_logic;

	signal speaker_internal : std_logic;

	signal ram_addr : std_logic_vector(17 downto 0);
	signal ram_rd_data : std_logic_vector(7 downto 0);
	signal ram_wr_data : std_logic_vector(7 downto 0);
	signal ram_rd : std_logic;
	signal ram_wr : std_logic;
	signal boot_rom_latch : std_logic;

	signal boot_rom_range : std_logic;
    signal boot_rom_dout : std_logic_vector(7 downto 0);

    signal sd_status : std_logic_vector(7 downto 0);
    signal pcu_debug : std_logic_vector(7 downto 0);
    signal fpga_debug : std_logic_vector(7 downto 0);
    signal disk_status : std_logic_vector(7 downto 0);

begin

	clock_core1 : entity work.ClockCore
	PORT MAP 
	(
		CLK_IN_32_000 => clock,
		CLK_OUT_108_000 => clock_108_000,
		CLK_OUT_40_000 => clock_40_000
	);

	-- Unused VGA Colors
	vga_red(0) <= '0';
	vga_red(1) <= '0';
	vga_green(0) <= '0';
	vga_green(1) <= '0';
	vga_blue(0) <= '0';
	vga_blue(1) <= '0';

	-- Audio to both channels
	audio1_left <= speaker_internal;
	audio1_right <= speaker_internal;

	-- Show SD status on LEDs
	leds(0) <= sd_status(STATUS_BIT_READING);
	leds(1) <= sd_status(STATUS_BIT_WRITING);
	leds(2) <= sd_status(STATUS_BIT_ERROR);
	leds(3) <= sd_status(STATUS_BIT_INIT);

	-- FPGABee Core
	FpgaBeeCore : entity work.FpgaBeeCore
	PORT MAP
	(
		clock_40_000 => clock_40_000,
		clock_108_000 => clock_108_000,

		reset_button => reset,
		monitor_key => '0',
		show_status_panel => '1',

		ram_addr => ram_addr,
		ram_rd_data => ram_rd_data,
		ram_wr_data => ram_wr_data,
		ram_wr => ram_wr,
		ram_rd => ram_rd,
		ram_wait => '0',

		boot_rom_latch => boot_rom_latch,

		vga_red => vga_red(3 downto 2),
		vga_green => vga_green(3 downto 2),
		vga_blue => vga_blue(3 downto 2),
		vga_hsync => vga_hsync,
		vga_vsync => vga_vsync,
		vga_pixel_x => open,
		vga_pixel_y => open,

		sd_sclk => sd_sclk,
		sd_mosi => sd_mosi,
		sd_miso => sd_miso,
		sd_ss_n => sd_ss_n,

		ps2_keyboard_data => ps2_keyboard_data,
		ps2_keyboard_clock => ps2_keyboard_clock,

		sd_status => sd_status,

		speaker => speaker_internal
	);


	-- Is it in the PCU ROM range (0x30000 - 0x33FFF)
	boot_rom_range <= '1' when ram_addr(17 downto 14)="1100" and boot_rom_latch='1' else '0';

	-- Read RAM or PCU Rom?
    ram_rd_data <= boot_rom_dout when boot_rom_range ='1' else sram_data;

    -- RAM
	sram_addr <= "000" & ram_addr;
	sram_data <= ram_wr_data when ram_wr='1' else "ZZZZZZZZ";
	sram_ce <= '0' when (ram_rd='1' or ram_wr='1') and boot_rom_range='0' else '1';
	sram_we <= '0' when (ram_wr='1') else '1';
	sram_oe <= '0' when (ram_rd='1') else '1';

    -- PCU firmware
    boot_rom : entity work.BootRom
    PORT MAP
    (
        clock => clock_108_000,
        addr => ram_addr(11 downto 0),
        dout => boot_rom_dout
    );

end Behavioral;

