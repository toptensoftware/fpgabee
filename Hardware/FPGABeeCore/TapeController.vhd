-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.DiskConstants.ALL;
use work.SDStatusBits.ALL;
 
entity TapeController is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;								-- CPU Clock
		clken_3_375 : in std_logic;								-- CPU Clock enable

		-- CPU interface
		cpu_port : in std_logic_vector(2 downto 0);			-- low 3 bits of the z80 port
		cpu_wr : in std_logic;								-- write signal
		cpu_rd : in std_logic;								-- read signal
		cpu_din : in std_logic_vector(7 downto 0);			-- output from z80, input to controller
		cpu_dout : out std_logic_vector(7 downto 0);		-- output from controller, input to z80

		-- Audio Interface
		audio_play : out std_logic;
		audio_record : in std_logic;

		-- PCU notification interrupt
		intreq : out std_logic;								-- asserts when disk controller is finished and status hasn't been read
		pcu_mode : in std_logic;

		-- SDCard Arbitration
		sd_arb_req : out std_logic;
		sd_arb_ack : in std_logic;

		-- SDCard Interface
		sd_status : in std_logic_vector(7 downto 0);
		sd_op_wr : out std_logic;
		sd_op_cmd : out std_logic_vector(1 downto 0);
		sd_op_block_number : out std_logic_vector(31 downto 0);
		sd_dstart : in std_logic;
		sd_dcycle : in std_logic;
		sd_dout : in std_logic_vector(7 downto 0);
		sd_din : out std_logic_vector(7 downto 0)

	);
end TapeController;


 
architecture behavior of TapeController is 


	-- Edge detection the for CPU read/write port signals
	-- (we need this since the Z80 always introduces one wait state
	--  for port instructions and we don't want to inadvertantly double
	--  invoke commands, or increments our DMA buffer address)
	signal cpu_wr_prev : std_logic;
	signal cpu_rd_prev : std_logic;

	-- DMA buffer - CPU side access
	signal cpuram_we : std_logic;
	signal cpuram_addr : std_logic_vector(9 DOWNTO 0);
	signal cpuram_din : std_logic_vector(7 DOWNTO 0);
	signal cpuram_dout : std_logic_vector(7 DOWNTO 0);

	-- DMA buffer - SD controller side
	signal sdram_we : std_logic;
	signal sdram_addr : std_logic_vector(9 DOWNTO 0);
	signal sdram_dout : std_logic_vector(7 DOWNTO 0);

	-- Command execution state machine
	type exec_states is 
	(
		STATE_READY,
		STATE_SD_ARBWAIT,
		STATE_SD_BUSY,
		STATE_PLAYING,
		STATE_RECORDING
	);

	signal exec_state : exec_states;				-- Execution state
	signal exec_cmd : std_logic_vector(1 downto 0);	-- 00 = stop, 01 = play, 02 = record, 03 = render
	signal exec_request : std_logic;				-- Request a read/write
	signal exec_error : std_logic;					-- Error occured
	signal exec_finished : std_logic;				-- 1 = out of space for recording or reach end of playback. 0 = SD read/write error

	signal is_playing : std_logic;
	signal is_recording : std_logic;
	signal is_rendering : std_logic;

	signal block_numbers : std_logic_vector(95 downto 0);
	signal speed_byte_index: std_logic_vector(9 downto 0);
	signal stop_byte_index : std_logic_vector(8 downto 0);
	signal block_number_first : std_logic_vector(31 downto 0);
	signal block_number_last : std_logic_vector(31 downto 0);
	signal block_number_used : std_logic_vector(31 downto 0);

	signal sd_op_block_number_int : std_logic_vector(31 downto 0);
	signal sd_op_cmd_int : std_logic_vector(1 downto 0);

	signal render_byte : std_logic_vector(10 downto 0);
	signal render_byte_mask : std_logic_vector(10 downto 0);
	signal render_1200baud : std_logic;
	signal in_first_block : std_logic;
	signal in_last_block : std_logic;

	signal clken_sampleRate : std_logic;
	signal sampleRateDivideCounter : unsigned(10 downto 0);

	signal clken_audio4800 : std_logic;
	signal audio4800DivideCounter : unsigned(15 downto 0);
	signal audio_phaseCounter : unsigned(3 downto 0);

	signal silent_bit : std_logic;
	signal silent_sample_count : unsigned(15 downto 0);
	signal waiting_for_signal : std_logic;
	signal raise_interrupt : std_logic;
	
begin


	speed_byte_index <= block_numbers(9 downto 0);		-- 10 bit
	stop_byte_index <= block_numbers(24 downto 16);		-- 9 bit
	block_number_first <= block_numbers(63 downto 32);
	block_number_last <= block_numbers(95 downto 64);

	sd_op_block_number <= sd_op_block_number_int;
	sd_op_cmd <= sd_op_cmd_int;

	-- Front end CPU interface
	process (clock_108_000)
	begin

		if rising_edge(clock_108_000) then
		if reset = '1' then

			cpu_wr_prev <= '0';
			cpu_rd_prev <= '0';


			exec_request <= '0';
			intreq <= '0';

		elsif clken_3_375='1' then

			-- Track read/write edges
			cpu_wr_prev <= cpu_wr;
			cpu_rd_prev <= cpu_rd;

			-- Reset ram write line
			exec_request <= '0';

			-- Raise interrupt
			if raise_interrupt = '1' then
				intreq <= '1';
			end if;

			-- Port write?
			if cpu_wr='1' and cpu_wr_prev='0' then

				case cpu_port is

					when "001" =>
						-- WRITE BLOCK NUMBER
						-- LSB first
						block_numbers <= cpu_din & block_numbers(95 downto 8);

					when "111" =>
						-- COMMAND
						case cpu_din(2 downto 0) is
							when "000" =>
								-- Stop
								exec_cmd <= "00";
								exec_request <= '1';

							when "001" =>
								-- Play
								exec_cmd <= "01";
								exec_request <= '1';

							when "010" =>
								-- Record
								exec_cmd <= "10";
								exec_request <= '1';

							when "011" =>
								-- Render
								exec_cmd <= "11";
								exec_request <= '1';

							when "111" =>
								-- Capture last used block number
								block_number_used <= sd_op_block_number_int;

							when others => 
								null;

						end case;

					when others =>
						null;

				end case;

			elsif cpu_rd='1' and cpu_rd_prev='0' then

				case cpu_port is

					when "001" =>
						-- READ END BLOCK NUMBER
						-- LSB first
						cpu_dout <= block_number_used(7 downto 0);
						block_number_used <= "00000000" & block_number_used(31 downto 8);

					when "111" => 
						-- STATUS
						cpu_dout(7) <= exec_error;
						cpu_dout(6 downto 3) <= (others => '0');
						cpu_dout(2) <= exec_finished;

						if exec_state = STATE_READY then
							cpu_dout(1 downto 0) <= "00";
						else
							cpu_dout(1 downto 0) <= exec_cmd;
						end if;

						-- status has been read so clear the intreq signal
						intreq <= '0';

					when others =>
						cpu_dout <= (others=>'0');

				end case;

			end if;

		end if;

		end if;

	end process;


	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then
			sampleRateDivideCounter <= to_unsigned(153, 11);
			clken_sampleRate <= '0';
		elsif clken_3_375='1' and pcu_mode='0' then

			-- decrement counter
			sampleRateDivideCounter <= sampleRateDivideCounter - 1;

			-- enable clock
			if sampleRateDivideCounter = 0 then
				clken_sampleRate <= '1';
				sampleRateDividecounter <= to_unsigned(153, 11);
			else
				clken_sampleRate <= '0';
			end if;
		end if;
		end if;
	end process;


	-- Generate a 4800Hz clock as the time base for 
	-- rendered audio (108Mhz / 22500 = 4800Hz)
	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			audio4800DivideCounter <= to_unsigned(703, 16);
			clken_audio4800 <= '0';

		elsif clken_3_375='1' and pcu_mode='0' then

			-- decrement counter
			audio4800DivideCounter <= audio4800DivideCounter - 1;

			-- enable clock
			if audio4800DivideCounter = 0 then
				clken_audio4800 <= '1';
				audio4800DivideCounter <= to_unsigned(703, 16);
			else
				clken_audio4800 <= '0';
			end if;

		end if;
		end if;
	end process;


	-- Command execution unit
	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			exec_state <= STATE_READY;
			exec_error <= '0';
			exec_finished <= '0';
			sd_op_wr <= '0';
			sd_op_cmd_int <= "00";
			sd_op_block_number_int <= x"00000000";

			cpuram_addr <= (others=>'0');
			cpuram_we <= '0';
			cpuram_din <= (others => '0');

			sdram_addr(9) <= '0';

			render_byte <= (others => '0');
			is_playing <= '0';
			is_recording <= '0';
			is_rendering <= '0';

			audio_phaseCounter <= "0000";
			render_1200baud <= '0';

			silent_bit <= '0';
			silent_sample_count <= to_unsigned(0, 16);
			waiting_for_signal <= '1';

			audio_play <= '0';
			raise_interrupt <= '0';

		elsif clken_3_375='1' then 

			sd_op_wr <= '0';
			cpuram_we <= '0';

			if is_playing='1' then

				-- Time for next sample?
				if clken_sampleRate='1' and pcu_mode='0' and waiting_for_signal='0' then

					-- output the lowest bit
					audio_play <= render_byte(0);

					-- get the next bit
					render_byte <= "0000" & render_byte(7 downto 1);

					-- shift byte mask
					render_byte_mask <= "0000" & render_byte_mask(7 downto 1);

				end if;


			elsif is_rendering='1' then

				-- Render audio
				if clken_audio4800='1' and pcu_mode='0' and waiting_for_signal='0' then

					-- Update our phase counter
					audio_phaseCounter <= audio_phaseCounter + 1;

					-- Render the audio signal
					if render_byte(0) = '1' then
	
						audio_play <= not audio_phaseCounter(0);
	
					else
	
						audio_play <= not audio_phaseCounter(1);
	
					end if;
	
					-- Move to next bit?
					if render_1200baud = '1' then
	
						-- Render at 1200 baud
						if audio_phaseCounter(1 downto 0) = "11" then
	
							render_byte <= "0" & render_byte(10 downto 1);
							render_byte_mask <= "0" & render_byte_mask(10 downto 1);
	
						end if;
	
					else
	
						-- Render at 300 baud
						if audio_phaseCounter(3 downto 0) = "1111" then
	
							render_byte <= "0" & render_byte(10 downto 1);
							render_byte_mask <= "0" & render_byte_mask(10 downto 1);
	
						end if;
	
					end if;

				end if;

			else
				
				audio_play <= '0';

			end if;


			if is_recording='1' and clken_sampleRate='1' and pcu_mode='0' then

				if waiting_for_signal='1' then

					-- Recording has started but waiting for 
					-- something to happen before actually capturing
					-- samples
					if audio_record /= silent_bit then

						-- recording has really started
						waiting_for_signal <= '0';

						-- input the lowest bit
						render_byte <= "000" & audio_record & render_byte(7 downto 1);

						-- shift byte mask
						render_byte_mask <= "0000" & render_byte_mask(7 downto 1);

					end if;

				else
					
					-- input the lowest bit
					render_byte <= "000" & audio_record & render_byte(7 downto 1);

					-- shift byte mask
					render_byte_mask <= "0000" & render_byte_mask(7 downto 1);

					-- check for 1 second of silence to auto stop recording
					if audio_record = silent_bit then

						-- nothing changed
						if silent_sample_count = 0 then

							-- timeout
							exec_state <= STATE_READY;

						else

							-- decrement timer
							silent_sample_count <= silent_sample_count - 1;

						end if;
					else

						-- Reset the silence detection count (1/4 second)
						silent_sample_count <= to_unsigned(5500, 16);

						-- Remember the last bit
						silent_bit <= audio_record;

					end if;

				end if;

			end if;

			-- Has the current byte finished rendering?
			if render_byte_mask(0)='0' then

				if is_playing='1' then

					-- get the next byte from ram
					render_byte(7 downto 0) <= cpuram_dout;
					render_byte_mask <= "000" & x"FF";

					-- increment the RAM addres for the next byte
					cpuram_addr <= std_logic_vector(unsigned(cpuram_addr)+1);

				end if;

				if is_recording='1' then

					-- write the next byte
					cpuram_din <= render_byte(7 downto 0);
					cpuram_we <= '1';

					-- setup shift counter for the next byte
					render_byte_mask <= "000" & x"FF";

				end if;

				if is_rendering='1' then

					-- get the next byte from ram
					render_byte <= "11" & cpuram_dout & "0";
					render_byte_mask <= (others => '1');

					-- Switch to speed mode?
					if cpuram_addr = speed_byte_index and in_first_block='1' then
						render_1200baud <= '1';
					end if;

					-- increment the RAM address for the next byte
					cpuram_addr <= std_logic_vector(unsigned(cpuram_addr)+1);

				end if;

			end if;

			if cpuram_we = '1' then

				-- increment the RAM address for the next byte
				cpuram_addr <= std_logic_vector(unsigned(cpuram_addr)+1);

			end if;

			-- Clear the first block flag
			if cpuram_addr = "1111111111" then

				in_first_block <= '0';

			end if;

			-- Stop request?
			if exec_request = '1' and exec_cmd = "00" then
				exec_state <= STATE_READY;
			end if;

			raise_interrupt <= '0';

			-- Handle states
			case exec_state is
				when STATE_READY =>

					-- Generate an interrupt request if
					-- we were doing something and now we're not
					if sd_op_cmd_int /= "00" then
						raise_interrupt <= '1';
					end if;

					-- Reset state
					sd_op_cmd_int <= "00";
					is_playing <= '0';
					is_recording <= '0';
					is_rendering <= '0';
					waiting_for_signal <= '1';
					render_1200baud <= '0';
					in_first_block <= '1';
					in_last_block <= '0';

					if exec_request='1' then

						case exec_cmd is

							when "01" =>
								-- play
								is_playing <= '1';

								-- Request SD from arbiter
								sd_arb_req <= '1';

								-- Wait for ack
								exec_state <= STATE_SD_ARBWAIT;

								-- Clear error
								exec_error <= '0';
								exec_finished <= '0';

								-- Load the first block number and the read command
								sd_op_block_number_int <= block_number_first;
								sd_op_cmd_int <= "01";

								-- Skip the header
								cpuram_addr <= "00" & x"10";
								sdram_addr(9) <= '0';

							when "10" =>
								-- record

								-- Clear error
								exec_error <= '0';
								exec_finished <= '0';

								-- Setup for writing
								sd_op_block_number_int <= block_number_first;
								sd_op_cmd_int <= "10";

								-- Start recording after the header
								cpuram_addr <= "00" & x"10";
								sdram_addr(9) <= '0';

								-- Set recording flag
								is_recording <= '1';

								-- Start collecting audio samples
								exec_state <= STATE_RECORDING;

								-- Setup for silence detection (1/4 second)
								silent_bit <= audio_record;
								silent_sample_count <= to_unsigned(5500, 16);
								waiting_for_signal <= '1';

								null;

							when "11" =>
								-- play
								is_rendering <= '1';

								-- Request SD from arbiter
								sd_arb_req <= '1';

								-- Wait for ack
								exec_state <= STATE_SD_ARBWAIT;

								-- Clear error
								exec_error <= '0';
								exec_finished <= '0';

								-- Load the first block number and the read command
								sd_op_block_number_int <= block_number_first;
								sd_op_cmd_int <= "01";

								-- Skip the 13 byte "TAP_DGOS_MBEE" header
								cpuram_addr <= "00" & x"0D";
								sdram_addr(9) <= '0';

							when others => 
								null;

						end case;	
					
					else

						-- make sure arb is released
						sd_arb_req <= '0';

					end if;
			
				when STATE_SD_ARBWAIT =>

					-- Wait for access to the SD card 
					if sd_arb_ack='1' then
						exec_state <= STATE_SD_BUSY;
						sd_op_wr <= '1';
					end if;

				when STATE_SD_BUSY =>
					
					-- Operation finished?
					if sd_status(STATUS_BIT_BUSY)='0' then

						-- error?
						if sd_status(STATUS_BIT_ERROR)='1' then

							exec_error <= '1';
							exec_state <= STATE_READY;

						elsif is_recording='1' then

							if sd_op_block_number_int = block_number_last then

								-- We just wrote the last available block so
								-- we're out of room. Stop
								exec_state <= STATE_READY;
								exec_error <= '1';
								exec_finished <= '1';

							else

								-- Continue recording
								exec_state <= STATE_RECORDING;

								-- Setup to write the next recorded block
								sdram_addr(9) <= cpuram_addr(9);

							end if;

						else

							-- Did we just read the last block?
							if sd_op_block_number_int = block_number_last then
								in_last_block <= '1';
							end if;

							-- Start playing
							exec_state <= STATE_PLAYING;

						end if;

						-- Release arb
						sd_arb_req <= '0';


					end if;

				when STATE_PLAYING =>
					-- Set playing flag
					waiting_for_signal <= '0';

					-- If we're rendering and we're in the last block and
					-- we've reached the stop byte index... then stop!
					if in_last_block = '1' and is_rendering='1' and cpuram_addr(8 downto 0) = stop_byte_index then
						exec_state <= STATE_READY;
						exec_finished <= '1';
					end if;

					-- When audio render moves into the block that was
					-- just read, start the next read operation
					if cpuram_addr(9) = sdram_addr(9) then

						sdram_addr(9) <= NOT cpuram_addr(9);

						if in_last_block='1' then

							exec_state <= STATE_READY;
							exec_finished <= '1';

						else

							-- Request SD from arbiter
							sd_arb_req <= '1';

							-- Start the next read
							exec_state <= STATE_SD_ARBWAIT;

							-- Setup the next block number
							sd_op_block_number_int <= std_logic_vector(unsigned(sd_op_block_number_int) + 1);

						end if;

					end if;

				when STATE_RECORDING => 
					if cpuram_addr(9) /= sdram_addr(9) then

						-- Block is full, request access to the SD card
						sd_op_block_number_int <= std_logic_vector(unsigned(sd_op_block_number_int) + 1);
						sd_arb_req <= '1';
						exec_state <= STATE_SD_ARBWAIT;
					
					end if;

			end case;

		end if;
		end if;
	end process;

	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			sdram_addr(8 downto 0) <= (others => '0');

		else

			if sd_dstart = '1' then

				-- Data start
				sdram_addr(8 downto 0) <= (others => '0');

			elsif sd_dcycle='1' then

				-- Data cycle
				sdram_addr(8 downto 0) <= std_logic_vector(unsigned(sdram_addr(8 downto 0)) + 1);

				-- Work out buffer utilitization
				--if sdram_addr(8 downto 0)="111111111" then
				--	if sdram_addr(9) = cpuram_addr(9) then
				--		buffer_util(9) <= '1';
				--	else
				--		buffer_util(9) <= '0';
				--	end if;
				--	buffer_util(8 downto 0) <= cpuram_addr(8 downto 0);
				--end if;

			end if;

		end if;
		end if;
	end process;

	-- SD Controller -> RAM
	sdram_we <= '1' when (sd_dcycle='1' and sd_arb_ack='1' and sd_op_cmd_int="01") else '0';

	-- Sector buffer
	ram : entity work.RamInferred	
	GENERIC MAP
	(
		ADDR_WIDTH => 10
	)
	PORT MAP
	(
		clock_a => clock_108_000,
		wr_a => cpuram_we,
		addr_a => cpuram_addr,
		din_a => cpuram_din,
		dout_a => cpuram_dout,

		clock_b => clock_108_000,
		wr_b => sdram_we,
		addr_b => sdram_addr,
		din_b => sd_dout,
		dout_b => sd_din
	);

end;
