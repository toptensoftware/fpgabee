-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.DiskConstants.ALL;
 
entity DiskControllerWD1002 is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;								-- CPU Clock
		clken_3_375 : in std_logic;								-- CPU Clock enable

		-- CPU Interface
		cpu_port : in std_logic_vector(3 downto 0);			-- low 4 bits of the z80 port
		cpu_wr : in std_logic;								-- write signal
		cpu_rd : in std_logic;								-- read signal
		cpu_din : in std_logic_vector(7 downto 0);			-- output from z80, input to controller
		cpu_dout : out std_logic_vector(7 downto 0);		-- output from controller, input to z80

		-- VDC Interace
		cmd : out std_logic_vector(2 downto 0);			-- VDC_CMD_xxx commands
		cmd_din : out std_logic_vector(7 downto 0);
		cmd_dout : in std_logic_vector(7 downto 0);
		cmd_track : out std_logic_vector(15 downto 0);
		cmd_drive : out std_logic_vector(2 downto 0);
		cmd_head : out std_logic_vector(2 downto 0);
		cmd_sector : out std_logic_vector(7 downto 0);
		cmd_status_busy : in std_logic;
		cmd_status_error : in std_logic;
		cmd_status_geo_error : in std_logic;
		cmd_flag_buffer : in std_logic;
		cmd_status_nodisk : in std_logic
	);
end DiskControllerWD1002;


-- Registers
--      rd					wr
-- 0	data				data
-- 1	error				precomp
-- 2	sector count		sector count
-- 3	sector number		sector number
-- 4	track lo			track lo
-- 5	track hi			track hi
-- 6	SDH					SDH
-- 7	status				command

-- STA_BUSY       		"10000000"   -- drive busy
-- STA_RDY        		"01000000"   -- drive ready
-- STA_WF         		"00100000"   -- write fault
-- STA_SC         		"00010000"   -- seek complete
-- STA_DRQ        		"00001000"   -- data request bit - drive ready to transfer data
-- STA_CORR       		"00000100"   -- soft error detected
-- STA_NOTUSED    		"00000010"   -- not used
-- STA_ERROR      		"00000001"   -- set when there is a drive error.

-- HDD_SDH_CRCECC     	"10000000"
-- HDD_SDH_SIZE       	"01100000"
-- HDD_SDH_DRIVE      	"00011000"
-- HDD_SDH_HDHEAD     	"00000111"
-- HDD_SDH_FDSEL      	"00000110"
-- HDD_SDH_FDHEAD     	"00000001"

-- HDD_ERR_BAD_BLOCK  	"10000000"   -- bad block detect
-- HDD_ERR_UNREC      	"01000000"   -- unrecoverable error
-- HDD_ERR_CRC_ERR_ID 	"00100000"   -- CRC error ID field
-- HDD_ERR_ID_NFOUND  	"00010000"   -- ID not found
-- HDD_ERR_NOTUSED    	"00001000"   -- -
-- HDD_ERR_ABORT_CMD  	"00000100"   -- aborted command
-- HDD_ERR_TR000      	"00000010"   -- TR000 error
-- HDD_ERR_DAM_NFOUND 	"00000001"   -- DAM not found

 
architecture behavior of DiskControllerWD1002 is 


	-- This it the WD1002's "taskfile"
	type regfile_type is array (0 to 7) of std_logic_vector(7 downto 0);
	signal regfile : regfile_type;
	signal reg_status_drq : std_logic;
	signal reg_port48 : std_logic;

	-- These continuous assignments decode the taskfile into more usable representation
	signal reg_sector_count : std_logic_vector(7 downto 0);
	signal reg_sdh :std_logic_vector(7 downto 0);

	-- Edge detection the for CPU read/write port signals
	-- (we need this since the Z80 always introduces one wait state
	--  for port instructions and we don't want to inadvertantly double
	--  invoke commands, or increments our DMA buffer address)
	signal cpu_wr_prev : std_logic;
	signal cpu_rd_prev : std_logic;

	type disk_info_type is record
		base_block_number : std_logic_vector(31 downto 0);
		disk_type : std_logic_vector(3 downto 0);
	end record;

	signal cmd_drive_int : std_logic_vector(2 downto 0);
	signal write_pending : std_logic;
	signal read_pending : std_logic;

begin

	-- Decode registers
	reg_sdh <= regfile(6);
	cmd_track <= regfile(5) & regfile(4);
	cmd_drive_int <= "1" & reg_sdh(2 downto 1) when reg_sdh(4 downto 3)="11" else "0" & reg_sdh(4 downto 3);
	cmd_drive <= cmd_drive_int;
	cmd_head <= "00" & (reg_sdh(0) or reg_port48) when cmd_drive_int(2)='1' else reg_sdh(2 downto 0);
	cmd_sector <= regfile(3);
	reg_sector_count <= regfile(2);

	-- Front end CPU interface
	process (clock_108_000)
	begin

		if rising_edge(clock_108_000) then
		if reset = '1' then

			regfile <= (others=>(others=>'0'));
			reg_port48 <= '0';

			cpu_wr_prev <= '0';
			cpu_rd_prev <= '0';

			write_pending <= '0';
			read_pending <= '0';

		elsif clken_3_375='1' then

			-- Track read/write edges
			cpu_wr_prev <= cpu_wr;
			cpu_rd_prev <= cpu_rd;

			-- No command by default
			cmd <= VDC_CMD_NOP;

			if cmd_flag_buffer='1' then

				reg_status_drq <= '0';
				if write_pending ='1' then
					cmd <= VDC_CMD_WRITE_BLOCK;
					write_pending <= '0';
				end if;

			end if;

			-- Finished?
			if cmd_status_busy = '0' then


			 	if read_pending='1' then
					reg_status_drq <= '1';
					read_pending <= '0';
				end if;

			end if;	

			-- Port write?
			if cpu_wr='1' and cpu_wr_prev='0' then

				case cpu_port is

					when "0000" =>

						cmd <= VDC_CMD_WRITE_BYTE;
						cmd_din <= cpu_din;

					when "0111" =>
						if cmd_status_busy='0' then  -- (ignore all write operations while busy)

							write_pending <= '0';
							read_pending <= '0';

							case cpu_din(7 downto 4) is

								when "0001" =>      

									-- Invoke restore command
									cmd <= VDC_CMD_RESTORE;


								when "0010" =>      
									-- READ command
									reg_status_drq <= '0';
									if cpu_din(2)='0' then
										-- sector count = 1
										regfile(2) <= x"01";		
									end if;
									read_pending <= '1';
									cmd <= VDC_CMD_READ_BLOCK;

								when "0011" =>      
									-- WRITE command
									reg_status_drq <= '1';
									if cpu_din(2)='0' then
										-- sector count = 1
										regfile(2) <= x"01";		
									end if;
									cmd <= VDC_CMD_RESTORE;
									write_pending <= '1';

								when others =>		

									cmd <= VDC_CMD_RESTORE;

							end case;
						end if;

					when "0001" | "0010" | "0011" | "0100" | "0101" | "0110" => 
						-- register write
						regfile(to_integer(unsigned(cpu_port(2 downto 0)))) <= cpu_din;

					when "1000" =>
						-- Write to port 48h
						reg_port48 <= cpu_din(0);

					when others =>
						null;

				end case;

			elsif cpu_rd='1' and cpu_rd_prev='0' then

				case cpu_port is

					when "0000" =>
						-- READ DATA
						cpu_dout <= cmd_dout;
						cmd <= VDC_CMD_READ_BYTE;		-- move to next byte

					when "0001" =>
						-- ERROR register
						cpu_dout(7) <= '0';
						cpu_dout(6) <= '0';
						cpu_dout(5) <= '0';
						cpu_dout(4) <= (cmd_status_error and cmd_status_geo_error) or cmd_status_nodisk;
						cpu_dout(3) <= '0';
						cpu_dout(2) <= '0';
						cpu_dout(1) <= (cmd_status_error and cmd_status_error) or cmd_status_nodisk;
						cpu_dout(0) <= cmd_status_error;

					when "0111" =>
						-- STATUS register
						cpu_dout(7) <= cmd_status_busy;
						cpu_dout(6) <= '1';		-- ready
						cpu_dout(5) <= '0';
						cpu_dout(4) <= '1';		-- sc
						cpu_dout(3) <= reg_status_drq;
						cpu_dout(2) <= '0';
						cpu_dout(1) <= '0';
						cpu_dout(0) <= cmd_status_error or cmd_status_nodisk;

					when "0010" | "0011" | "0100" | "0101" | "0110" => 
						-- REGISTER read
						cpu_dout <= regfile(to_integer(unsigned(cpu_port(2 downto 0))));

					when "1000" =>
						--Read port 48h
						cpu_dout <= "0000000" & reg_port48;


					when others =>
						cpu_dout <= (others=>'0');

				end case;

			end if;

		end if;
		end if;
	end process;


end;
