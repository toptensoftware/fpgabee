-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.

library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity FpgaBeeCore is
Port 
( 
	-- Clocks
	clock_108_000 : in std_logic;		-- 108Mhz primary clock
	clock_40_000 : in std_logic;		-- Video Pixel Clock

	-- Buttons
	reset_button : in std_logic;
	monitor_key : in std_logic;			-- 1 to fake 'M' key
 	show_status_panel : in std_logic;	-- 1 to show status panel

	-- Access to 256K of off-chip RAM
	-- 
	-- 0x00000 - 0x1FFFF = 128K Main Microbee Memory (4 x 32K banks)
	-- 0010 0000 0x20000 - 0x23FFF - 16K Rom Pack 0 (maps to Microbee Z80 addr 0x8000)
	-- 0010 0100 0x24000 - 0x27FFF - 16K Rom Pack 1 (maps to Microbee Z80 addr 0xC000)
	-- 0010 1000 0x28000 - 0x2BFFF - 16K Rom Pack 2 (maps to Microbee Z80 addr 0xC000)
	-- 0x2C000 - 0x2FFFF - Unused
	-- 0x30000 - 0x3FFFF - 64K PCU ROM/RAM (maps to Microbee Z80 addr 0x0000 when in pcu_mode)
	-- 
	-- NB: 
	-- 1) ROM packs are loaded by PCU from SD card, but could be mapped to board 
	--		flash/ROM if preferred. 
	-- 2) The CPU starts execution from Z80 address 0x0000 in PCU mode (ie:0x30000 in above 
	--		memory map).  At 0x30000 needs to be either:
	--			a) the PCU firmware in flash/ROM
	--			b) a boot ROM to load the PCU firmware from SD card.
	ram_addr : out std_logic_vector(17 downto 0);
	ram_rd_data : in std_logic_vector(7 downto 0);
	ram_wr_data : out std_logic_vector(7 downto 0);
	ram_wr : out std_logic;
	ram_rd : out std_logic;
	ram_wait : in std_logic;
	boot_rom_latch : out std_logic;		-- Mapped to Port D0 bit 6

	-- VGA
	vga_red: out std_logic_vector(1 downto 0);
	vga_green: out std_logic_vector(1 downto 0);
	vga_blue: out std_logic_vector(1 downto 0);
	vga_hsync: out std_logic;
	vga_vsync: out std_logic;
	vga_pixel_x : out std_logic_vector(10 downto 0);		-- Optional, use to externally generate overlaid video
	vga_pixel_y : out std_logic_vector(10 downto 0);

	-- SD Card
	sd_sclk : out std_logic;
	sd_mosi : out std_logic;
	sd_miso : in std_logic;
	sd_ss_n : out std_logic;
	sd_status : out std_logic_vector(7 downto 0);

	-- Keyboard
	ps2_keyboard_data : inout std_logic;
	ps2_keyboard_clock : inout std_logic;

	-- Audio
	speaker : out std_logic


);
end FpgaBeeCore;

architecture Behavioral of FpgaBeeCore is

	constant SysFlagNoVideoColor : natural := 0;
	constant SysFlagNoVideoAttr : natural := 1;
	constant SysFlagNoTapeMonitor : natural := 2;
	constant SysFlagNoSound : natural := 3;
	constant sysFlagDirectTapeMonitor : natural :=4;

	signal clken_3_375_divider : unsigned(4 downto 0);
	signal clken_3_375 : std_logic;
    signal boot_scan : std_logic := '1';
	signal output_clock : std_logic;
	signal z80_dout : std_logic_vector(7 downto 0);
	signal z80_din : std_logic_vector(7 downto 0);
	signal z80_addr : std_logic_vector(15 downto 0);
	signal z80_mreq_n : std_logic;
	signal z80_iorq_n : std_logic;
	signal z80_rd_n : std_logic;
	signal z80_wr_n : std_logic;
	signal z80_m1_n : std_logic;
	signal z80_wait_n : std_logic;
	signal z80_nmi_n : std_logic := '1';
	signal char_ram_wea : std_logic;
	signal char_ram_dout : std_logic_vector(7 downto 0);
	signal pcgram_wea : std_logic;
	signal pcgram_dout : std_logic_vector(7 downto 0);
	signal charrom_dout : std_logic_vector(7 downto 0);
	signal seg7_wea : std_logic;
	signal led_reg : std_logic_vector(7 downto 0);
	signal hex_reg : std_logic_vector(15 downto 0);
	signal hex_signal : std_logic_vector(15 downto 0);
	signal mem_rd : std_logic;
	signal mem_wr : std_logic;
	signal port_rd : std_logic;
	signal port_wr : std_logic;
	signal ram_range : std_logic;
	signal ram_range_ro : std_logic;			-- '1' when accessing ROM's loaded into RAM
	signal char_ram_range : std_logic;
	signal attr_ram_range : std_logic;
	signal charrom_range : std_logic;
	signal pcg_ram_range : std_logic;
	signal color_ram_range : std_logic;
	signal latch_rom : std_logic := '0';
	signal small_char_set_selected : std_logic;
	signal charrom_addr_b : std_logic_vector(11 downto 0);
	signal charrom_dout_b : std_logic_vector(7 downto 0);
	signal char_ram_addr_b : std_logic_vector(10 downto 0);
	signal char_ram_dout_b : std_logic_vector(7 downto 0);
	signal port_1c : std_logic_vector(7 downto 0) := x"00";
	signal port_08 : std_logic_vector(7 downto 0) := x"00";
	signal port_50 : std_logic_vector(7 downto 0) := x"00";
	signal port_D0 : std_logic_vector(7 downto 0) := x"80";
	signal attr_ram_wea : std_logic;
	signal attr_ram_dout : std_logic_vector(7 downto 0);
	signal attr_ram_dout_b : std_logic_vector(7 downto 0);
	signal attr_ram_dout_b_resolved : std_logic_vector(7 downto 0);
	signal color_ram_wea : std_logic;
	signal color_ram_dout : std_logic_vector(7 downto 0);
	signal color_ram_dout_b : std_logic_vector(7 downto 0);
	signal color_ram_dout_b_resolved : std_logic_vector(7 downto 0);
	signal pcgram_addr_crtc : std_logic_vector(14 downto 0);
	signal pcgram_addr_b : std_logic_vector(13 downto 0);
	signal pcgram_dout_b : std_logic_vector(7 downto 0);
	signal pcgram_dout_b_range_tested : std_logic_vector(7 downto 0);
	signal crtc_addr_port : std_logic;
	signal crtc_data_port : std_logic;
	signal crtc_wr : std_logic;
	signal crtc_rd : std_logic;
	signal crtc_dout : std_logic_vector(7 downto 0);
	signal vsync_internal : std_logic;
	signal pio_port_b : std_logic_vector(7 downto 0);
	signal MicrobeeSwitches : std_logic_vector(0 to 63);
	signal shift_key_pressed : std_logic;
	signal ctrl_key_pressed : std_logic;
	signal status_hex : std_logic_vector(63 downto 0);
	signal status_leds : std_logic_vector(7 downto 0);
	signal KeyboardMessageAvailable : std_logic;
	signal KeyboardMessageAvailable_prev : std_logic;
	signal KeyboardMessage : std_logic_vector(9 downto 0);

	signal sd_arb_req : std_logic_vector(2 downto 0);
	signal sd_arb_ack : std_logic_vector(2 downto 0);

	signal pcudisk_port : std_logic;
	signal pcudisk_port_write : std_logic;
	signal pcudisk_port_read : std_logic;
	signal pcudisk_dout : std_logic_vector(7 downto 0);
	signal pcudisk_sd_op_wr : std_logic;
	signal pcudisk_sd_op_cmd : std_logic_vector(1 downto 0);
	signal pcudisk_sd_op_block_number : std_logic_vector(31 downto 0);
	signal pcudisk_sd_din : std_logic_vector(7 downto 0);
	signal pcudisk_intreq : std_logic;

	signal tape_audio_play : std_logic;
	signal tape_audio_record : std_logic;
	signal tape_port : std_logic;
	signal tape_port_write : std_logic;
	signal tape_port_read : std_logic;
	signal tape_dout : std_logic_vector(7 downto 0);
	signal tape_sd_op_wr : std_logic;
	signal tape_sd_op_block_number : std_logic_vector(31 downto 0);
	signal tape_sd_op_cmd : std_logic_vector(1 downto 0);
	signal tape_sd_din : std_logic_vector(7 downto 0);
	signal tape_intreq : std_logic;

	signal wddisk_port : std_logic;
	signal wddisk_port_write : std_logic;
	signal wddisk_port_read : std_logic;
	signal wddisk_dout : std_logic_vector(7 downto 0);
	signal wddisk_sd_op_wr : std_logic;
	signal wddisk_sd_op_cmd : std_logic_vector(1 downto 0);
	signal wddisk_sd_op_block_number : std_logic_vector(31 downto 0);
	signal wddisk_sd_din : std_logic_vector(7 downto 0);

	signal vdc_port : std_logic;
	signal vdc_port_write : std_logic;
	signal vdc_port_read : std_logic;
	signal vdc_dout : std_logic_vector(7 downto 0);

	signal sd_status_int : std_logic_vector(7 downto 0);
	signal sd_op_wr : std_logic;
	signal sd_op_cmd : std_logic_vector(1 downto 0);
	signal sd_op_block_number : std_logic_vector(31 downto 0);
	signal sd_cur_block_number : std_logic_vector(31 downto 0);
	signal sd_dstart : std_logic;
	signal sd_dcycle : std_logic;
	signal sd_din : std_logic_vector(7 downto 0);
	signal sd_dout : std_logic_vector(7 downto 0);

	signal video_blank : std_logic;
	signal vga_pixel_x_internal : std_logic_vector(10 downto 0);
	signal vga_pixel_y_internal : std_logic_vector(10 downto 0);

	signal vgaRed_mbee: std_logic_vector(1 downto 0);
	signal vgaGreen_mbee: std_logic_vector(1 downto 0);
	signal vgaBlue_mbee: std_logic_vector(1 downto 0);
	signal vgaRed_pcu: std_logic_vector(1 downto 0);
	signal vgaGreen_pcu: std_logic_vector(1 downto 0);
	signal vgaBlue_pcu: std_logic_vector(1 downto 0);
	signal pcu_pixel_visible : std_logic;

	signal status_pixel : std_logic_vector(1 downto 0);

	signal pcu_mode : std_logic := '1';
	signal pcu_display_mode : std_logic_vector(7 downto 0) := x"00";
	signal pcu_exit_request : std_logic := '0';

	signal pcu_char_ram_wea : std_logic;
	signal pcu_char_ram_dout : std_logic_vector(7 downto 0);
	signal pcu_char_ram_addr_b : std_logic_vector(8 downto 0);
	signal pcu_char_ram_dout_b : std_logic_vector(7 downto 0);
	signal pcu_color_ram_wea : std_logic;
	signal pcu_color_ram_dout : std_logic_vector(7 downto 0);
	signal pcu_color_ram_dout_b : std_logic_vector(7 downto 0);
	signal pcu_char_ram_range : std_logic;
	signal pcu_color_ram_range : std_logic;

	signal pcu_key_available : std_logic;
	signal pcu_key_data : std_logic_vector(9 downto 0);
	signal pcu_key_important : std_logic;

	signal current_pc : std_logic_vector(15 downto 0) := x"0000";

	signal translated_address : std_logic_vector(17 downto 0);

	signal soft_reset_shifter : std_logic_vector(7 downto 0) := x"FF"; 
	signal request_soft_reset : std_logic := '0';
	signal reset_s : std_logic;
	signal reset_n : std_logic;

	signal dac_i : std_logic_vector(7 downto 0);
	signal dac_o : std_logic;
	signal speaker_volume : signed(7 downto 0) := x"F0";
	signal speaker_aout : signed(7 downto 0);
	signal tape_volume : signed(7 downto 0) := x"F0";
	signal tape_aout : signed(7 downto 0);
	signal pardac : unsigned(7 downto 0) := x"00";

	signal psg_ce_n : std_logic;
	signal psg_we_n : std_logic;
	signal psg_ready : std_logic;
	signal psg_out : signed(0 to 7);
	signal psg_wait : std_logic;
	signal psg_aout : signed(0 to 7);

	signal system_flags : std_logic_vector(7 downto 0);

	signal vdc_cmd : std_logic_vector(2 downto 0);
	signal vdc_cmd_din : std_logic_vector(7 downto 0);
	signal vdc_cmd_dout : std_logic_vector(7 downto 0);
	signal vdc_cmd_track : std_logic_vector(15 downto 0);
	signal vdc_cmd_drive : std_logic_vector(2 downto 0);
	signal vdc_cmd_head : std_logic_vector(2 downto 0);
	signal vdc_cmd_sector : std_logic_vector(7 downto 0);
	signal vdc_cmd_status_busy : std_logic;
	signal vdc_cmd_status_error : std_logic;
	signal vdc_cmd_status_geo_error : std_logic;
	signal vdc_cmd_flag_buffer : std_logic;
	signal vdc_cmd_status_nodisk : std_logic;

begin

	-- Main CPU clock
	-- (108Mhz / 32 = 3.375)

	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset_button='1' then
			clken_3_375_divider <= to_unsigned(0, 5);
		else
			clken_3_375_divider <= clken_3_375_divider + 1;
		end if;
		end if;
	end process;

	process (clken_3_375_divider)
	begin
		clken_3_375 <= '0';
		if pcu_mode = '1' then
			if clken_3_375_divider(1 downto 0) = "00" then
				clken_3_375 <= '1';
			end if;
		else
			if clken_3_375_divider = 0 then
				clken_3_375 <= '1';
			end if;
		end if;
	end process;
	--clken_3_375 <= '1' when clken_3_375_divider=0 else '0';

	-- Misc continuous assignments
	reset_s <= '1' when soft_reset_shifter(0)='1' or reset_button='1' else '0';
	reset_n <= NOT reset_s;

	pio_port_b(0) <= '0';

	vga_pixel_x <= vga_pixel_x_internal;
	vga_pixel_y <= vga_pixel_y_internal;

	-- Status data
	status_hex(15 downto 0) <= current_pc;
	status_hex(31 downto 16) <= (others => '0');
	status_hex(63 downto 32) <= sd_cur_block_number;
	status_leds <= led_reg;

	boot_rom_latch <= port_D0(7);

	tape_audio_record <= pio_port_b(1);

	process (KeyboardMessage)
	begin

		case KeyboardMessage is

			when "00" & x"07" | 	-- F12
			 	 "01" & x"37" |	 -- Power
				 "01" & x"23" |	 -- Media Keys Volumn Mute
				 "01" & x"48" |	 -- Launch Email
				 "01" & x"3a" |	 -- Launch Browser
				 "01" & x"15" |	 -- Media Keys Previous Track
				 "01" & x"34" |	 -- Media Keys Play Pause
				 "01" & x"4d" |	 -- Media Keys Next Track
				 "01" & x"3b" |	 -- Media Keys Stop
				 "01" & x"21" |	 -- Media Keys Volume Down
				 "01" & x"32" => -- Media Keys Volumn Up
				pcu_key_important <= '1';

			when others =>
				pcu_key_important <= '0';

		end case;

	end process;
	
	-- Output video
	process (vgaRed_mbee, vgaGreen_mbee, vgaBlue_mbee, 
				vgaRed_pcu, vgaGreen_pcu, vgaBlue_pcu,
				pcu_display_mode, pcu_pixel_visible, 
				status_pixel, video_blank)
	begin

		if video_blank='1' or status_pixel="01" then

			-- Video blank, or status panel background
			vga_red <= "00";
			vga_green <= "00";
			vga_blue <= "00";

		elsif status_pixel="10" then

			-- Dull status pixel (off led)
			vga_red <= "10";
			vga_green <= "10";
			vga_blue <= "10";

		elsif status_pixel="11" then

			-- Bright status pixel
			vga_red <= "11";
			vga_green <= "00";
			vga_blue <= "00";

		elsif pcu_pixel_visible='1' and pcu_display_mode(0)='1' then

			-- PCU display
			vga_red <= vgaRed_pcu;
			vga_green <= vgaGreen_pcu;
			vga_blue <= vgaBlue_pcu;

		else

			-- Microbee display
			vga_red <= vgaRed_mbee;
			vga_green <= vgaGreen_mbee;
			vga_blue <= vgaBlue_mbee;

		end if;

	end process;

	-- Soft reset
	process(clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset_button='1' then

			soft_reset_shifter <= (others=>'0');

		elsif clken_3_375='1' then

			if request_soft_reset='1' then
				soft_reset_shifter <= (others=>'1');
			else
				soft_reset_shifter <= '0' & soft_reset_shifter(7 downto 1);
			end if;

		end if;
		end if;
	end process;

	-- Reset, clock and simple port writes...
	process(clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset_s='1' then
		
			system_flags <= (others => '0');
			boot_scan <= '1';
			latch_rom <= '0';
			port_1c <= (others=>'0');
			port_08 <= (others=>'0');
			port_50 <= (others=>'0');
			port_D0 <= "10000000";		-- Boot rom latch on
			led_reg <= (others=>'0');
			hex_reg <= (others=>'0');
			z80_nmi_n <= '1';
			pcu_mode <= '1';
			pcu_exit_request <= '0';
			pcu_display_mode <= (others=>'0');
			request_soft_reset <= '0';
			pardac <= (others=>'0');
			pio_port_b(7 downto 1) <= (others => '0');
			current_pc <= (others => '0');
			pcu_key_available <= '0';
			pcu_key_data <= (others => '0');

		elsif clken_3_375='1' then

			request_soft_reset <= '0';

			-- On first read of a memory address >= 0x8000, clear the boot scan flag
			if pcu_mode='0' and z80_m1_n='0' and boot_scan='1' and z80_addr(15)='1' then
				boot_scan <= '0';
			end if;

			if z80_addr(7 downto 0)=x"00" and port_wr='1' then
				pardac <= unsigned(z80_dout);
			end if;
			
			if z80_addr(7 downto 0)=x"A0" and port_wr='1' then
				led_reg <= z80_dout;
			end if;
			
			if z80_addr(7 downto 0)=x"A1" and port_wr='1' then
				hex_reg(7 downto 0) <= z80_dout;
			end if;
			
			if z80_addr(7 downto 0)=x"A2" and port_wr='1' then
				hex_reg(15 downto 8) <= z80_dout;
			end if;
			
			-- Port 0b (11) is the VDU latch rom
			-- when 1, mem 0xF000 -> FFFFF comes from charrom
			-- when 0, mem 0xF000 -> FFFFF is the video/pcg ram
			if z80_addr(7 downto 0)=x"0b" and port_wr='1' then
				latch_rom <= z80_dout(0);
			end if;
			
			-- Write to port 2 (PIO port B)
			if z80_addr(7 downto 0)=x"02" and port_wr='1' then
				pio_port_b(7 downto 1) <= z80_dout(7 downto 1);		-- not the 0 bit which is used for tape input
			end if;

			-- Write to port 0x1C, 0x1D, 0x1E and 0x1F
			if z80_addr(7 downto 2)="000111" and port_wr='1' then
				port_1c <= z80_dout(7 downto 0);
			end if;
			
			-- Write to port 0x08
			if z80_addr(7 downto 0)=x"08" and port_wr='1' then
				port_08 <= z80_dout(7 downto 0);
			end if;

			-- Write to port 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57
			if z80_addr(7 downto 3)="01010" and port_wr='1' then
				port_50 <= z80_dout(7 downto 0);
			end if;

			-- Write to port 0xD0
			if pcu_mode='1' and z80_addr(7 downto 0)=x"D0" and port_wr='1' then
				port_D0 <= z80_dout(7 downto 0);
			end if;

			-- Trigger NMI to wake PCU
			if pcu_mode='0' then

				-- PCU disk operation completed?
				-- Keys available?
				-- Tape operation finished?
				if pcudisk_intreq='1' or tape_intreq='1' or pcu_key_available='1' then
					z80_nmi_n <= '0';
				end if;

			end if;

			-- On the first instruction after an NMI, switch to PCU mode
			if z80_m1_n='0' and z80_nmi_n='0' and z80_addr=x"0066" then
				pcu_mode <= '1';
				pcu_exit_request <= '0';
				z80_nmi_n <= '1';
			end if;

			-- Any write to port 80 while in pcu mode is a request to exit pcu mode.
			-- We need to wait for the RETN instruction to be read before the bank switch.
			-- To exit PCU mode:
			--      OUT	(80h),A
			--      RETN or RET
			if pcu_mode='1' and z80_addr(7 downto 0)=x"80" and port_wr='1' then
				pcu_exit_request <= '1';
			end if;

			-- Writing to port 0xFF starts a soft reset
			if pcu_mode='1' and z80_addr(7 downto 0)=x"FF" and port_wr='1' then
				request_soft_reset <= '1';
			end if;

			-- Switching back from PCU mode
			-- RETN instruction = ED 45 - (just look for the 45)
			-- RET instruction = C9
			if pcu_exit_request='1' and (z80_din=x"45" or z80_din=x"c9") and z80_wait_n='1' then
				pcu_mode <= '0';
				pcu_exit_request <= '0';
			end if; 

			-- PCU display mode
			if pcu_mode='1' and z80_addr(7 downto 0)=x"81" and port_wr='1' then
				pcu_display_mode <= z80_dout;
			end if;

			-- System flags
			if pcu_mode='1' and z80_addr(7 downto 0)=x"84" and port_wr='1' then
				system_flags <= z80_dout;
			end if;

			-- Capture current pc
			if z80_m1_n='0' then
				current_pc <= z80_addr;
			end if;

			-- Clear PCU key available flag?
			if z80_addr(7 downto 0)=x"83" and port_wr='1' and pcu_mode='1' then
				pcu_key_available <= '0';
			end if;

			-- Key press available? (edge detect, keydown only)
			KeyboardMessageAvailable_prev <= KeyboardMessageAvailable;
			if KeyboardMessageAvailable='1' and KeyboardMessageAvailable_prev='0' and KeyboardMessage(9)='0' then

				-- Only pass keys when pcu key mode enabled, or if it's F12
				if pcu_display_mode(1)='1' or pcu_key_important='1' then

					pcu_key_available <= '1';
					pcu_key_data <= KeyboardMessage;

				end if;

			end if;

		end if;
		end if; 
	end process;

	-- Generate Z80 wait signal
	z80_wait_n <= not (ram_wait or psg_wait);

	-- Decode I/O control signals from Z80
	mem_rd <= '1' when (z80_mreq_n = '0' and z80_iorq_n = '1' and z80_rd_n = '0') else '0';
	mem_wr <= '1' when (z80_mreq_n = '0' and z80_iorq_n = '1' and z80_wr_n = '0') else '0';
	port_rd <= '1' when (z80_iorq_n = '0' and z80_mreq_n = '1' and z80_rd_n = '0') else '0';
	port_wr <= '1' when (z80_iorq_n = '0' and z80_mreq_n = '1' and z80_wr_n = '0') else '0';

	-- CRTC ports
	crtc_data_port <= '1' when (z80_addr(7 downto 0) = x"0d") else '0';
	crtc_addr_port <= '1' when (z80_addr(7 downto 0) = x"0c") else '0';
	crtc_wr <= '1' when (port_wr='1' and (crtc_addr_port='1' or crtc_data_port='1')) else '0';
	crtc_rd <= '1' when (crtc_data_port='1') else '0';

	-- Memory write signals
	char_ram_wea <= mem_wr and char_ram_range;
	attr_ram_wea <= mem_wr and attr_ram_range and not system_flags(SysFlagNoVideoAttr);
	color_ram_wea <= mem_wr and color_ram_range and not system_flags(SysFlagNoVideoColor);
	pcgram_wea <= mem_wr and pcg_ram_range;
	pcu_char_ram_wea <= mem_wr and pcu_char_ram_range;
	pcu_color_ram_wea <= mem_wr and pcu_color_ram_range;
	color_ram_dout_b_resolved <= color_ram_dout_b when system_flags(SysFlagNoVideoColor)='0' else x"0A";
	attr_ram_dout_b_resolved <= attr_ram_dout_b when system_flags(SysFlagNoVideoAttr)='0' else (others=>'0');

	-- PCU Disk controller ports C0 - C7
	pcudisk_port <= '1' when pcu_mode='1' and z80_addr(7 downto 3)="11000" else '0';		-- 0xc0 => 0xc7 in pcu mode
	pcudisk_port_write <= pcudisk_port and port_wr;
	pcudisk_port_read <= pcudisk_port and port_rd;

	-- PCU Tape controller ports C8 - CF
	tape_port <= '1' when pcu_mode='1' and z80_addr(7 downto 3)="11001" else '0';		-- 0xc8 => 0xcF in pcu mode
	tape_port_write <= tape_port and port_wr;
	tape_port_read <= tape_port and port_rd;

	-- WD1002 Disk controller ports
	wddisk_port <= '1' when z80_addr(7 downto 4)="0100" and pcu_mode='0' else '0';		-- 0x40 -> 0x4F
	wddisk_port_write <= wddisk_port and port_wr;
	wddisk_port_read <= wddisk_port and port_rd;

	-- WD1002 Disk controller ports
	vdc_port <= '1' when z80_addr(7 downto 4)="0100" and pcu_mode='1' else '0';		-- 0x40 -> 0x4F
	vdc_port_write <= vdc_port and port_wr;
	vdc_port_read <= vdc_port and port_rd;

	-- When extended PCG range is enabled (port_1c(7)) and attempt to read memory beyond the first 16K
	-- return 0, rather than wrapping the address.
	pcgram_dout_b_range_tested <= pcgram_dout_b when pcgram_addr_crtc(14)='0' or port_1c(7)='0' else x"00";
	pcgram_addr_b <= pcgram_addr_crtc(13 downto 0) when port_1c(7)='1' else "000" & pcgram_addr_crtc(10 downto 0);
		
	-- Flash/Cellular memory
	ram_addr <= translated_address;
	ram_wr <= '1' when (ram_range='1' and mem_wr='1' and ram_range_ro='0') else '0';
	ram_rd <= '1' when (ram_range='1' and mem_rd='1') else '0';
	ram_wr_data <= z80_dout;

	-- Map z80 addresses
	process (z80_addr, port_50, port_1c, port_08, port_D0, latch_rom, small_char_set_selected, mem_rd, pcu_mode)
	begin

		ram_range <= '0';
		ram_range_ro <= '0';
		char_ram_range <= '0';
		attr_ram_range <= '0';
		pcg_ram_range <= '0';
		color_ram_range <= '0';
		charrom_range <= '0';
		pcu_char_ram_range <= '0';
		pcu_color_ram_range <= '0';

		-- Default translated address
		translated_address <= "00" & z80_addr;

		if pcu_mode='1' then

			if z80_addr(15 downto 14)="11" and port_D0(4)='1' then

				-- 0xC000-0xFFFF mapped to external RAM
				-- where low 4-bits of port D0 are the high 4 bits of the
				-- 18-bit external ram address
				-- Gives PCU access to all external RAM in 16k Pages
				ram_range <= '1';
				translated_address <= port_D0(3 downto 0) & z80_addr(13 downto 0);

			elsif z80_addr(15 downto 9)="1111000" then

				-- F000 - F1FF is PCU's video character buffer
				pcu_char_ram_range <= '1';

			elsif z80_addr(15 downto 9)="1111001" then

				-- F200 - F3FF is PCU's video color buffer
				pcu_color_ram_range <= '1';

			else

				-- PCU main memory resides in RAM at 0x30000
				ram_range <= '1';
				translated_address <= "11" & z80_addr;
			
			end if;

		elsif port_50(3)='0' and 											-- Video RAM enabled
			((port_50(4)='0' and z80_addr(15 downto 12)=x"F") or			-- 0xF000 - 0xFFFF
			 (port_50(4)='1' and z80_addr(15 downto 12)=x"8")) then		    -- 0x8000 - 0x8FFF

			if z80_addr(11)='0' then					

				if latch_rom='0' then

					-- Lower video RAM 
					if port_1c(4)='0' then
						char_ram_range <= '1';
					else
						attr_ram_range <= '1';
					end if;

				else

					translated_address <= "000000" & small_char_set_selected & z80_addr(10 downto 0);
					charrom_range <= '1';

				end if;

			else

				-- Upper video RAM
				if port_08(6)='0' then

					pcg_ram_range <= '1';
					if port_1c(7)='1' then

						-- Extended PCG banks enabled
						translated_address <= "0000" & port_1c(2 downto 0) & z80_addr(10 downto 0);
						pcg_ram_range <= not port_1c(3);  -- if accessing >16K PCG ram, ignore/return 0

					else

						-- Extended PCG banks disabled
						translated_address <= "0000000" & z80_addr(10 downto 0);

					end if; 
				else

					color_ram_range <='1';

				end if;
			end if;

		elsif port_50(2)='0' and z80_addr(15)='1' then

			-- Microbee ROM Range
			-- Roms are stored in RAM (initialize by PCU from SD card)
			ram_range <= '1';
			ram_range_ro <= '1';		-- Prevent write to RAM


			if z80_addr(14)='0' then 		-- < 0xC000

				-- ROM 1 0x8000->0xBFFF (comes from flash 0x20000 -> 0x23FFF)
				translated_address <= "10" & "00" & z80_addr(13 downto 0);

			elsif port_50(5)='0' then 

				-- ROM 2 0xC000->0xFFFF (comes from flash 0x24000 -> 0x27FFF)
				translated_address <= "10" & "01" & z80_addr(13 downto 0);

			else

				-- ROM 3 0xC000->0xFFFF (comes from flash 0x28000 -> 0x2BFFF)
				translated_address <= "10" & "10" & z80_addr(13 downto 0);

			end if;

		elsif z80_addr(15)='1' then

			-- RAM access above 0x8000 is always to bank 0 block 0
			ram_range <= '1';
			translated_address <= "000" & z80_addr(14 downto 0);

		else

			-- Banked RAM access
			-- NB: when port50 bit 2 is set, the meaning of port50 bit 1 is inverted - hence the xor below
			ram_range <= '1';
			translated_address <= "0" & (port_50(1) xor port_50(2)) & port_50(0) & z80_addr(14 downto 0);

		end if;

	end process;

	-- Multiplex data into the CPU
	process (
			boot_scan, system_flags,
			mem_rd, port_rd, 
			char_ram_range, attr_ram_range, pcg_ram_range, color_ram_range, charrom_range, ram_range, crtc_addr_port, crtc_data_port,
			char_ram_dout, pcgram_dout, charrom_dout, color_ram_dout, attr_ram_dout, ram_rd_data, crtc_dout, pio_port_b,
			tape_port_read, tape_dout, tape_audio_play,
			pcudisk_port_read, pcudisk_dout,
			wddisk_port_read, wddisk_dout,
			vdc_port_read, vdc_dout,
			port_1c, port_08, port_50, z80_addr,
			pcu_mode, pcu_display_mode, pcu_char_ram_range, pcu_color_ram_range, pcu_char_ram_dout, pcu_color_ram_dout, 
			pcu_key_data, pcu_key_available, shift_key_pressed, ctrl_key_pressed
			)
	begin

		z80_din <= (others=>'0');

		if mem_rd='1' then

			if pcu_mode='0' and boot_scan='1' then

				-- boot scan from 0000 -> 7fff
				z80_din <= x"00";

			elsif charrom_range='1' then

				-- Read from character ROM
				z80_din <= charrom_dout;

			elsif char_ram_range='1' then

				z80_din <= char_ram_dout;

			elsif attr_ram_range='1' then

				if system_flags(SysFlagNoVideoAttr)='1' then
					z80_din <= (others => '0');
				else
					z80_din <= attr_ram_dout;
				end if;

			elsif pcg_ram_range='1' then

				z80_din <= pcgram_dout;

			elsif color_ram_range='1' then

				if system_flags(SysFlagNoVideoColor)='1' then
					z80_din <= (others => '0');
				else
					z80_din <= color_ram_dout;
				end if;

			elsif ram_range='1' then

				z80_din <= ram_rd_data;
				
			elsif pcu_char_ram_range='1' then

				z80_din <= pcu_char_ram_dout;

			elsif pcu_color_ram_range='1' then

				z80_din <= pcu_color_ram_dout;

			end if;

		elsif port_rd='1' then

			if crtc_addr_port='1' or crtc_data_port='1' then 

				-- Read from 6545
				z80_din <= crtc_dout;

			elsif pcudisk_port_read='1' then

				-- Read from disk
				z80_din <= pcudisk_dout;

			elsif tape_port_read='1' then

				-- Read from disk
				z80_din <= tape_dout;

			elsif wddisk_port_read='1' then

				-- Read from disk
				z80_din <= wddisk_dout;

			elsif vdc_port_read='1' then

				-- Read from disk
				z80_din <= vdc_dout;

			elsif z80_addr(7 downto 0)=x"1c" then

				-- Read from port 1C
				z80_din <= port_1c;

			elsif z80_addr(7 downto 0)=x"02" then 

				-- Read from pio port B (cassette in)
				z80_din <= pio_port_b(7 downto 1) & tape_audio_play;

			elsif z80_addr(7 downto 0)=x"81" and pcu_mode='1' then

				z80_din <= pcu_display_mode;

			elsif z80_addr(7 downto 0)=x"82" and pcu_mode='1' then

				z80_din <= pcu_key_data(7 downto 0);

			elsif z80_addr(7 downto 0)=x"83" and pcu_mode='1' then

				-- Get the next key
				z80_din <= pcu_key_available & "000" & ctrl_key_pressed & shift_key_pressed & pcu_key_data(9 downto 8);

			elsif z80_addr(7 downto 0)=x"84" and pcu_mode='1' then

				z80_din <= system_flags;

			elsif z80_addr(7 downto 0)=x"85" and pcu_mode='1' then

				z80_din <= "00000" & pcudisk_intreq & tape_intreq & pcu_key_available;

			end if;

		end if;

	end process;


	-- Z80 CPU Core
	z80_core: entity work.T80se 
	GENERIC MAP
	(
		Mode 	=> 0,		-- 0 => Z80, 1 => Fast Z80, 2 => 8080, 3 => GB
		T2Write => 1,		-- 0 => WR_n active in T3, /=0 => WR_n active in T2
		IOWait 	=> 1		-- 0 => Single cycle I/O, 1 => Std I/O cycle
	)
	PORT MAP
	(
		RESET_n => reset_n, 
		CLK_n =>  clock_108_000,
		A => z80_addr,
		DI => z80_din,
		DO => z80_dout,
		MREQ_n => z80_mreq_n,
		IORQ_n => z80_iorq_n,
		RD_n => z80_rd_n,
		WR_n => z80_wr_n,
		CLKEN => clken_3_375,
		WAIT_n => z80_wait_n,
		INT_n => '1',
		NMI_n => z80_nmi_n,
		BUSRQ_n => '1',
		M1_n => z80_m1_n,
		RFSH_n => open,
		HALT_n => open,
		BUSAK_n => open
	);
		
	-- CRTC 6545
	Crtc6545: entity work.Crtc6545 
	PORT MAP
	(
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		reset => reset_s,
		wr => crtc_wr,
		rs => crtc_rd,
		din => z80_dout,
		dout => crtc_dout,
		
		pixel_clock => clock_40_000,
		vga_pixel_x => vga_pixel_x_internal,
		vga_pixel_y => vga_pixel_y_internal,
		vgaRed => vgaRed_mbee,
		vgaGreen => vgaGreen_mbee,
		vgaBlue => vgaBlue_mbee,

		vram_addr => char_ram_addr_b,
		char_ram_dout => char_ram_dout_b,
		attr_ram_dout => attr_ram_dout_b_resolved,
		color_ram_dout => color_ram_dout_b_resolved,
		pcgram_addr => pcgram_addr_crtc,
		pcgram_dout => pcgram_dout_b_range_tested,
		charrom_addr => charrom_addr_b,
		charrom_dout => charrom_dout_b,
		
		MicrobeeSwitches => MicrobeeSwitches,
		suppress_keyboard => pcu_display_mode(1),
		latch_rom => latch_rom,
		small_char_set_selected => small_char_set_selected
	);

	-- 2k Video RAM at 0xF000 - 0xF7FF (when latchrom = 0)
	char_ram : entity work.Ram
	GENERIC MAP
	(
		ADDR_WIDTH => 11
	)
	PORT MAP 
	(
		-- port A for CPU read/write
		clock_a => clock_108_000,
		wr_a => char_ram_wea,
		addr_a => translated_address(10 downto 0),
		din_a => z80_dout,
		dout_a => char_ram_dout,

		-- port B for 6545, read-only
		clock_b => clock_40_000,
		wr_b => '0',
		addr_b => char_ram_addr_b,
		din_b => (others => '0'),
		dout_b => char_ram_dout_b
	);


	-- 2k Attribute RAM at 0xF000 - 0xF7FF
	attr_ram : entity work.Ram
	GENERIC MAP
	(
		ADDR_WIDTH => 11
	)
	PORT MAP 
	(
		-- port A for CPU read/write
		clock_a => clock_108_000,
		wr_a => attr_ram_wea,
		addr_a => translated_address(10 downto 0),
		din_a => z80_dout,
		dout_a => attr_ram_dout,

		-- port B for 6545, read-only
		clock_b => clock_40_000,
		wr_b => '0',
		addr_b => char_ram_addr_b,
		din_b => (others => '0'),
		dout_b => attr_ram_dout_b
	);

	-- 2k Color RAM at 0xF800 - 0xFFFF
	color_ram : entity work.Ram
	GENERIC MAP
	(
		ADDR_WIDTH => 11
	)
	PORT MAP 
	(
		-- port A for CPU read/write
		clock_a => clock_108_000,
		wr_a => color_ram_wea,
		addr_a => translated_address(10 downto 0),
		din_a => z80_dout,
		dout_a => color_ram_dout,

		-- port B for 6545, read-only
		clock_b => clock_40_000,
		wr_b => '0',
		addr_b => char_ram_addr_b,
		din_b => (others => '0'),
		dout_b => color_ram_dout_b
	);

	-- 2k PCG RAM at 0xF800 - 0xFFFF (when latchrom = 0)
	-- 8 banks controlled by port 1C
	pcgram : entity work.Ram
	GENERIC MAP
	(
		ADDR_WIDTH => 14
	)
	PORT MAP 
	(
		-- port A for CPU read/write
		clock_a => clock_108_000,
		wr_a => pcgram_wea,
		addr_a => translated_address(13 downto 0),
		din_a => z80_dout,
		dout_a => pcgram_dout,

		-- port B for 6545, read-only
		clock_b => clock_40_000,
		wr_b => '0',
		addr_b => pcgram_addr_b,
		din_b => (others => '0'),
		dout_b => pcgram_dout_b
	);

	-- 4k Character ROM at 0xF000 - 0xFFFF (when latchrom = 1)
	-- content loaded by core generator from 4k charrom.bin
	charrom : entity work.CharRom
	PORT MAP 
	(
		-- port A for CPU
		clock_a => clock_108_000,
		addr_a => translated_address(11 downto 0),
		dout_a => charrom_dout,

		-- port B for 6545
		clock_b => clock_40_000,
		addr_b => charrom_addr_b,
		dout_b => charrom_dout_b
	);

	-- PCU Video Character RAM (512 bytes at 0xf000)
	pcu_char_ram : entity work.Ram
	GENERIC MAP
	(
		ADDR_WIDTH => 9
	)
	PORT MAP 
	(
		-- port A for CPU read/write
		clock_a => clock_108_000,
		wr_a => pcu_char_ram_wea,
		addr_a => translated_address(8 downto 0),
		din_a => z80_dout,
		dout_a => pcu_char_ram_dout,

		-- port B for PCU video controller, read-only
		clock_b => clock_40_000,
		wr_b => '0',
		addr_b => pcu_char_ram_addr_b,
		din_b => (others => '0'),
		dout_b => pcu_char_ram_dout_b
	);

	-- PCU Color RAM (512 bytes at 0xf200)
	pcu_color_ram : entity work.Ram
	GENERIC MAP
	(
		ADDR_WIDTH => 9
	)
	PORT MAP 
	(
		-- port A for CPU read/write
		clock_a => clock_108_000,
		wr_a => pcu_color_ram_wea,
		addr_a => translated_address(8 downto 0),
		din_a => z80_dout,
		dout_a => pcu_color_ram_dout,

		-- port B for PCU video controller, read-only
		clock_b => clock_40_000,
		wr_b => '0',
		addr_b => pcu_char_ram_addr_b,
		din_b => (others => '0'),
		dout_b => pcu_color_ram_dout_b
	);

	-- PCU Video Controller
	PcuVideoController: entity work.PcuVideoController
	PORT MAP
	(
		reset => reset_button,
		pixel_clock => clock_40_000,
		vga_pixel_x => vga_pixel_x_internal,
		vga_pixel_y => vga_pixel_y_internal,
		vgaRed => vgaRed_pcu,
		vgaGreen => vgaGreen_pcu,
		vgaBlue => vgaBlue_pcu,
		pixel_visible => pcu_pixel_visible,
		vram_addr => pcu_char_ram_addr_b,
		char_ram_dout => pcu_char_ram_dout_b,
		color_ram_dout => pcu_color_ram_dout_b
	);

	-- VGA timing
	vga_controller: entity work.vga_controller_800_60 
	PORT MAP
	(
		rst => reset_button,
		pixel_clk => clock_40_000,
		HS => vga_hsync,
		VS => vga_vsync,
		hcount => vga_pixel_x_internal,
		vcount => vga_pixel_y_internal,
		blank => video_blank
	);

	-- Status Panel
	StatusPanel : entity work.StatusPanel
	PORT MAP
	(
		reset => reset_button,
		pixel_clock => clock_40_000,
		enable => show_status_panel,
		leds => status_leds,
		hex => status_hex,
		vga_x_pixel => vga_pixel_x_internal,
		vga_y_pixel => vga_pixel_y_internal,
		pixel_out => status_pixel
	);


	-- Keyboard Port
	KeyboardPort : entity work.KeyboardPort
	PORT MAP
	(
		clock_108_000 => clock_108_000,
		reset => reset_button,
		PS2KeyboardData => ps2_keyboard_data,
		PS2KeyboardClk => ps2_keyboard_clock,
		KeyboardMessageAvailable => KeyboardMessageAvailable,
		KeyboardMessage => KeyboardMessage
	);

	-- Microbee keyboard decoder - decodes PS2 to Microbee switch states
	MicrobeeKeyboardDecoder: entity work.MicrobeeKeyboardDecoder 
	PORT MAP
	(
		clock_108_000 => clock_108_000,
		reset => reset_button,
		MonitorKey => monitor_key,
		KeyboardMessageAvailable => KeyboardMessageAvailable,
		KeyboardMessage => KeyboardMessage,
		MicrobeeSwitches => MicrobeeSwitches,
		raw_shift => shift_key_pressed,
		raw_ctrl => ctrl_key_pressed
	);



	-- SD Card Controller
	SDCardController : entity work.SDCardController
	PORT MAP
	(
		reset => reset_button,
		clock_108_000 => clock_108_000,

		ss_n => sd_ss_n,
		mosi => sd_mosi,
		miso => sd_miso,
		sclk => sd_sclk,

		status => sd_status_int,
		op_wr => sd_op_wr,
		op_cmd => sd_op_cmd,
		op_block_number => sd_op_block_number,
		last_block_number => sd_cur_block_number,

		dstart => sd_dstart,
		dcycle => sd_dcycle,
		dout => sd_dout,
		din => sd_din
	);

	sd_status <= sd_status_int;

	process (sd_arb_ack, 
				tape_sd_op_wr, tape_sd_op_cmd, tape_sd_op_block_number, tape_sd_din,
				pcudisk_sd_op_wr, pcudisk_sd_op_cmd, pcudisk_sd_op_block_number, pcudisk_sd_din,
				wddisk_sd_op_wr, wddisk_sd_op_cmd, wddisk_sd_op_block_number, wddisk_sd_din
			)
	begin

		case sd_arb_ack is

			when "001" => 
				sd_op_wr <= tape_sd_op_wr;
				sd_op_cmd <= tape_sd_op_cmd;
				sd_op_block_number <= tape_sd_op_block_number;
				sd_din <= tape_sd_din;

			when "010" => 
				sd_op_wr <= wddisk_sd_op_wr;
				sd_op_cmd <= wddisk_sd_op_cmd;
				sd_op_block_number <= wddisk_sd_op_block_number;
				sd_din <= wddisk_sd_din;

			when "100" => 
				sd_op_wr <= pcudisk_sd_op_wr;
				sd_op_cmd <= pcudisk_sd_op_cmd;
				sd_op_block_number <= pcudisk_sd_op_block_number;
				sd_din <= pcudisk_sd_din;

			when others =>
				sd_op_wr <= '0';
				sd_op_cmd <= (others => '0');
				sd_op_block_number <= (others => '0');
				sd_din <= (others => '0');

		end case;

	end process;

	-- Disk controller
	sd_arb : entity work.PriorityArbiter
	GENERIC MAP
	(
		CNT => 3
	)
	PORT MAP
	(
		clktb => clock_108_000,
		clken => clken_3_375,
		reset => reset_s,
		req => sd_arb_req,
		gnt => sd_arb_ack
	);

	-- Tape controller
	TapeController : entity work.TapeController
	PORT MAP
	(
		reset => reset_s,
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,

		cpu_port(2 downto 0) => z80_addr(2 downto 0),
		cpu_din => z80_dout,
		cpu_dout => tape_dout,
		cpu_wr => tape_port_write,
		cpu_rd => tape_port_read,

		audio_play => tape_audio_play,
		audio_record => tape_audio_record,

		intreq => tape_intreq,
		pcu_mode => pcu_mode,

		sd_arb_req => sd_arb_req(0),
		sd_arb_ack => sd_arb_ack(0),

		sd_status => sd_status_int,
		sd_op_wr => tape_sd_op_wr,
		sd_op_cmd => tape_sd_op_cmd,
		sd_op_block_number => tape_sd_op_block_number,
		sd_dstart => sd_dstart,
		sd_dcycle => sd_dcycle,
		sd_dout => sd_dout,
		sd_din => tape_sd_din
	);

	-- Microbee WD1002 Disk controller
	DiskControllerWD1002 : entity work.DiskControllerWD1002
	PORT MAP
	(
		reset => reset_s,
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		cpu_port(3 downto 0) => z80_addr(3 downto 0),
		cpu_din => z80_dout,
		cpu_dout => wddisk_dout,
		cpu_wr => wddisk_port_write,
		cpu_rd => wddisk_port_read,

		cmd => vdc_cmd,
		cmd_din => vdc_cmd_din,
		cmd_dout => vdc_cmd_dout,
		cmd_track => vdc_cmd_track,
		cmd_drive => vdc_cmd_drive,
		cmd_head => vdc_cmd_head,
		cmd_sector => vdc_cmd_sector,
		cmd_status_busy => vdc_cmd_status_busy,
		cmd_status_error => vdc_cmd_status_error,
		cmd_status_geo_error => vdc_cmd_status_geo_error,
		cmd_flag_buffer => vdc_cmd_flag_buffer,
		cmd_status_nodisk => vdc_cmd_status_nodisk
	);

	VirtualDiskController : entity work.VirtualDiskController
	PORT MAP
	(
		reset => reset_s,
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		cpu_port(0 downto 0) => z80_addr(0 downto 0),
		cpu_din => z80_dout,
		cpu_dout => vdc_dout,
		cpu_wr => vdc_port_write,
		cpu_rd => vdc_port_read,

		sd_arb_req => sd_arb_req(1),
		sd_arb_ack => sd_arb_ack(1),

		sd_status => sd_status_int,
		sd_op_wr => wddisk_sd_op_wr,
		sd_op_cmd => wddisk_sd_op_cmd,
		sd_op_block_number => wddisk_sd_op_block_number,
		sd_dstart => sd_dstart,
		sd_dcycle => sd_dcycle,
		sd_dout => sd_dout,
		sd_din => wddisk_sd_din,

		cmd => vdc_cmd,
		cmd_din => vdc_cmd_din,
		cmd_dout => vdc_cmd_dout,
		cmd_track => vdc_cmd_track,
		cmd_drive => vdc_cmd_drive,
		cmd_head => vdc_cmd_head,
		cmd_sector => vdc_cmd_sector,
		cmd_status_busy => vdc_cmd_status_busy,
		cmd_status_error => vdc_cmd_status_error,
		cmd_status_geo_error => vdc_cmd_status_geo_error,
		cmd_flag_buffer => vdc_cmd_flag_buffer,
		cmd_status_nodisk => vdc_cmd_status_nodisk
	);

	-- PCU Disk controller
	DiskControllerPcu : entity work.DiskControllerPcu
	PORT MAP
	(
		reset => reset_s,
		clock_108_000 => clock_108_000,
		clken_3_375 => clken_3_375,
		cpu_port(2 downto 0) => z80_addr(2 downto 0),
		cpu_din => z80_dout,
		cpu_dout => pcudisk_dout,
		cpu_wr => pcudisk_port_write,
		cpu_rd => pcudisk_port_read,

		sd_arb_req => sd_arb_req(2),
		sd_arb_ack => sd_arb_ack(2),

		intreq => pcudisk_intreq,

		sd_status => sd_status_int,
		sd_op_wr => pcudisk_sd_op_wr,
		sd_op_cmd => pcudisk_sd_op_cmd,
		sd_op_block_number => pcudisk_sd_op_block_number,
		sd_dstart => sd_dstart,
		sd_dcycle => sd_dcycle,
		sd_dout => sd_dout,
		sd_din => pcudisk_sd_din
	);

	Dac : entity work.Dac
	GENERIC MAP
	(
		SAMPLE_WIDTH => 8
	)
	PORT MAP
	(
		clock => clock_108_000,
		clken => clken_3_375,
		reset => reset_button,
		dac_i => dac_i,
		dac_o => dac_o
	);

	sn76489 : entity work.sn76489_top
	GENERIC MAP
	(
		clock_div_16_g => 1
	)
	PORT MAP
	(
	    clock_i => clock_108_000,
	    clock_en_i => clken_3_375,
	    res_n_i => reset_n,
	    ce_n_i => psg_ce_n,
	    we_n_i => psg_we_n,
	    ready_o => psg_ready,
	    d_i => z80_dout,
	    aout_o => psg_out
	);

	-- Write to ports 0x10, 0x11, 0x12, 0x13
	psg_we_n <= '0' when port_wr='1' else '1';
	psg_ce_n <= '0' when z80_addr(7 downto 2)="000100" else '1';
	psg_wait <= '1' when psg_we_n='0' and psg_ce_n='0' and psg_ready='0' else '0';

	speaker_aout <= 
				speaker_volume 
				when (pio_port_b(6) and not system_flags(SysFlagNoSound))='1' 
				else x"00";

	tape_aout <= 
				tape_volume 
				when ((tape_audio_play xor tape_audio_record) and not system_flags(SysFlagNoTapeMonitor))='1'
				else x"00";

	psg_aout <=
				psg_out
				when system_flags(SysFlagNoSound)='0'
				else x"00";

	-- Send audio to DAC
	dac_i <= std_logic_vector(unsigned(std_logic_vector(speaker_aout + tape_aout + psg_aout)) + 128);

	speaker <= (tape_audio_play xor tape_audio_record) when system_flags(sysFlagDirectTapeMonitor)='1' else dac_o;

end Behavioral;

