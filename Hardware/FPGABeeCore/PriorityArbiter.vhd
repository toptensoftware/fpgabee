-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.SDStatusBits.ALL;

entity PriorityArbiter is
	generic 
	( 
		CNT : integer := 4 
	);
	port 
	(
		clktb : in std_logic;
		clken : in std_logic;
		reset : in std_logic;
		req : in std_logic_vector(CNT-1 downto 0);
		gnt : out std_logic_vector(CNT-1 downto 0)
	);

end PriorityArbiter;

architecture Behavioral of PriorityArbiter IS
	signal gntcur : std_logic_vector(CNT-1 downto 0);
begin

	gnt <= gntcur;

	process (clktb)
	begin

		if rising_edge(clktb) then
		if reset='1' then

			gntcur <= (others => '0');

		elsif clken='1' then

			if (gntcur and req) = (gntcur'range => '0') then
			
				gntcur <= req and std_logic_vector(unsigned(not(req)) + 1);

			end if;

		end if;
		end if;

	end process;
end Behavioral;


