-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use work.DiskConstants.ALL;
use work.SDStatusBits.ALL;
 
entity VirtualDiskController is
	port
	(
		-- Clocking
		reset : in std_logic;
		clock_108_000 : in std_logic;						-- CPU Clock
		clken_3_375 : in std_logic;							-- CPU Clock enable

		-- CPU Interface
		cpu_port : in std_logic_vector(0 downto 0);			-- low 4 bits of the z80 port
		cpu_wr : in std_logic;								-- write signal
		cpu_rd : in std_logic;								-- read signal
		cpu_din : in std_logic_vector(7 downto 0);			-- output from z80, input to controller
		cpu_dout : out std_logic_vector(7 downto 0);		-- output from controller, input to z80

		-- SDCard Interface
		sd_arb_req : out std_logic;
		sd_arb_ack : in std_logic;
		sd_status : in std_logic_vector(7 downto 0);
		sd_op_wr : out std_logic;
		sd_op_cmd : out std_logic_vector(1 downto 0);
		sd_op_block_number : out std_logic_vector(31 downto 0);
		sd_dstart : in std_logic;
		sd_dcycle : in std_logic;
		sd_dout : in std_logic_vector(7 downto 0);
		sd_din : out std_logic_vector(7 downto 0);

		-- Command interface
		cmd : in std_logic_vector(2 downto 0);			-- VDC_CMD_xxx commands
		cmd_din : in std_logic_vector(7 downto 0);
		cmd_dout : out std_logic_vector(7 downto 0);
		cmd_track : in std_logic_vector(15 downto 0);
		cmd_drive : in std_logic_vector(2 downto 0);
		cmd_head : in std_logic_vector(2 downto 0);
		cmd_sector : in std_logic_vector(7 downto 0);
		cmd_status_busy : out std_logic;
		cmd_status_error : out std_logic;
		cmd_status_geo_error : out std_logic;
		cmd_status_nodisk : out std_logic;
		cmd_flag_buffer : out std_logic
	);
end VirtualDiskController;

architecture behavior of VirtualDiskController is 

	-- Edge detection the for CPU read/write port signals
	-- (we need this since the Z80 always introduces one wait state
	--  for port instructions and we don't want to inadvertantly double
	--  invoke commands, or increments our DMA buffer address)
	signal cpu_wr_prev : std_logic;
	signal cpu_rd_prev : std_logic;

	-- Geometry related signals
	signal geo_invoke : std_logic;
	signal geo_ready : std_logic;
	signal geo_error : std_logic;
	signal geo_disk_type : std_logic_vector(3 downto 0);
	signal geo_cluster : std_logic_vector(16 downto 0);
	signal no_disk : std_logic;

	-- Disk operations
	signal calculated_cluster : std_logic_vector(16 downto 0);
	signal calculated_error : std_logic;
	signal diskimage_base_block_number : std_logic_vector(31 downto 0);

	signal pcu_base_block_reg : std_logic_vector(31 downto 0);

	-- DMA buffer - CPU side access
	signal opram_we : std_logic;
	signal opram_addr : std_logic_vector(8 DOWNTO 0);

	-- DMA buffer - SD controller side
	signal sdram_we : std_logic;
	signal sdram_addr : std_logic_vector(8 DOWNTO 0);
	signal sdram_dout : std_logic_vector(7 DOWNTO 0);

	-- Command execution state machine
	type exec_states is 
	(
		STATE_READY,
		STATE_GEO_WAIT_START,
		STATE_GEO_WAIT,
		STATE_SD_ARBWAIT,
		STATE_SD_BUSY
	);

	type exec_results is
	(
		RESULT_OK,
		RESULT_GEO_ERROR,
		RESULT_SD_ERROR
	);

	signal exec_state : exec_states := STATE_READY;	-- Execution state
	signal exec_request : std_logic;				-- Request a read/write
	signal exec_response : std_logic;				-- Indicates response to read/write command
	signal exec_result : exec_results := RESULT_OK;	-- Result of the last executed command	

--	signal block_number : std_logic_vector(31 downto 0);
	signal sd_op_cmd_int : std_logic_vector(1 downto 0);


	type disk_info_type is record
		base_block_number : std_logic_vector(31 downto 0);
		disk_type : std_logic_vector(3 downto 0);
	end record;

	type disk_info_array_type is array(0 to 7) of disk_info_type;

	signal disk_info_array : disk_info_array_type := ( 
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE ),
		( base_block_number => (others => '0'), disk_type => DISK_NONE )
	);
	signal disk_info : disk_info_type;

begin

	-- Select disk info
	disk_info <= disk_info_array(to_integer(unsigned(cmd_drive)));
	geo_disk_type <= disk_info.disk_type;
	diskimage_base_block_number <= disk_info.base_block_number;

	-- Work out if a disk is present
	cmd_status_nodisk <= geo_disk_type(3);

	-- Export block number to SD Controller
	sd_op_cmd <= sd_op_cmd_int;

	-- Front end CPU interface
	process (clock_108_000)
	begin

		if rising_edge(clock_108_000) then
		if reset = '1' then

			cpu_wr_prev <= '0';
			cpu_rd_prev <= '0';

			opram_addr <= (others=>'0');
			opram_we <= '0';

			exec_request <= '0';
			sd_op_cmd_int <= "00";

			pcu_base_block_reg <= (others => '0');

			disk_info_array(0).disk_type <= DISK_NONE;
			disk_info_array(1).disk_type <= DISK_NONE;
			disk_info_array(2).disk_type <= DISK_NONE;
			disk_info_array(3).disk_type <= DISK_NONE;
			disk_info_array(4).disk_type <= DISK_NONE;
			disk_info_array(5).disk_type <= DISK_NONE;
			disk_info_array(6).disk_type <= DISK_NONE;
			disk_info_array(7).disk_type <= DISK_NONE;

			cmd_status_busy <= '0';
			cmd_status_error <= '0';
			cmd_status_geo_error <= '0';
			cmd_flag_buffer <= '0';

		elsif clken_3_375='1' then

			-- Track read/write edges
			cpu_wr_prev <= cpu_wr;
			cpu_rd_prev <= cpu_rd;

			cmd_flag_buffer <= '0';

			if opram_we = '1' then
				opram_addr <= std_logic_vector(unsigned(opram_addr) + 1);
				if opram_addr = "111111111" then
					cmd_flag_buffer <= '1';
				end if;
			end if;

			-- Reset ram write line
			opram_we <= '0';
			exec_request <= '0';

			-- If busy, check for end of operation
			if exec_response='1' then

				-- Clear busy flag
				cmd_status_busy <= '0';
				sd_op_cmd_int <= "00";

				-- Handle result
				case exec_result is

					when RESULT_OK =>
						cmd_status_error <= '0';

					when RESULT_GEO_ERROR =>
						cmd_status_error <= '1';
						cmd_status_geo_error <= '1';

					when RESULT_SD_ERROR =>
						cmd_status_error <= '1';
						cmd_status_geo_error <= '0';

				end case;

			end if;

			-- Port write?
			if cpu_wr='1' and cpu_wr_prev='0' then

				case cpu_port is

					when "0" =>
						-- WRITE BASE BLOCK NUMBER
						-- LSB first
						pcu_base_block_reg <= cpu_din & pcu_base_block_reg(31 downto 8);

					when "1" =>
						-- COMMAND
						disk_info_array(to_integer(unsigned(cpu_din(2 downto 0)))).base_block_number <= pcu_base_block_reg;
						disk_info_array(to_integer(unsigned(cpu_din(2 downto 0)))).disk_type <= cpu_din(6 downto 3);

					when others => 
						null;

				end case;

			elsif cpu_rd='1' and cpu_rd_prev='0' then

				cpu_dout <= (others=>'0');

			end if;

			opram_we <= '0';

			case cmd is

				when VDC_CMD_READ_BLOCK =>
					sd_op_cmd_int <= "01";
					opram_addr <= (others=>'0');
					exec_request <= '1';
					cmd_status_busy <= '1';

				when VDC_CMD_WRITE_BLOCK =>
					sd_op_cmd_int <= "10";
					exec_request <= '1';
					cmd_status_busy <= '1';

				when VDC_CMD_READ_BYTE =>
					opram_addr <= std_logic_vector(unsigned(opram_addr) + 1);
					if opram_addr = "111111111" then
						cmd_flag_buffer <= '1';
					end if;

				when VDC_CMD_WRITE_BYTE =>
					opram_we <= '1';

				when VDC_CMD_RESTORE =>
					opram_addr <= (others => '0');
					cmd_status_busy	<= '0';
					cmd_status_error <= '0';
					cmd_status_geo_error <= '0';

				when VDC_CMD_RESET_BYTE_PTR =>
					opram_addr <= (others=>'0');

				when others => 
					null;

			end case;

		end if;
		end if;

	end process;

	-- Command execution unit
	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			sd_op_wr <= '0';
			exec_state <= STATE_READY;
			sd_op_block_number <= x"00000000";
			exec_response <= '0';
			exec_result <= RESULT_OK;
			geo_invoke <= '0';

		elsif clken_3_375='1' then

			sd_op_wr <= '0';
			exec_response <= '0';
			geo_invoke <= '0';

			case exec_state is
				when STATE_READY =>

					if exec_request='1' then
						-- Request SD from arbiter
						sd_arb_req <= '1';
						geo_invoke <= '1';

						-- Wait for ack
						exec_state <= STATE_GEO_WAIT_START;
					else
						sd_arb_req <= '0';
					end if;
			

				when STATE_GEO_WAIT_START =>
					-- The DiskGeometry component requires one cycle after being
					-- invoked before it leaves the ready state.  So pause one
					-- cycle before checking if the calculation has finished.
					exec_state <= STATE_GEO_WAIT;

				when STATE_GEO_WAIT =>

					if geo_ready='1' then
						if geo_error='1' then

							-- Abort with geometry error
							exec_result <= RESULT_GEO_ERROR;
							exec_state <= STATE_READY;
							exec_response <= '1';

						else
							-- Start the read/write request
							sd_op_block_number <= std_logic_vector(unsigned(diskimage_base_block_number) + unsigned(geo_cluster));
							exec_state <= STATE_SD_ARBWAIT;
						end if;
					end if;

				when STATE_SD_ARBWAIT => 
					if sd_arb_ack='1' then
						sd_op_wr <= '1';
						exec_state <= STATE_SD_BUSY;
					end if;

				when STATE_SD_BUSY =>
					if sd_status(STATUS_BIT_BUSY)='0' then

						-- Operation finished?
						if sd_status(STATUS_BIT_ERROR)='1' then
							exec_result <= RESULT_SD_ERROR;
						else
							exec_result <= RESULT_OK;
						end if;

						exec_state <= STATE_READY;
						exec_response <= '1';

					end if;

			end case;
		end if;
		end if;
	end process;

	-- Disk geometry calculation unit
	DiskGeometry : entity work.DiskGeometry
	PORT MAP
	(
		reset => reset,
		clock => clock_108_000,
		clken => clken_3_375,
		invoke => geo_invoke,
		ready => geo_ready,
		error => geo_error,
		disk_type => geo_disk_type,
		track => cmd_track,
		head => cmd_head,
		sector => cmd_sector,
		cluster => geo_cluster
	);

	process (clock_108_000)
	begin
		if rising_edge(clock_108_000) then
		if reset='1' then

			sdram_addr <= (others => '0');

		else

			if sd_dstart = '1' then

				-- Data start
				sdram_addr <= (others => '0');

			elsif sd_dcycle='1' then

				-- Data cycle
				sdram_addr <= std_logic_vector(unsigned(sdram_addr) + 1);

			end if;

		end if;
		end if;
	end process;

	-- SD Controller -> RAM
	sdram_we <= '1' when (sd_dcycle='1' and sd_arb_ack='1' and sd_op_cmd_int="01") else '0';

	-- Sector buffer
	ram : entity work.RamInferred
	GENERIC MAP
	(
		ADDR_WIDTH => 9
	)
	PORT MAP
	(
		clock_a => clock_108_000,
		wr_a => opram_we,
		addr_a => opram_addr,
		din_a => cmd_din,
		dout_a => cmd_dout,

		clock_b => clock_108_000,
		wr_b => sdram_we,
		addr_b => sdram_addr,
		din_b => sd_dout,
		dout_b => sd_din
	);


end;
