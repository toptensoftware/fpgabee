-- FpgaBee
-- FpgaBee
--
-- Copyright (C) 2012-2013 Topten Software.
-- All Rights Reserved
-- 
-- Licensed under the Apache License, Version 2.0 (the "License"); you may not use this 
-- product except in compliance with the License. You may obtain a copy of the License at
-- 
-- http://www.apache.org/licenses/LICENSE-2.0
-- 
-- Unless required by applicable law or agreed to in writing, software distributed under 
-- the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF 
-- ANY KIND, either express or implied. See the License for the specific language governing 
-- permissions and limitations under the License.


library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;

entity Ram1024 is
	port
	(
		-- Port A
		clock_a : in std_logic;
		addr_a : in std_logic_vector(9 downto 0);
		din_a : in std_logic_vector(7 downto 0);
		dout_a : out std_logic_vector(7 downto 0);
		wr_a : in std_logic;

		-- Port B
		clock_b : in std_logic;
		addr_b : in std_logic_vector(9 downto 0);
		din_b : in std_logic_vector(7 downto 0);
		dout_b : out std_logic_vector(7 downto 0);
		wr_b : in std_logic
	);
end Ram1024;
 
architecture behavior of Ram1024 is 
	signal wea : std_logic_vector(0 downto 0);
	signal web : std_logic_vector(0 downto 0);
begin
	
	wea(0) <= wr_a;
	web(0) <= wr_b;

	ramImpl : entity work.Ram1K
	PORT MAP 
	(
		-- port A		
		clka => clock_a,
		wea => wea,
		addra => addr_a,
		dina => din_a,
		douta => dout_a,

		-- port B
		clkb => clock_b,
		web => web,
		addrb => addr_b,
		dinb => din_b,
		doutb => dout_b
	);

end;
