﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeTestDisk
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "test.ds40";

            int heads = 2;
            int cylinders = 40;
            int sectors = 10;

            // Create the file
            var f = System.IO.File.Create(filename);

            var buf = new byte[1024];
            for (int c = 0; c < cylinders; c++)
            {
                for (int h = 0; h < heads; h++)
                {
                    for (int s = 0; s < sectors; s++)
                    {
                        buf[0] = (byte)(c & 0xFF);
                        buf[1] = (byte)((c >> 8) & 0xFF);
                        buf[2] = (byte)(h & 0xFF);
                        buf[3] = (byte)((s+1) & 0xFF);
                        f.Write(buf, 0, 4);

                        var sb = new StringBuilder();
                        while (sb.Length < 512)
                        {
                            sb.AppendFormat("[Cyl: {0} Head: {1} Sector: {2}] ", c, h, s);
                        }

                        f.Write(Encoding.ASCII.GetBytes(sb.ToString()), 0, 508);
                    }
                }
            }
        }
    }
}
